import axios, { AxiosError } from 'axios';
import moment from 'moment';
import dayjs from 'dayjs';
import { serverEndPointType } from './context/DRFReactBySchemaContext';
import {
    isTmpId,
    emptyByType,
    getChoiceByValue,
    Field,
    Item,
    SchemaType,
    modelOptionsType,
    DataSchemaColumnsType,
    ItemSchemaColumnsType,
    PaginatedResult,
    Id,
    SchemaOptionsType,
    GridEnrichedBySchemaColDef,
    TargetApiParams,
    SchemaOptionsSingleActionType,
    TargetApiPostParams,
    GetGenericModelListProps,
} from './utils';
import { GridFilterModel, GridSortModel } from '@mui/x-data-grid';

const expiredTokenMessage = 'Token expired! System must login again';
const noServerEndPointMessage = 'There is not API definition (serverEndPoint)!';
const unknownError = 'Unknown error!';

interface TargetApiParamsOptionalId {
    path: string;
    serverEndPoint: serverEndPointType | null;
    data: Item;
    id?: Id;
}
const getOptions = async (path: string, serverEndPoint: serverEndPointType | null) => {
    if (!serverEndPoint) {
        return <AxiosError>{
            message: noServerEndPointMessage,
            isAxiosError: true,
        };
    }
    const url = `${serverEndPoint.api}/${path}`;
    try {
        const { data } = await axios.options(url);
        return <SchemaOptionsType>data;
    } catch (e) {
        if (axios.isAxiosError(e)) {
            const err = <AxiosError>e;
            const message = `Error fetching options from ${url}`;
            if (err.response?.status === 401) {
                const isRefreshed = await refreshToken(serverEndPoint);
                if (!isRefreshed) {
                    console.log(expiredTokenMessage);
                    return <AxiosError>{
                        message: expiredTokenMessage,
                        isAxiosError: true,
                    };
                }
                try {
                    const { data } = await axios.options(url);
                    return <SchemaOptionsType>data;
                } catch (e) {
                    console.log(message, e);
                    return <AxiosError>{
                        message,
                        isAxiosError: true,
                    };
                }
            }
            console.log(message, e);
            return <AxiosError>{
                message,
                isAxiosError: true,
            };
        }
        return <AxiosError>{
            message: unknownError,
            isAxiosError: true,
        };
    }
};

const getSchema = async (path: string, serverEndPoint: serverEndPointType | null) => {
    const options = await getOptions(path, serverEndPoint);
    if ('isAxiosError' in options) {
        return false;
    }

    // Special default value of "currentUser":
    let usuaria: Item | boolean = false;
    const postActions: Record<string, Field> = options.actions.POST;
    for (const [key, field] of Object.entries(postActions)) {
        if (field.model_default === 'currentUser') {
            if (!usuaria) {
                usuaria = await isLoggedIn(serverEndPoint);
            }
            if (usuaria) {
                options.actions.POST[key].model_default = {
                    id: usuaria.id,
                    label: usuaria.first_name,
                };
            }
        }
        if (field.model_default && field.type === 'choice') {
            if (field.choices) {
                const modelDefault = field.choices.filter(
                    (choice) => choice.value === field.model_default
                );
                options.actions.POST[key].model_default =
                    modelDefault.length === 1 ? modelDefault[0] : undefined;
            }
        }
    }

    const modelOptions = <modelOptionsType>{
        name: options.name,
        description: options.description,
        verbose_name: options.verbose_name,
        verbose_name_plural: options.verbose_name_plural,
    };

    return { schema: options.actions.POST, modelOptions };
};

export const getRawData = async ({
    path,
    serverEndPoint,
}: {
    path: string;
    serverEndPoint: serverEndPointType | null;
}) => {
    if (!serverEndPoint) {
        return <AxiosError>{
            message: noServerEndPointMessage,
            isAxiosError: true,
        };
    }
    const url = `${serverEndPoint.api}/${path}`;
    try {
        const { data } = await axios.get<Item[]>(url);
        return data;
    } catch (e) {
        const message = `Error fetching data from ${url}`;
        if (axios.isAxiosError(e)) {
            const err = <AxiosError>e;
            if (err.response?.status === 401) {
                const isRefreshed = await refreshToken(serverEndPoint);
                if (!isRefreshed) {
                    console.log(expiredTokenMessage);
                    return <AxiosError>{
                        message: expiredTokenMessage,
                        isAxiosError: true,
                    };
                }
                try {
                    const { data } = await axios.get<Item[]>(url);
                    return data;
                } catch (e) {
                    const message = `Error fetching data from ${url}`;
                    console.log(message, e);
                    return <AxiosError>{
                        message,
                        isAxiosError: true,
                    };
                }
            }
        }
        console.log(message, e);
        return <AxiosError>{
            message,
            isAxiosError: true,
        };
    }
};

const getData = async ({
    path,
    serverEndPoint,
    route = 'api',
}: {
    path: string;
    serverEndPoint: serverEndPointType | null;
    route?: string;
}) => {
    if (!serverEndPoint) {
        return <AxiosError>{
            message: noServerEndPointMessage,
            isAxiosError: true,
        };
    }
    const url = `${serverEndPoint[route as keyof serverEndPointType]}/${path}`;
    try {
        const { data } = await axios.get<Item[] | PaginatedResult | Item>(url);
        return data;
    } catch (e) {
        if (axios.isAxiosError(e)) {
            const err = <AxiosError>e;
            if (err.response?.status === 401) {
                const isRefreshed = await refreshToken(serverEndPoint);
                if (!isRefreshed) {
                    console.log(expiredTokenMessage);
                    return <AxiosError>{
                        message: expiredTokenMessage,
                        isAxiosError: true,
                    };
                }
                try {
                    const { data } = await axios.get<Item[] | PaginatedResult | Item>(url);
                    return data;
                } catch (e) {
                    console.log(expiredTokenMessage);
                    return <AxiosError>{
                        message: expiredTokenMessage,
                        isAxiosError: true,
                    };
                }
            }
        }
        const message = `Error fetching data from ${url}`;
        console.log(message, e);
        return <AxiosError>{
            message,
            isAxiosError: true,
        };
    }
};

/**
 *
 * @param options - params
 * @returns updatedData when succesfully updated, axiosError otherwise
 */
export const updateData = async ({ path, serverEndPoint, data, id }: TargetApiParams) => {
    if (!serverEndPoint) {
        return <AxiosError>{
            message: noServerEndPointMessage,
            isAxiosError: true,
        };
    }
    const url = `${serverEndPoint.api}/${path}/${id}/`;
    try {
        const { data: updatedData } = await axios.put<Item>(url, data);
        return updatedData;
    } catch (e) {
        if (axios.isAxiosError(e)) {
            const err = <AxiosError>e;
            const message = `Error updating data at ${url}`;
            if (err.response?.status === 401) {
                const isRefreshed = await refreshToken(serverEndPoint);
                if (!isRefreshed) {
                    console.log(expiredTokenMessage);
                    return <AxiosError>{
                        message: expiredTokenMessage,
                        isAxiosError: true,
                    };
                }
                try {
                    const { data: updatedData } = await axios.put<Item>(url, data);
                    return updatedData;
                } catch (e) {
                    console.log(message, data, err.response?.data);
                    return { message, isAxiosError: true } as AxiosError;
                }
            }
            console.log(message, data, err.response?.data);
            return { message, isAxiosError: true } as AxiosError;
        }
    }
    return { message: unknownError, isAxiosError: true } as AxiosError;
};

export const partialUpdateData = async ({ path, serverEndPoint, data, id }: TargetApiParams) => {
    if (!serverEndPoint) {
        return <AxiosError>{
            message: noServerEndPointMessage,
            isAxiosError: true,
        };
    }
    const url = `${serverEndPoint.api}/${path}/${id}/`;
    try {
        const { data: updatedData } = await axios.patch<Item>(url, data);
        return updatedData;
    } catch (e) {
        if (axios.isAxiosError(e)) {
            const err = <AxiosError>e;
            const message = `Error partial updating data at ${url}`;
            if (err.response?.status === 401) {
                const isRefreshed = await refreshToken(serverEndPoint);
                if (!isRefreshed) {
                    console.log(expiredTokenMessage);
                    return <AxiosError>{
                        message: expiredTokenMessage,
                        isAxiosError: true,
                    };
                }
                try {
                    const { data: updatedData } = await axios.patch<Item>(url, data);
                    return updatedData;
                } catch (e) {
                    console.log(message, data, e);
                    return {
                        message: err.response ? err.response.data : message,
                        isAxiosError: true,
                    } as AxiosError;
                }
            }
            console.log(message, data, err.response?.data);
            return {
                message: err.response ? err.response.data : message,
                isAxiosError: true,
            } as AxiosError;
        }
    }
    return { message: unknownError, isAxiosError: true } as AxiosError;
};

export const createData = async ({ path, serverEndPoint, data }: TargetApiPostParams) => {
    if (!serverEndPoint) {
        return <AxiosError>{
            message: noServerEndPointMessage,
            isAxiosError: true,
        };
    }
    const url = `${serverEndPoint.api}/${path}/`;
    try {
        const { data: createdData } = await axios.post<Item>(url, data);
        return createdData;
    } catch (e) {
        if (axios.isAxiosError(e)) {
            const err = <AxiosError>e;
            const message = `Error creating data at ${url}`;
            if (err.response?.status === 401) {
                const isRefreshed = await refreshToken(serverEndPoint);
                if (!isRefreshed) {
                    console.log(expiredTokenMessage);
                    return <AxiosError>{
                        message: expiredTokenMessage,
                        isAxiosError: true,
                    };
                }
                try {
                    const { data: createdData } = await axios.post<Item>(url, data);
                    return createdData;
                } catch (e) {
                    console.log(message, data, e);
                    return <AxiosError>{
                        message: err.response ? err.response.data : message,
                        isAxiosError: true,
                    };
                }
            }
            console.log(message, data, err.response?.data);
            return <AxiosError>{
                message: err.response ? err.response.data : message,
                isAxiosError: true,
            };
        }
    }
    return <AxiosError>{
        message: unknownError,
        isAxiosError: true,
    };
};

export const deleteData = async (
    path: string,
    serverEndPoint: serverEndPointType | null,
    id: Id
) => {
    if (!serverEndPoint) {
        return <AxiosError>{
            message: noServerEndPointMessage,
            isAxiosError: true,
        };
    }
    const url = `${serverEndPoint.api}/${path}/${id}`;
    try {
        await axios.delete(url);
        return true;
    } catch (e) {
        if (axios.isAxiosError(e)) {
            const err = <AxiosError>e;
            const message = `Error deleting data from ${url}`;
            if (err.response?.status === 401) {
                const isRefreshed = await refreshToken(serverEndPoint);
                if (!isRefreshed) {
                    console.log(expiredTokenMessage);
                    return <AxiosError>{
                        message: expiredTokenMessage,
                        isAxiosError: true,
                    };
                }
                try {
                    await axios.delete(url);
                    return true;
                } catch (e) {
                    console.log(message, e);
                    return {
                        message: err.response ? err.response.data : message,
                        isAxiosError: true,
                    } as AxiosError;
                }
            }
            console.log(message, e);
            return {
                message: err.response ? err.response.data : message,
                isAxiosError: true,
            } as AxiosError;
        }
    }
    return <AxiosError>{
        message: unknownError,
        isAxiosError: true,
    };
};

export const createOrUpdateData = async ({
    path,
    serverEndPoint,
    data,
    id,
}: TargetApiParamsOptionalId) => {
    if (isTmpId(id) || !id) {
        const responseCreate = await createData({ path, serverEndPoint, data });
        return responseCreate;
    }

    const responseUpdate = await updateData({ path, serverEndPoint, data, id });
    return responseUpdate;
};

const prepareDataBySchema = ({ data, schema }: { data: Item; schema: SchemaType }) => {
    // console.log('Entered prepareDataBySchema', schema);
    const dbData: Item = {};
    for (const [key, field] of Object.entries(schema)) {
        // console.log(key, field, data);
        if (!(key in data) || (key === 'id' && isTmpId(data[key]))) {
            continue;
        }
        if (!data[key]) {
            dbData[key] = emptyByType(field, true);
            continue;
        }
        // console.log({ key, data: data[key] });

        // OneToMany or ManyToMany relations:
        if (field.type === 'field' && field.child) {
            const dataArr: Item[] = data[key];
            dbData[key] = dataArr.map((row) => {
                return field.child?.children
                    ? prepareDataBySchema({
                          data: row,
                          schema: field.child.children,
                      })
                    : row;
            });
            continue;
        }

        // Nested Object:
        if (field.type === 'nested object' && field.children && typeof data[key] === 'object') {
            dbData[key] = isTmpId(data[key].id)
                ? prepareDataBySchema({
                      data: data[key] as Item,
                      schema: field.children,
                  })
                : data[key].id;
            continue;
        }

        // Choices:
        if (field.type === 'choice') {
            dbData[key] = data[key].value;
            continue;
        }

        // Date:
        if (field.type === 'date') {
            dbData[key] = dayjs(data[key]).format('YYYY-MM-DD');
            continue;
        }

        // DateTime:
        if (field.type === 'datetime') {
            const date = moment(data[key]);
            dbData[key] = date.format('YYYY-MM-DDTHH:mm');
            continue;
        }

        // Default:
        dbData[key] = data[key];
    }
    // console.log({dbData});
    return dbData;
};

export const updateDataBySchema = async ({
    model,
    modelObjectId,
    serverEndPoint,
    data,
    schema,
    path = null,
}: {
    model: string;
    modelObjectId: Id;
    serverEndPoint: serverEndPointType | null;
    data: Item;
    schema: SchemaType;
    path?: string | null;
}) => {
    const dbData: Item = prepareDataBySchema({ data, schema });

    if (!path) {
        path = model;
    }
    // DEBUG console.log({ model, modelObjectId, path, data, dbData });
    const response = await createOrUpdateData({
        path,
        serverEndPoint,
        data: dbData,
        id: modelObjectId,
    });
    return response;
};

export const addExistingRelatedModel = async ({
    model,
    serverEndPoint,
    id,
    data,
}: {
    model: string;
    serverEndPoint: serverEndPointType | null;
    id: Id;
    data: Item;
}) => {
    const response = await partialUpdateData({ path: model, serverEndPoint, data, id });
    return response;
};

const getDataGridColumns = (
    schema: SchemaType,
    columnFields: string[] = [],
    hiddenFields: string[] = [],
    creatableFields: string[] = [],
    disabledFields: string[] = []
) => {
    if (!schema) {
        return false;
    }
    columnFields =
        columnFields.length === 0
            ? Object.keys(schema)
            : columnFields.filter((field) => {
                  return Object.prototype.hasOwnProperty.call(schema, field);
              });

    return columnFields.map((field) => {
        const column: GridEnrichedBySchemaColDef = {
            field,
            headerName: schema[field].label,
            hide: hiddenFields.includes(field),
            creatable: creatableFields.includes(field) || schema[field].creatable,
            disabled: disabledFields.includes(field),
            width: 100,
        };
        return column;
    });
};

export const getAutoComplete = async ({
    model,
    serverEndPoint,
}: {
    model: string;
    serverEndPoint: serverEndPointType | null;
}) => {
    const data = await getData({ path: `${model}/`, serverEndPoint, route: 'autocomplete' });
    if ('isAxiosError' in data) {
        return false;
    }
    return <Item[]>data;
};

export const loginByPayload = async (payload: Item, serverEndPoint: serverEndPointType | null) => {
    if (!serverEndPoint) {
        return <AxiosError>{
            message: noServerEndPointMessage,
            isAxiosError: true,
        };
    }
    const url = serverEndPoint.getToken;
    if (!url) {
        console.log('Erro no loginByPayload: faltou a configuração de url getToken!');
        return false;
    }
    try {
        const { data } = await axios.post(url, payload);
        localStorage.setItem('refreshToken', data.refresh);
        setAuthToken(data.access);
        return true;
    } catch (e) {
        console.log('Erro no loginByPayload!', e);
        setAuthToken(null);
        return false;
    }
};

export const clearJWT = () => {
    localStorage.removeItem('token');
    return false;
};

export const hasJWT = () => {
    const token = localStorage.getItem('token');
    if (token) {
        return true;
    }
    return false;
};

export const setAuthToken = (token: string | null) => {
    if (token) {
        localStorage.setItem('token', token);
        axios.defaults.headers.common.Authorization = `Bearer ${token}`;
        return;
    }
    localStorage.removeItem('token');
    localStorage.removeItem('refreshToken');
    delete axios.defaults.headers.common.Authorization;
};

const refreshToken = async (serverEndPoint: serverEndPointType | null) => {
    if (!serverEndPoint) {
        return <AxiosError>{
            message: noServerEndPointMessage,
            isAxiosError: true,
        };
    }
    const refreshToken = localStorage.getItem('refreshToken');
    // console.log('entrou refreshToken', refreshToken);
    setAuthToken(null);
    if (!refreshToken) {
        return false;
    }
    try {
        const { data } = await axios.post(`${serverEndPoint.refreshToken}`, {
            refresh: refreshToken,
        });
        setAuthToken(data.access);
        return true;
    } catch (e) {
        console.log('Failed refreshing token', e);
        return false;
    }
};

export const isLoggedIn = async (serverEndPoint: serverEndPointType | null) => {
    const token = localStorage.getItem('token');
    setAuthToken(token);
    if (!token) {
        return false;
    }
    const usuaria = await getData({ path: 'minhaconta/', serverEndPoint });
    if ('isAxiosError' in usuaria) {
        console.log('Erro ao recuperar dados de usuária!');
        return false;
    }
    return <Item>usuaria;
};

export const getSignUpOptions = async (serverEndPoint: serverEndPointType | null) => {
    if (!serverEndPoint || !serverEndPoint.signUp) {
        return <AxiosError>{
            message: noServerEndPointMessage,
            isAxiosError: true,
        };
    }
    const url = serverEndPoint.signUp;
    try {
        const { data } = await axios.options<SchemaOptionsSingleActionType>(url);
        return data.action.POST;
    } catch (e) {
        const message = `Error fetching options from ${url}`;
        console.log(message, e);
        return <AxiosError>{
            message,
            isAxiosError: true,
        };
    }
};

export const signUp = async (data: Item, serverEndPoint: serverEndPointType | null) => {
    if (!serverEndPoint || !serverEndPoint.signUp) {
        return <AxiosError>{
            message: noServerEndPointMessage,
            isAxiosError: true,
        };
    }
    const url = serverEndPoint.signUp;
    try {
        const { data: responseData } = await axios.post<Item>(url, data);
        return responseData;
    } catch (e) {
        if (axios.isAxiosError(e)) {
            const err = <AxiosError>e;
            const message = `Error trying signUp at ${url}`;
            if (err.response?.status === 401) {
                const isRefreshed = await refreshToken(serverEndPoint);
                if (!isRefreshed) {
                    console.log(expiredTokenMessage);
                    return <AxiosError>{
                        message: expiredTokenMessage,
                        isAxiosError: true,
                    };
                }
                try {
                    const { data: responseData } = await axios.post<Item>(url, data);
                    return responseData;
                } catch (e) {
                    console.log(message, data, e);
                    return <AxiosError>{
                        message,
                        isAxiosError: true,
                    };
                }
            }
            console.log(message, data, e);
            return <AxiosError>{
                message,
                isAxiosError: true,
            };
        }
    }
    console.log(unknownError, data);
    return <AxiosError>{
        message: unknownError,
        isAxiosError: true,
    };
};

export const getGenericModelList = async ({
    model,
    serverEndPoint,
    id = '',
    relatedModel = '',
    relatedModelId = '',
    columnFields,
    hiddenFields = ['id'],
    creatableFields = [],
    disabledFields = [],
    isInBatches = false,
    loadedSchema,
    loadedModelOptions,
    page,
    filter,
    queryParams = [],
    sort,
    sumRows,
}: GetGenericModelListProps) => {
    const newQueryParams = [...queryParams];
    let path = `${model}/${id}`;
    let schemaPath = `${model}/`;
    let schema = loadedSchema;
    let modelOptions = loadedModelOptions;
    let columns: GridEnrichedBySchemaColDef[] | false = [];
    if (!isTmpId(id) && relatedModel) {
        path += `/${relatedModel}/`;
        if (relatedModelId) {
            path += `${relatedModelId}/`;
        }
        schemaPath += `${id}/${relatedModel}/`;
    }
    // SERVER-SIDE TOTALS (sumRows):
    if (sumRows) {
        const sumRowsParams = sumRows.rows.map((row) => row.field).join(',');
        newQueryParams.push(`sum_rows=${sumRowsParams}`);
    }
    // SERVER-SIDE FILTERING:
    if (filter) {
        if (
            filter.quickFilterValues &&
            filter.quickFilterValues.length > 0 &&
            filter.quickFilterValues[0]
        ) {
            newQueryParams.push(`search=${filter.quickFilterValues[0]}`);
        }
        for (const item of filter.items) {
            if (!item.operatorValue) {
                continue;
            }
            const queryParam = item.value
                ? `columnField=${item.columnField}&operatorValue=${item.operatorValue}&value=${item.value}`
                : `columnField=${item.columnField}&operatorValue=${item.operatorValue}`;
            newQueryParams.push(queryParam);
        }
    }
    // SERVER-SIDE SORTING:
    if (sort) {
        const sortParams: string[] = [];
        for (const item of sort) {
            sortParams.push(item.sort === 'desc' ? `-${item.field}` : item.field);
        }
        newQueryParams.push(`ordering=${sortParams.join(',')}`);
    }
    // SERVER-SIDE PAGINATION:
    if (page) {
        newQueryParams.push(`page=${page + 1}`);
    }

    if (newQueryParams.length > 0) {
        path += `?${newQueryParams.join('&')}`;
    }

    // Only get schema and columns if not in batches or in first batch:
    if (!schema) {
        const options = await getSchema(schemaPath, serverEndPoint);
        if (!options) {
            return false;
        }
        schema = options.schema;
        modelOptions = options.modelOptions;
        columns = getDataGridColumns(
            schema,
            columnFields,
            hiddenFields,
            creatableFields,
            disabledFields
        );
        if (!columns) {
            return false;
        }
    }

    let rowCount = 0;
    let sumRowsTotals: Record<string, number> | null = null;
    let data: Item[] = [];

    if (!id || (id && !relatedModelId)) {
        if (isInBatches) {
            path += loadedSchema ? '?is_last_batch=1' : '?is_first_batch=1';
        }
        const ret = await getData({ path, serverEndPoint });
        if ('isAxiosError' in ret) {
            return false;
        }

        const dataRaw: Item[] = 'results' in ret ? ret.results : ret;
        if ('results' in ret) {
            rowCount = ret.count;
            if (sumRows) {
                sumRowsTotals = ret.sum_rows;
            }
        }

        data = dataRaw.map((row) => {
            const newRow: Item = {};
            for (const [key, field] of Object.entries(schema as SchemaType)) {
                if (!(key in row)) {
                    continue;
                }
                if (field.type === 'choice') {
                    newRow[key] = row[key]
                        ? {
                              value: row[key],
                              display_name: getChoiceByValue(row[key], field.choices),
                          }
                        : emptyByType(field);
                    continue;
                }
                newRow[key] = row[key] ? row[key] : emptyByType(field);
            }
            return newRow;
        });
    }
    if (loadedSchema) {
        // DEBUG console.log({ path, data });
        return <DataSchemaColumnsType>{ data, rowCount, sumRowsTotals };
    }
    // DEBUG  console.log({ path, data, columns, schema });
    return <DataSchemaColumnsType>{ data, columns, schema, modelOptions, rowCount, sumRowsTotals };
};

export const getGenericModel = async ({
    model,
    serverEndPoint,
    id = '',
    relatedModel = '',
    relatedModelId = '',
}: {
    model: string;
    serverEndPoint: serverEndPointType | null;
    id?: Id;
    relatedModel?: string;
    relatedModelId?: string;
}) => {
    let path = `${model}/${id}`;
    let schemaPath = `${model}/`;
    if (id && relatedModel) {
        path += `/${relatedModel}/`;
        if (relatedModelId) {
            path += `${relatedModelId}/`;
        }
        schemaPath += `${id}/${relatedModel}/`;
    } else if (id) {
        path += '/';
    }
    const options = await getSchema(schemaPath, serverEndPoint);
    if (!options) {
        return false;
    }
    const { schema, modelOptions } = options;
    let data: Item = {};
    if (id && (!relatedModel || (relatedModel && relatedModelId))) {
        const response = await getData({ path, serverEndPoint });
        if (!('isAxiosError' in response)) {
            data = <Item>response;
        }
    }

    return <ItemSchemaColumnsType>{ schema, modelOptions, data };
};

export const getAllModels = async (serverEndPoint: serverEndPointType | null) => {
    if (!serverEndPoint) {
        return <AxiosError>{
            message: noServerEndPointMessage,
            isAxiosError: true,
        };
    }
    const data = await getData({ path: 'endpoints/', serverEndPoint });
    return 'isAxiosError' in data ? [] : <Item[]>data;
};
