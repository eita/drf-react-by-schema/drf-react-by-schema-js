import React from 'react';
import { Theme } from '@mui/material/styles';

import defaultTheme from '../styles/theme';

export interface serverEndPointType {
    url: string;
    apiTokenUrl: string;
    api?: string;
    signUp?: string;
    autocomplete?: string;
    getToken?: string;
    refreshToken?: string;
    verifyToken?: string;
}
export interface DRFReactBySchemaContextType {
    serverEndPoint: serverEndPointType | null;
    theme: Theme;
    isInBatches?: boolean;
    firstBatchLength?: number;
}

export const DRFReactBySchemaContext = React.createContext<DRFReactBySchemaContextType>({
    serverEndPoint: null,
    theme: defaultTheme,
    isInBatches: true,
    firstBatchLength: 100,
});

export const useDRFReactBySchema = () => {
    const context = React.useContext(DRFReactBySchemaContext);
    return context;
};
