import React from 'react';
import { ThemeProvider } from '@mui/material/styles';
import CssBaseline from '@mui/material/CssBaseline';

import defaultTheme from '../styles/theme';
import Overlays from './Overlays';
import {
    DRFReactBySchemaContextType,
    serverEndPointType,
    DRFReactBySchemaContext,
} from './DRFReactBySchemaContext';

interface DRFReactBySchemaProviderProps extends DRFReactBySchemaContextType {
    children: React.ReactNode;
}

/**
 *
 *
 * @param {*} props
 * @returns {*}
 */
const DRFReactBySchemaProvider = ({
    serverEndPoint,
    theme,
    isInBatches,
    firstBatchLength,
    children,
}: DRFReactBySchemaProviderProps) => {
    if (serverEndPoint) {
        const defaultKeys = [
            'autocomplete',
            'api',
            'getToken',
            ['refreshToken', 'refresh'],
            ['verifyToken', 'verify'],
        ];
        for (const key of defaultKeys) {
            if (key === 'getToken') {
                serverEndPoint[key] = serverEndPoint.apiTokenUrl;
                continue;
            }
            const hybridKey =
                typeof key === 'string'
                    ? (key as keyof serverEndPointType)
                    : (key[0] as keyof serverEndPointType);
            const hybridUrl = typeof key === 'string' ? key : key[1];
            if (serverEndPoint[hybridKey]) {
                continue;
            }
            serverEndPoint[hybridKey] = ['refreshToken', 'verifyToken'].includes(hybridKey)
                ? `${serverEndPoint.apiTokenUrl}${hybridUrl}/`
                : `${serverEndPoint.url}/${hybridUrl}`;
        }
    }

    const mergedTheme = theme ? { ...defaultTheme, ...theme } : defaultTheme;

    return (
        <ThemeProvider theme={mergedTheme}>
            <CssBaseline />
            <DRFReactBySchemaContext.Provider
                value={{
                    serverEndPoint,
                    theme: mergedTheme,
                    isInBatches,
                    firstBatchLength,
                }}>
                <Overlays children={children} />
            </DRFReactBySchemaContext.Provider>
        </ThemeProvider>
    );
};

export default DRFReactBySchemaProvider;
