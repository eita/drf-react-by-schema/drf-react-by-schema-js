import React, { useState, useReducer, useEffect, useRef } from 'react';
import { FieldValues } from 'react-hook-form';
import { AnyObjectSchema } from 'yup';

import {
    Id,
    Item,
    reducer,
    SchemaType,
    populateValues,
    buildGenericYupValidationSchema,
    getTmpId,
    isTmpId,
    modelOptionsType,
    OptionsAC,
    OnSuccessEvent,
    TargetApiParamsLocal,
    TargetApiPostParamsLocal,
    GetGenericModelListPropsLocal,
} from '../utils';
import DialogActions from '../components/DialogActions';
import { useDRFReactBySchema } from './DRFReactBySchemaContext';
import {
    isLoggedIn,
    getRawData,
    getAutoComplete,
    getGenericModel,
    getGenericModelList,
    getAllModels,
    updateDataBySchema,
    addExistingRelatedModel,
    deleteData,
    loginByPayload,
    getSignUpOptions,
    signUp,
    updateData,
    createData,
} from '../api';
import {
    APIWrapperContext,
    DialogType,
    DRFReactBySchemaSubmitHandler,
    LoadSinglePageDataProps,
    OnDeleteRelatedModelType,
    onEditModelDataGridSaveType,
    OnEditModelType,
    OnEditRelatedModelType,
    PageFormType,
    SnackBarType,
} from './APIWrapperContext';
import DialogFormBySchema from '../components/forms/DialogFormBySchema';

interface APIWrapperProps {
    handleLoading: (x: boolean) => void;
    setSnackBar: (x: Partial<SnackBarType>) => void;
    setDialog: (x: Partial<DialogType>) => void;
    children: React.ReactNode;
}
const initialPageForm: PageFormType = {
    id: '',
    schema: null,
    initialValues: null,
    validationSchema: null,
};

function APIWrapper({ handleLoading, setSnackBar, setDialog, children }: APIWrapperProps) {
    const { serverEndPoint } = useDRFReactBySchema();

    const [usuaria, setUsuaria] = useState<Item | null>(null);
    const [optionsAC, setOptionsAC] = useReducer(reducer<OptionsAC>, null);
    const [pageForm, setPageForm] = useReducer(reducer<PageFormType>, initialPageForm);
    const editModel = useRef<Item>({});

    const updateUsuaria = () => {
        setUsuaria(null);
        isLoggedIn(serverEndPoint).then((usuaria) => {
            setUsuaria(usuaria || { erro: 'token inválido' });
        });
    };

    useEffect(() => {
        updateUsuaria();
    }, []);

    useEffect(() => {
        setPageForm(initialPageForm);
    }, []);

    const onTriggerSnackBar = ({ msg, severity = 'info' }: SnackBarType) => {
        setSnackBar({
            open: true,
            msg,
            severity,
        });
    };

    const loadSinglePageData = async ({
        model,
        objId,
        optionsACModels = null,
        defaults = {},
        extraValidators = {},
    }: LoadSinglePageDataProps) => {
        handleLoading(true);
        if (!objId || objId === 'novo') {
            objId = getTmpId();
        }

        const object = await getGenericModel({
            model,
            serverEndPoint,
            id: isTmpId(objId) ? null : objId,
        });

        if (object === false) {
            setPageForm({ schema: undefined, id: '' });
            console.log('Houve um erro ao tentar carregar os dados!');
            return false;
        }

        handleLoading(false);

        if (optionsACModels) {
            populateOptionsAC(optionsACModels);
        }

        for (const [field, value] of Object.entries(defaults)) {
            if (!object.data[field]) {
                object.data[field] = value;
            }
        }

        const values = populateInitialValues({
            id: objId,
            extraValidators,
            ...object,
        });

        return values;
    };

    const onSubmit: DRFReactBySchemaSubmitHandler = async (
        model: string,
        id: Id,
        data: FieldValues
    ) => {
        if (!pageForm || !pageForm.schema) {
            console.log('there must be a pageForm!');
            return false;
        }
        handleLoading(true);
        const response = await updateDataBySchema({
            model,
            modelObjectId: id,
            serverEndPoint,
            data,
            schema: pageForm.schema,
        });
        handleLoading(false);
        if ('isAxiosError' in response) {
            onTriggerSnackBar({
                msg: 'Houve um problema ao salvar seus dados! Por favor, entre em contato',
                severity: 'error',
            });
            console.log({
                msg: 'Error saving model',
                errors: response,
                data,
            });
            return false;
        }

        if (optionsAC) {
            populateOptionsAC(Object.keys(optionsAC));
        }

        onTriggerSnackBar({
            msg: id ? 'Dados atualizados com sucesso!' : 'Criado com sucesso!',
        });

        return response.id;
    };

    const populateOptionsAC = async (optionsACModels: string[]) => {
        const promises: Promise<false | Item[]>[] = [];
        for (const model of optionsACModels) {
            promises.push(localGetAutoComplete(model));
        }
        const results = await Promise.all(promises);
        const newOptionsAC: OptionsAC = {};
        results.map((result, index) => {
            if (result !== false) {
                newOptionsAC[optionsACModels[index]] = result;
            }
        });
        setOptionsAC(newOptionsAC);
    };

    interface PopulateInitialValuesProps {
        // model: string;
        id: Id;
        data: Item;
        schema: SchemaType;
        modelOptions: modelOptionsType;
        extraValidators: Item;
        isEditModel?: boolean;
    }
    const populateInitialValues = ({
        id,
        extraValidators,
        ...object
    }: PopulateInitialValuesProps) => {
        const values = populateValues(object);
        const yupSchema = buildGenericYupValidationSchema({
            ...object,
            data: values,
            extraValidators,
        });
        setPageForm({
            // model,
            id,
            schema: object.schema,
            // modelOptions: object.modelOptions,
            initialValues: values,
            validationSchema: yupSchema,
            forceReload: false,
        });
        return values;
    };

    const onEditModelSave = async (data: Item) => {
        setDialog({ open: false });
        handleLoading(true);
        const { fieldKey, index, model, id, labelKey, setValue, getValues, schema } =
            editModel.current;

        const response = await updateDataBySchema({
            model,
            modelObjectId: id,
            serverEndPoint,
            data,
            schema,
        });

        if ('isAxiosError' in response) {
            onTriggerSnackBar({
                msg: 'Houve um problema ao salvar seus dados.',
                severity: 'error',
            });
            console.log({
                msg: 'Error saving model',
                response,
                data,
            });
            return false;
        }

        onTriggerSnackBar({
            msg: 'Alterações salvas com sucesso!',
            severity: 'info',
        });
        if (setValue && getValues) {
            const targetKey = fieldKey && index >= 0 ? `${fieldKey}.${index}.${model}` : model;
            const value = getValues(targetKey);
            const newValue = Array.isArray(value)
                ? value.map((item) => {
                      return item.id === data.id ? data : item;
                  })
                : {
                      ...value,
                      ...data,
                      label: data[labelKey],
                  };
            setValue(targetKey, newValue);
            populateOptionsAC([model]);
        }
        handleLoading(false);
        return true;
    };

    const onEditModelDataGridSave = async ({
        model,
        newRow,
        schema,
    }: onEditModelDataGridSaveType) => {
        const response = await updateDataBySchema({
            model,
            modelObjectId: newRow.id,
            serverEndPoint,
            data: newRow,
            schema,
            path: model,
        });
        if ('isAxiosError' in response) {
            onTriggerSnackBar({
                msg: 'Não foi possível salvar os dados. Confira os erros.',
                severity: 'error',
            });
            console.log({
                msg: 'Error saving model',
                response,
                data: newRow,
            });
            return false;
        }

        onTriggerSnackBar({
            msg: 'Alterações salvas com sucesso!',
            severity: 'info',
        });
        return response;
    };

    const onEditModel = ({
        fieldKey,
        index,
        model,
        id,
        labelKey,
        setValue,
        getValues,
        fieldsLayout,
    }: OnEditModelType) => {
        setDialog({
            open: true,
            loading: true,
        });
        getGenericModel({ model, serverEndPoint, id }).then((result) => {
            if (result === false) {
                setDialog({
                    open: true,
                    loading: false,
                    title: 'Falha ao carregar o formulário',
                    Body: <>Não foi possível carregar o formulário!</>,
                });
                return false;
            }
            const values = populateValues(result);
            const yupSchema = buildGenericYupValidationSchema({
                schema: result.schema,
                data: values,
            }) as AnyObjectSchema;
            setDialog({
                open: true,
                loading: false,
                title: 'Editar',
                Body: (
                    <DialogFormBySchema
                        schema={result.schema}
                        validationSchema={yupSchema}
                        initialValues={values}
                        setDialog={setDialog}
                        getAutoComplete={localGetAutoComplete}
                        onEditModelSave={onEditModelSave}
                        fieldsLayout={fieldsLayout}
                    />
                ),
                Actions: <></>,
            });
            editModel.current = {
                fieldKey,
                index,
                model,
                id,
                labelKey,
                setValue,
                getValues,
                schema: result.schema,
            };
        });
    };

    const onDeleteModel = (model: string, id: Id, onSuccess?: OnSuccessEvent) => {
        setDialog({
            open: true,
            loading: false,
            title: 'Apagar',
            Body: 'Tem certeza de que deseja apagar este item?',
            Actions: (
                <DialogActions
                    setDialog={setDialog}
                    handleSave={(e: React.SyntheticEvent) => {
                        return onDeleteModelSave(model, id, onSuccess ? onSuccess(e) : undefined);
                    }}
                    btnConfirm="Sim, apagar"
                />
            ),
        });
    };

    const onDeleteModelSave = async (model: string, id: Id, onSuccess?: OnSuccessEvent) => {
        setDialog({ open: false });
        handleLoading(true);
        const ret = await deleteData(model, serverEndPoint, id);
        if (ret === true) {
            onTriggerSnackBar({
                msg: 'Apagado com com sucesso!',
                severity: 'info',
            });
            if (onSuccess) {
                onSuccess({} as React.BaseSyntheticEvent);
            }
            return true;
        }

        handleLoading(false);
        onTriggerSnackBar({
            msg: 'Houve um problema ao remover o item! Por favor, entre em contato.',
            severity: 'error',
        });
        return false;
    };

    const onEditRelatedModelSave = async ({
        model,
        id,
        relatedModel,
        newRow,
        schema,
        onlyAddExisting,
    }: OnEditRelatedModelType) => {
        const updateUrl = `${model}/${id}/${relatedModel}`;
        if (onlyAddExisting) {
            const response = await addExistingRelatedModel({
                model,
                id,
                serverEndPoint,
                data: {
                    onlyAddExisting: {
                        key: relatedModel,
                        value: newRow.id_to_add,
                    },
                },
            });
            if ('isAxiosError' in response) {
                console.log(response);
                onTriggerSnackBar({
                    msg: 'Houve um problema ao salvar a alteração! Por favor, entre em contato.',
                    severity: 'error',
                });
                return false;
            }
            onTriggerSnackBar({
                msg: 'Alterações salvas com sucesso!',
                severity: 'info',
            });
            const object = await getGenericModel({
                model,
                id,
                serverEndPoint,
                relatedModel,
                relatedModelId: newRow.id_to_add,
            });
            return object;
        }
        // This is important for related data
        if (schema[model] && !newRow[model]) {
            newRow[model] = id;
        }
        const response = await updateDataBySchema({
            model: relatedModel,
            modelObjectId: newRow.id,
            serverEndPoint,
            data: newRow,
            schema,
            path: updateUrl,
        });
        if ('isAxiosError' in response) {
            onTriggerSnackBar({
                msg: 'Não foi possível salvar os dados. Confira os erros.',
                severity: 'error',
            });
            console.log({
                error: response,
                data: newRow,
            });
            return false;
        }

        onTriggerSnackBar({
            msg: 'Alterações salvas com sucesso!',
            severity: 'info',
        });
        return response;
    };

    const onDeleteRelatedModel = async ({
        model,
        id,
        relatedModel,
        relatedModelId,
    }: OnDeleteRelatedModelType) => {
        const deleteUrl = `${model}/${id}/${relatedModel}`;
        const response = await deleteData(deleteUrl, serverEndPoint, relatedModelId);
        if (response !== true) {
            onTriggerSnackBar({
                msg: 'Houve um problema ao remover o item! Por favor, entre em contato.',
                severity: 'error',
            });
            console.log({
                error: response,
                id,
            });
            return false;
        }

        onTriggerSnackBar({
            msg: 'Alterações salvas com sucesso!',
            severity: 'info',
        });
        return response;
    };

    if (!serverEndPoint) {
        console.error('There must be a serverEndPoint properly configured for apiWrapper to work!');
        return <>{children}</>;
    }

    function localGetRawData(route: string) {
        return getRawData({ path: route, serverEndPoint });
    }

    function localGetAutoComplete(model: string) {
        return getAutoComplete({ model, serverEndPoint });
    }

    function localGetGenericModelList(params: GetGenericModelListPropsLocal) {
        return getGenericModelList({ ...params, serverEndPoint });
    }

    async function localGetAllModels() {
        const response = await getAllModels(serverEndPoint);
        if ('isAxiosError' in response) {
            return [];
        }
        return response as Item[];
    }

    async function localLoginByPayload(payload: Item) {
        const response = await loginByPayload(payload, serverEndPoint);
        if (typeof response !== 'boolean') {
            console.log({
                error: response,
                payload,
            });
            return false;
        }
        return response;
    }

    async function localSignUp(data: Item) {
        const response = await signUp(data, serverEndPoint);
        if ('isAxiosError' in response) {
            return false;
        }
        return response;
    }

    async function localGetSignupOptions() {
        const response = await getSignUpOptions(serverEndPoint);
        if ('isAxiosError' in response) {
            return false;
        }
        return response;
    }

    async function localUpdateModel(params: TargetApiParamsLocal) {
        const response = await updateData({ ...params, serverEndPoint });
        if ('isAxiosError' in response) {
            return false;
        }
        return response;
    }

    async function postData(params: TargetApiPostParamsLocal) {
        const response = await createData({ ...params, serverEndPoint });
        if ('isAxiosError' in response) {
            return false;
        }
        return response;
    }

    return (
        <APIWrapperContext.Provider
            value={{
                usuaria,
                updateUsuaria,
                onSubmit,
                loadSinglePageData,
                handleLoading,
                optionsACState: [optionsAC, setOptionsAC],
                pageFormState: [pageForm, setPageForm],
                onEditModel,
                onEditModelSave,
                onEditModelDataGridSave,
                onDeleteModel,
                onEditRelatedModelSave,
                onDeleteRelatedModel,
                onTriggerSnackBar,
                setDialog,
                // api utils:
                getRawData: localGetRawData,
                getAutoComplete: localGetAutoComplete,
                getGenericModelList: localGetGenericModelList,
                getAllModels: localGetAllModels,
                loginByPayload: localLoginByPayload,
                getSignUpOptions: localGetSignupOptions,
                signUp: localSignUp,
                postData: postData,
                // Remove after integrating new "onEditModel" to package:
                serverEndPoint,
                editModel,
                updateModel: localUpdateModel,
                populateOptionsAC,
            }}>
            {children}
        </APIWrapperContext.Provider>
    );
}

export default React.memo(APIWrapper);
