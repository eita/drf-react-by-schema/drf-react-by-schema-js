import React, { useState, useReducer } from 'react';
import Backdrop from '@mui/material/Backdrop';
import CircularProgress from '@mui/material/CircularProgress';
import Snackbar from '@mui/material/Snackbar';
import Alert from '@mui/material/Alert';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';
import Box from '@mui/material/Box';

import { reducer } from '../utils';
import APIWrapper from './APIWrapper';
import { DialogType, SnackBarType } from './APIWrapperContext';

export default function Overlays({ children }: { children: React.ReactNode }) {
    const initialSnackBar: SnackBarType = {
        open: false,
        msg: '',
        severity: 'info',
    };

    const initialDialog: DialogType = {
        open: false,
        loading: true,
        title: '',
        Body: null,
        Actions: null,
    };
    const [snackBar, setSnackBar] = useReducer(reducer<SnackBarType>, initialSnackBar);
    const [dialog, setDialog] = useReducer(reducer<DialogType>, initialDialog);
    const [loading, setLoading] = useState<boolean>(false);

    // give some time to stop loading when data is retrieved and must render before loading rendering:
    const handleLoading = (loadingState: boolean) => {
        if (loadingState) {
            setLoading(true);
            return;
        }
        setTimeout(() => {
            setLoading(false);
        }, 300);
    };

    const handleDialogClose = () => {
        setDialog({
            open: false,
        });
    };

    return (
        <>
            <APIWrapper
                handleLoading={handleLoading}
                setSnackBar={setSnackBar}
                setDialog={setDialog}
                children={children}
            />
            <Backdrop
                invisible={false}
                sx={{ color: '#fff', zIndex: (theme) => theme.zIndex.drawer + 1 }}
                open={loading}>
                <CircularProgress />
            </Backdrop>
            {snackBar && (
                <Snackbar
                    open={snackBar.open}
                    autoHideDuration={3000}
                    onClose={() => {
                        setSnackBar({ open: false });
                    }}
                    anchorOrigin={{ vertical: 'top', horizontal: 'right' }}>
                    <Alert severity={snackBar.severity}>{snackBar.msg}</Alert>
                </Snackbar>
            )}
            {dialog && (
                <Dialog open={dialog.open} onClose={handleDialogClose}>
                    <DialogTitle>{dialog.loading ? 'Carregando...' : dialog.title}</DialogTitle>
                    <DialogContent>
                        {dialog.loading ? (
                            <Box sx={{ display: 'flex' }}>
                                <CircularProgress />
                            </Box>
                        ) : (
                            <>{dialog.Body}</>
                        )}
                    </DialogContent>
                    <DialogActions>{!dialog.loading && <>{dialog.Actions}</>}</DialogActions>
                </Dialog>
            )}
        </>
    );
}
