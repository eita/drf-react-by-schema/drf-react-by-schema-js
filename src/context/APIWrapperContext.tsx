import React from 'react';
import { FieldValues, SubmitHandler, UseFormGetValues, UseFormSetValue } from 'react-hook-form';
import { AlertColor, AlertPropsColorOverrides } from '@mui/material/Alert';
import { OverridableStringUnion } from '@mui/types';
import {
    ItemSchemaColumnsType,
    Id,
    Item,
    SchemaType,
    DataSchemaColumnsType,
    OnSuccessEvent,
    OptionsAC,
    GenericValue,
    FormFieldLayout,
    TargetApiParamsLocal,
    TargetApiPostParamsLocal,
    GetGenericModelListPropsLocal,
} from '../utils';
import { serverEndPointType } from './DRFReactBySchemaContext';

export interface LoadSinglePageDataProps {
    model: string;
    objId?: Id;
    objTitleField?: string;
    optionsACModels: string[] | null;
    defaults?: Item;
    basePath?: string;
    formPath?: string | null;
    extraValidators?: Item;
}

export interface PageFormType {
    id: Id;
    schema: SchemaType | null;
    initialValues: Item | null;
    validationSchema: Item | null;
    forceReload?: boolean;
}

export interface OnEditModelType {
    fieldKey?: string;
    index?: number;
    model: string;
    id: Id;
    labelKey: string;
    setValue?: UseFormSetValue<FieldValues>;
    getValues?: UseFormGetValues<FieldValues>;
    fieldsLayout?: FormFieldLayout[];
}

export interface OnEditRelatedModelType {
    model: string;
    id: Id;
    relatedModel: string;
    relatedModelId: Id;
    newRow: Item;
    schema: SchemaType;
    onlyAddExisting: boolean;
}

export interface onEditModelDataGridSaveType {
    model: string;
    id: Id;
    newRow: Item;
    schema: SchemaType;
}

export interface OnDeleteRelatedModelType {
    model: string;
    id: Id;
    relatedModel: string;
    relatedModelId: Id;
}

export interface SnackBarType {
    open?: boolean;
    msg?: string;
    severity?: OverridableStringUnion<AlertColor, AlertPropsColorOverrides>;
}

export interface DialogType {
    open: boolean;
    loading?: boolean;
    title?: string;
    Body?: React.ReactNode;
    Actions?: React.ReactNode;
}

export type DRFReactBySchemaSubmitHandler = (
    ...args: [...[model: string, id: Id], ...Parameters<SubmitHandler<FieldValues>>]
) => ReturnType<SubmitHandler<FieldValues>>;

export interface APIWrapperContextType {
    usuaria: Item | null;
    updateUsuaria: () => void;
    onSubmit: DRFReactBySchemaSubmitHandler;
    loadSinglePageData: (p: LoadSinglePageDataProps) => Promise<false | Item>;
    handleLoading: (p: boolean) => void;
    optionsACState: [OptionsAC | null, (x: Partial<OptionsAC>) => void];
    pageFormState: [PageFormType | null, (x: Partial<PageFormType>) => void];
    onEditModel: (p: OnEditModelType) => void;
    onEditModelDataGridSave: (p: onEditModelDataGridSaveType) => Promise<false | Item>;
    onEditModelSave: (p: Item) => Promise<boolean>;
    onDeleteModel: (model: string, id: Id, onSuccess?: OnSuccessEvent) => void;
    onEditRelatedModelSave: (
        p: OnEditRelatedModelType
    ) => Promise<false | Item | ItemSchemaColumnsType>;
    onDeleteRelatedModel: (p: OnDeleteRelatedModelType) => Promise<boolean>;
    onTriggerSnackBar: (p: SnackBarType) => void;
    setDialog: (x: Partial<DialogType>) => void;
    // api utils:
    getRawData: (route: string) => Promise<GenericValue>;
    getAutoComplete: (model: string) => Promise<false | Item[]>;
    getGenericModelList: (
        x: GetGenericModelListPropsLocal
    ) => Promise<false | DataSchemaColumnsType>;
    getAllModels: () => Promise<Item[]>;
    loginByPayload: (payload: Item) => Promise<boolean>;
    getSignUpOptions: () => Promise<false | SchemaType>;
    signUp: (data: Item) => Promise<false | Item>;
    postData: (params: TargetApiPostParamsLocal) => Promise<false | Item>;
    // Remove after integrating new "onEditModel" to package:
    serverEndPoint: serverEndPointType;
    editModel: React.MutableRefObject<Item>;
    updateModel: (p: TargetApiParamsLocal) => Promise<false | Item>;
    populateOptionsAC: (optionsACModels: string[]) => void;
}

export const APIWrapperContext = React.createContext<APIWrapperContextType>({
    usuaria: null,
    updateUsuaria: () => undefined,
    onSubmit: () => undefined,
    loadSinglePageData: async () => false,
    handleLoading: () => undefined,
    optionsACState: [null, () => undefined],
    pageFormState: [null, () => undefined],
    onEditModel: () => undefined,
    onEditModelDataGridSave: async () => false,
    onEditModelSave: async () => false,
    onDeleteModel: () => undefined,
    onEditRelatedModelSave: async () => false,
    onDeleteRelatedModel: async () => false,
    onTriggerSnackBar: () => undefined,
    setDialog: () => undefined,
    // api utils:
    getRawData: async () => undefined,
    getAutoComplete: async () => [],
    getGenericModelList: async () => false,
    getAllModels: async () => [],
    loginByPayload: async () => false,
    getSignUpOptions: async () => false,
    signUp: async () => false,
    postData: async () => false,
    // Remove after integrating new "onEditModel" to package:
    serverEndPoint: { url: '', apiTokenUrl: '' },
    editModel: { current: {} },
    updateModel: async () => false,
    populateOptionsAC: () => undefined,
});

export const useAPIWrapper = () => {
    const context = React.useContext(APIWrapperContext);
    return context;
};
