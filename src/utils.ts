import * as Yup from 'yup';
import { ReactElement } from 'react';
import { GridActionsColDef, GridColDef, GridFilterModel, GridSortModel } from '@mui/x-data-grid';
import { Control, FieldValues, UseFormGetValues, UseFormSetValue } from 'react-hook-form';
import { AlertColor, AutocompleteRenderOptionState, SxProps } from '@mui/material';
import { OnEditModelType } from './context/APIWrapperContext';
import { serverEndPointType } from './context/DRFReactBySchemaContext';
import { DateView } from '@mui/x-date-pickers';

export type Id = string | number | null;
// eslint-disable-next-line @typescript-eslint/no-explicit-any
export type GenericValue = any;
export type Item = Record<string, GenericValue>;
export type OptionsAC = Record<string, Item[]>;
export type PaginatedResult = {
    count: number;
    next: number;
    previous: number;
    sum_rows: null | Record<string, number>;
    results: Item[];
};
export interface PaginationModel {
    page: number;
    pageSize: number;
    filter?: GridFilterModel;
    sort?: GridSortModel;
}
export type SchemaType = Record<string, Field>;
export type SchemaOptionsType = {
    name: string;
    description: string;
    actions: { POST: SchemaType };
    renders: string[];
    parses: string[];
    verbose_name: string;
    verbose_name_plural: string;
};
export type SchemaOptionsSingleActionType = Omit<SchemaOptionsType, 'actions'> & {
    action: { POST: SchemaType };
};
export type modelOptionsType = Omit<SchemaOptionsType, 'actions' | 'renders' | 'parses'>;
export type ChoiceValue = string | number;
export interface Choice {
    value: ChoiceValue;
    display_name: string;
}
type FieldChild = Record<string, SchemaType>;
export interface Field {
    type: string;
    child?: FieldChild;
    children?: SchemaType;
    model_default?: GenericValue;
    model_required?: boolean;
    choices?: Choice[];
    max_length?: number | string;
    max_digits?: number;
    decimal_places?: number;
    label: string;
    read_only?: boolean;
    is_currency?: boolean;
    prefix?: string;
    suffix?: string;
    creatable?: boolean;
    related_editable?: boolean;
    validators_regex?: Item[];
    many?: boolean;
    date_views?: DateView[];
    required?: boolean;
    disabled?: boolean;
    help_text?: string;
    model_multiline?: boolean;
}
interface GridBySchemaColDef extends GridColDef {
    isIndexField?: boolean;
    creatable?: boolean;
    disabled?: boolean;
    orderable?: boolean;
    patternFormat?: string;
}
interface GridActionsBySchemaColDef extends GridActionsColDef {
    isIndexField?: boolean;
    creatable?: boolean;
    disabled?: boolean;
    orderable?: boolean;
    patternFormat?: string;
}
export type GridEnrichedBySchemaColDef = GridBySchemaColDef | GridActionsBySchemaColDef;
export interface DataSchemaColumnsType {
    data: Item[];
    schema: SchemaType;
    modelOptions: modelOptionsType;
    rowCount?: number;
    sumRowsTotals?: null | Record<string, number>;
    columns?: GridEnrichedBySchemaColDef[];
}
export interface ItemSchemaColumnsType {
    data: Item;
    schema: SchemaType;
    modelOptions: modelOptionsType;
    columns?: GridEnrichedBySchemaColDef[];
}

export interface CommonFieldProps {
    value?: GenericValue;
    multiline?: boolean;
    setValue?: UseFormSetValue<FieldValues>;
    getValues?: UseFormGetValues<FieldValues>;
    fieldKey?: string;
    labelKey?: string;
    index?: number;
    optionsAC?: OptionsAC;
    optionsModel?: string;
    getOptionLabel?: (option: Item | string) => string;
    renderOption?: (
        props: React.HTMLAttributes<HTMLLIElement>,
        option: Item,
        state: AutocompleteRenderOptionState
    ) => React.ReactNode;
    onEditModel?: (x: OnEditModelType) => void;
    fieldsLayout?: FormFieldLayout[];
    multiple?: boolean;
    sx?: SxProps;
    options?: Item[];
    isSemaphoric?: boolean;
    label?: string;
    onValueChange?: (x: GenericValue) => void;
    decimalScale?: number;
    isPassword?: boolean;
    type?: React.HTMLInputTypeAttribute;
    autocomplete?: string;
    minRows?: number;
    optionIdKey?: string;
    optionLabelKey?: string;
    disabled?: boolean;
}

export interface FieldBySchemaProps extends Omit<CommonFieldProps, 'value'> {
    name: string;
    schema: SchemaType;
    control: Control;
    errors: Item;
}

export interface DetailFieldBySchemaProps {
    name: string;
    schema: SchemaType;
    value: GenericValue;
    labelKey?: string;
    decimalScale?: number;
    optionIdKey?: string;
    optionLabelKey?: string;
    sxField?: SxProps;
    sxLabel?: SxProps;
    sxValue?: SxProps;
    sxValueList?: SxProps;
    sxValueListItem?: SxProps;
    sxValueListItemText?: SxProps;
}

export const emptyByType: GenericValue = (field: Field, forDatabase = false) => {
    if (field.model_default) {
        return field.model_default;
    }
    switch (field.type) {
        case 'nested object':
            // emptyByType must be an empty object for the database, but must be null for the mui-autocomplete component. So I had to make this ugly hack here, when we're preparing to sendo to the database:
            return forDatabase ? {} : null;
        case 'field':
            if (field.child) {
                return [];
            }
            return forDatabase ? undefined : null;
        case 'string':
        case 'email':
            return '';
        case 'integer':
            return 0;
        case 'array':
            return [];
        case 'boolean':
            return false;
        default:
            return null;
    }
};

export const getChoiceByValue = (value: ChoiceValue, choices: Choice[] | undefined) => {
    if (!choices) {
        return null;
    }
    for (const choice of choices) {
        if (choice.value === value) {
            return choice.display_name;
        }
    }
};

export const populateValues = ({ data, schema }: { data: Item; schema: SchemaType }) => {
    const values: Item = {};
    for (const [key, field] of Object.entries(schema)) {
        if (key === 'id' && isTmpId(data[key])) {
            continue;
        }

        if (!data[key]) {
            values[key] = emptyByType(field);
            continue;
        }

        if (field.type === 'field' && field.child) {
            if (Array.isArray(data[key])) {
                const arValues = [];
                for (const row of data[key]) {
                    const value = populateValues({
                        data: row,
                        schema: field.child.children,
                    });
                    arValues.push(value);
                }
                values[key] = arValues;
                continue;
            }

            values[key] = populateValues({
                data: data[key],
                schema: field.child.children,
            });
            continue;
        }

        if (field.type === 'choice') {
            values[key] = data[key]
                ? {
                      value: data[key],
                      display_name: getChoiceByValue(<ChoiceValue>data[key], field.choices),
                  }
                : emptyByType(field);
            continue;
        }

        values[key] = data[key];
    }
    // console.log(values);
    return values;
};

const getYupValidator = (type: string) => {
    let yupFunc;
    try {
        switch (type) {
            case 'slug':
                yupFunc = Yup.string();
                break;
            case 'email':
                yupFunc = Yup.string().email('Este campo deve ser um e-mail válido.');
                break;
            case 'integer':
                yupFunc = Yup.number().integer('Este campo deve ser um número inteiro');
                break;
            case 'choice':
                yupFunc = Yup.object();
                break;
            case 'field':
                yupFunc = Yup.mixed();
                break;
            case 'nested object':
                yupFunc = Yup.object();
                break;
            case 'date':
                yupFunc = Yup.date();
                break;
            case 'string':
                yupFunc = Yup.string();
                break;
            case 'number':
            case 'decimal':
                yupFunc = Yup.number();
                break;
            case 'boolean':
                yupFunc = Yup.bool();
                break;
            case 'array':
                yupFunc = Yup.array();
                break;
            case 'object':
                yupFunc = Yup.object();
                break;
            default:
                yupFunc = Yup.string();
                break;
        }
    } catch (e) {
        yupFunc = Yup.string();
    }
    return yupFunc.nullable();
};

export const buildGenericYupValidationSchema = ({
    data,
    schema,
    many = false,
    skipFields = [],
    extraValidators = {},
}: {
    data: Item;
    schema: SchemaType;
    many?: boolean;
    skipFields?: string[];
    extraValidators?: Item;
}) => {
    const yupValidator: Item = {};
    for (const entry of Object.entries(schema)) {
        const [key, field] = entry;

        if (!data || !(key in data) || key === 'id' || skipFields.includes(key)) {
            continue;
        }

        // console.log({ key, field, data: data[key] });

        // OneToMany or ManyToMany:
        if (field.type === 'field' && field.child) {
            yupValidator[key] = buildGenericYupValidationSchema({
                schema: field.child.children,
                many: true,
                data: data[key],
                extraValidators: Object.prototype.hasOwnProperty.call(extraValidators, key)
                    ? extraValidators[key]
                    : {},
            });
            continue;
        }

        // Nested Object:
        if (field.type === 'nested object' && field.children) {
            // yupValidator[key] = buildGenericYupValidationSchema({
            //     schema: field.children,
            //     many: false,
            //     data: data[key],
            //     extraValidators: Object.prototype.hasOwnProperty.call(extraValidators, key)
            //         ? extraValidators[key]
            //         : {}
            // });
            // if (!field.model_required) {
            //     yupValidator[key] = yupValidator[key].nullable();
            // }
            // continue;
        }

        yupValidator[key] = Object.prototype.hasOwnProperty.call(extraValidators, key)
            ? extraValidators[key]
            : getYupValidator(field.type);

        if (field.model_required) {
            yupValidator[key] = yupValidator[key].required('Este campo é obrigatório');
        }
        if (field.max_length) {
            yupValidator[key] = yupValidator[key].max(
                parseInt(field.max_length as string),
                `Este campo só pode ter no máximo ${field.max_length} caracteres`
            );
        }
        if (field.max_digits && field.type === 'decimal') {
            const maxDigits = field.max_digits as number;
            yupValidator[key] = yupValidator[key].test(
                'len',
                `Este número pode ter no máximo ${maxDigits} dígitos`,
                (val: number) => {
                    if (!val) {
                        return true;
                    }
                    return val.toString().length <= maxDigits;
                }
            );
        }
        if (!field.read_only && field.validators_regex) {
            for (const validator of field.validators_regex) {
                yupValidator[key] = yupValidator[key].matches(validator.regex, validator.message);
            }
        }
    }
    // console.log({ yupValidator });
    return many
        ? Yup.array().of(Yup.object().shape(yupValidator))
        : Yup.object().shape(yupValidator);
};

export const errorProps = ({
    errors,
    fieldKey,
    fieldKeyProp,
    index,
}: {
    errors: Item;
    fieldKey: string;
    fieldKeyProp: string;
    index?: number;
}) => {
    let error;
    let helperText;
    if (index) {
        const hasErrors =
            errors &&
            errors[fieldKey] &&
            errors[fieldKey][index] &&
            Boolean(errors[fieldKey][index][fieldKeyProp]);
        error = hasErrors;
        helperText = hasErrors ? errors[fieldKey][index][fieldKeyProp].message : null;
        return { error, helperText };
    }

    const hasErrors = errors && errors[fieldKey] && Boolean(errors[fieldKey][fieldKeyProp]);
    error = hasErrors;
    helperText = hasErrors ? errors[fieldKey][fieldKeyProp].message : null;
    return { error, helperText };
};

export const getTmpId = () => {
    return 'tmp' + Math.floor(Math.random() * 1000000);
};

export const isTmpId = (id: string | number | undefined | null) => {
    if (!id) {
        return true;
    }
    return id.toString().slice(0, 3) === 'tmp';
};

export function reducer<T>(state: T | null, newState: Partial<T> | null) {
    if (newState === null) {
        return null;
    }
    if (state === null) {
        return newState as T;
    }
    return { ...state, ...newState };
}

export const getPatternFormat = (type: string) => {
    let format = '';
    switch (type) {
        case 'telefone':
        case 'fone':
        case 'phone':
        case 'contact':
        case 'contato':
            format = '(##)#####-####';
            break;
        case 'cpf':
            format = '###.###.###-##';
            break;
        case 'cnpj':
            format = '##.###.###/####-##';
            break;
        case 'cep':
            format = '##.###-###';
            break;
    }
    return format;
};

export type AddParametersToEnd<
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    TFunction extends (...args: any) => any,
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    TParameters extends [...args: any],
> = (...args: [...Parameters<TFunction>, ...TParameters]) => ReturnType<TFunction>;

export function buildDateFormatBySchema(dateViews: string[] | null | undefined) {
    const defaultFormat = 'DD/MM/YYYY';
    if (!dateViews) {
        return defaultFormat;
    }

    const hasDay = dateViews.includes('day');
    const hasMonth = dateViews.includes('month');
    const hasYear = dateViews.includes('year');
    if (hasDay && hasMonth && hasYear) {
        return defaultFormat;
    }
    if (!hasDay && hasMonth && hasYear) {
        return 'MM/YYYY';
    }
    if (!hasDay && !hasMonth && hasYear) {
        return 'YYYY';
    }
    if (!hasDay && hasMonth && !hasYear) {
        return 'MM';
    }
    if (hasDay && !hasMonth && !hasYear) {
        return 'DD';
    }

    return defaultFormat;
}

export const slugToCamelCase = (str: string, includeFirst = false) => {
    if (!str) {
        return '';
    }
    const ret = str.replace(/_([a-z])/g, function (g) {
        return g[1].toUpperCase();
    });
    if (includeFirst) {
        return ret[0].toUpperCase() + ret.slice(1);
    }
    return ret;
};

export const slugify = (text: string | null) => {
    if (!text) {
        return '';
    }
    return text
        .toString()
        .normalize('NFD')
        .replace(/[\u0300-\u036f]/g, '')
        .toLowerCase()
        .trim()
        .replace(/\s+/g, '-')
        .replace(/[^\w-]+/g, '')
        .replace(/--+/g, '-');
};

export function mergeFilterItems(
    defaultFilter: GridFilterModel | undefined,
    filter: GridFilterModel | undefined
) {
    if (!filter && defaultFilter) {
        return defaultFilter;
    }
    if (!defaultFilter && filter) {
        return filter;
    }
    if (filter && defaultFilter) {
        const items = filter.items;
        const defaultItems = defaultFilter.items;
        const mergedItems = defaultItems.map((defaultItem) => {
            const itemFound = items.find((item) => item.columnField === defaultItem.columnField);
            if (itemFound) {
                return itemFound;
            }
            return defaultItem;
        });
        const unMergedItems = items.filter(
            (item) =>
                !defaultItems
                    .map((defaultItem) => defaultItem.columnField)
                    .includes(item.columnField)
        );
        return { ...filter, items: [...mergedItems, ...unMergedItems] };
    }
    return undefined;
}

export type ActionType = 'editInline' | 'remove' | 'edit' | 'view';
export interface CustomAction {
    key: string;
    icon: ReactElement;
    label: string;
    handleClick: (item: Item | undefined) => undefined;
}

export type BulkUpdateData = (newData: Item[]) => Promise<{ id: Id; success: boolean }[]>;
export type BulkDeleteData = (ids: Id[]) => Promise<{ id: Id; success: boolean }[]>;

export type OnSelectActionCustom = (selectedData: Item[], bulkUpdateData: BulkUpdateData) => void;

export type OnSelectActionTypes = OnSelectActionCustom | 'bulkDelete' | 'bulkCreate';

export type OnSelectActions = {
    title: string;
    action: OnSelectActionTypes;
};

interface CustomFormField {
    key: string;
    CustomElement: (x: CommonFieldProps) => JSX.Element;
}

export interface FormFieldLayout {
    title?: string;
    rows?: (string | (string | CustomFormField)[] | CustomFormField)[];
    CustomElement?: React.ReactNode;
}

export interface ExtraSxCommonFieldProps {
    sxRow?: SxProps;
    sxRowMultiple?: SxProps;
    sxField?: SxProps;
    sxLabel?: SxProps;
    sxValue?: SxProps;
    sxValueList?: SxProps;
    sxValueListItem?: SxProps;
    sxValueListItemText?: SxProps;
}

export type DetailCommonFieldProps = CommonFieldProps & ExtraSxCommonFieldProps;

interface CustomDetailField {
    key: string;
    CustomElement: (x: DetailCommonFieldProps) => JSX.Element;
}

export interface DetailFieldLayout {
    title?: string;
    rows?: (string | (string | CustomDetailField)[] | CustomDetailField)[];
    CustomElement?: React.ReactNode;
}

export interface DetailBySchemaProps extends ExtraSxCommonFieldProps {
    values: Item;
    schema: SchemaType;
    editLink?: string;
    editLabel?: string;
    labelKey?: string;
    decimalScale?: number;
    fieldsLayout?: DetailFieldLayout[];
    fieldsProps?: Record<string, DetailCommonFieldProps>;
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export type OnSuccessEvent = (e: React.BaseSyntheticEvent) => any;

export interface TargetApiParams {
    path: string;
    serverEndPoint: serverEndPointType | null;
    data: Item;
    id: Id;
}

export type TargetApiParamsLocal = Omit<TargetApiParams, 'serverEndPoint'>;

export type TargetApiPostParams = Omit<TargetApiParams, 'id'>;

export type TargetApiPostParamsLocal = Omit<TargetApiPostParams, 'serverEndPoint'>;

export interface SumRowsType {
    rows: SumRowsItem[];
    severity?: AlertColor;
}

interface SumRowsItem {
    field: string;
    prefix?: string;
    suffix?: string;
    isCount?: boolean;
}

export interface GetGenericModelListProps {
    model: string;
    serverEndPoint: serverEndPointType | null;
    id?: Id;
    relatedModel?: string;
    relatedModelId?: Id;
    columnFields: string[];
    hiddenFields?: string[];
    creatableFields?: string[];
    disabledFields?: string[];
    isInBatches?: boolean;
    loadedSchema?: SchemaType | boolean;
    loadedModelOptions?: modelOptionsType | boolean;
    page?: number;
    filter?: GridFilterModel;
    queryParams?: string[];
    sort?: GridSortModel;
    sumRows?: SumRowsType;
}

export type GetGenericModelListPropsLocal = Omit<GetGenericModelListProps, 'serverEndPoint'>;
