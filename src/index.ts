// Methods:
import {
    updateData,
    partialUpdateData,
    createData,
    deleteData,
    createOrUpdateData,
    updateDataBySchema,
    addExistingRelatedModel,
    getAutoComplete,
    loginByPayload,
    setAuthToken,
    isLoggedIn,
    getGenericModelList,
    getGenericModel,
    hasJWT,
    clearJWT,
} from './api';
import {
    emptyByType,
    getChoiceByValue,
    populateValues,
    buildGenericYupValidationSchema,
    errorProps,
    getTmpId,
    isTmpId,
    getPatternFormat,
    slugToCamelCase,
} from './utils';

// Wrappers/Providers:
import DRFReactBySchemaProvider from './context/DRFReactBySchemaProvider';
import { useDRFReactBySchema } from './context/DRFReactBySchemaContext';
import { useAPIWrapper } from './context/APIWrapperContext';

// Forms:
// import Form from './context/Form';
import FormBySchema from './components/forms/FormBySchema';
import DialogFormBySchema from './components/forms/DialogFormBySchema';

// Components:
import DataGridBySchemaEditable from './components/DataGridBySchemaEditable';
import GenericModelList from './components/GenericModelList';
import GenericRelatedModelList from './components/GenericRelatedModelList';
import DataTotals from './components/DataTotals';
import DialogActions from './components/DialogActions';

// DataGridBySchema Components:
import { SelectEditInputCell } from './components/DataGridBySchemaEditable/SelectEditInputCell';
import { GridDateInput } from './components/DataGridBySchemaEditable/GridDateInput';
import { GridDecimalInput } from './components/DataGridBySchemaEditable/GridDecimalInput';
import { GridPatternInput } from './components/DataGridBySchemaEditable/GridPatternInput';
import { BooleanInputCell } from './components/DataGridBySchemaEditable/BooleanInputCell';

// FormComponents
import FieldBySchema from './components/forms/FieldBySchema';
import TextFieldBySchema from './components/forms/inputs/TextFieldBySchema';
import BooleanFieldBySchema from './components/forms/inputs/BooleanFieldBySchema';
import DesktopDatePickerBySchema from './components/forms/inputs/DesktopDatePickerBySchema';
import DesktopDateTimePickerBySchema from './components/forms/inputs/DesktopDateTimePickerBySchema';
import EditableAutocompleteFieldBySchema from './components/forms/inputs/EditableAutocompleteFieldBySchema';
import AutocompleteFieldBySchema from './components/forms/inputs/AutocompleteFieldBySchema';
import FloatFieldBySchema from './components/forms/inputs/FloatFieldBySchema';

// DetailComponents
import DetailBySchema from './components/details/DetailBySchema';
import DetailFieldBySchema from './components/details/DetailFieldBySchema';

export {
    // Wrappers/Providers:
    DRFReactBySchemaProvider,
    useAPIWrapper,
    useDRFReactBySchema,

    // Forms:
    // Form,
    FormBySchema,
    DialogFormBySchema,
    // FormContext,

    // Components:
    DataGridBySchemaEditable,
    GenericModelList,
    GenericRelatedModelList,
    DataTotals,
    DialogActions,

    // DataGridBySchema Components:
    SelectEditInputCell,
    GridDateInput,
    GridDecimalInput,
    GridPatternInput,
    BooleanInputCell,

    // FormComponents:
    FieldBySchema,
    TextFieldBySchema,
    BooleanFieldBySchema,
    DesktopDatePickerBySchema,
    DesktopDateTimePickerBySchema,
    EditableAutocompleteFieldBySchema,
    AutocompleteFieldBySchema,
    FloatFieldBySchema,

    // DetailComponents:
    DetailBySchema,
    DetailFieldBySchema,

    // Methods:
    updateData,
    partialUpdateData,
    createData,
    deleteData,
    createOrUpdateData,
    updateDataBySchema,
    addExistingRelatedModel,
    getAutoComplete,
    loginByPayload,
    setAuthToken,
    isLoggedIn,
    getGenericModelList,
    getGenericModel,
    emptyByType,
    getChoiceByValue,
    populateValues,
    buildGenericYupValidationSchema,
    errorProps,
    getTmpId,
    isTmpId,
    getPatternFormat,
    clearJWT,
    hasJWT,
    slugToCamelCase,
};
