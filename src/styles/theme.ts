import type {} from '@mui/x-data-grid/themeAugmentation';
import { createTheme } from '@mui/material/styles';
import { ptBR as corePtBR } from '@mui/material/locale';
import { ptBR } from '@mui/x-data-grid';
import { ptBR as pickersPtBR } from '@mui/x-date-pickers';

declare module '@mui/material/styles' {
    // interface Theme {
    //     status: {
    //         danger: React.CSSProperties['color'];
    //     };
    // }
    interface Palette {
        avatars: string[];
        avatarsMore: string[];
        topBarButton: Palette['primary'];
        producao: Palette['primary'];
        empreendimento: Palette['primary'];
        comercializacao: Palette['primary'];
        credito: Palette['primary'];
        certificacao: Palette['primary'];
        successButton: Palette['primary'];
        selectedItem: Palette['primary'];
        tableColumnHeader: Palette['primary'];
        formCard: Palette['primary'];
    }
    // interface PaletteOptions {
    //     neutral: PaletteOptions['primary'];
    // }
    //
    interface PaletteColor {
        semaphoric?: string;
    }
    // interface SimplePaletteColorOptions {
    //     darker?: string;
    // }
    // interface ThemeOptions {
    //     status: {
    //         danger: React.CSSProperties['color'];
    //     };
    // }
}

ptBR.components.MuiDataGrid.defaultProps.localeText = {
    ...ptBR.components.MuiDataGrid.defaultProps.localeText,
    toolbarQuickFilterPlaceholder: 'Buscar...',
    toolbarQuickFilterLabel: 'Buscar',
    toolbarQuickFilterDeleteIconLabel: 'Limpar busca',
};

const palette = {
    avatars: [
        '#e60049',
        '#0bb4ff',
        '#50e991',
        '#e6d800',
        '#9b19f5',
        '#ffa300',
        '#dc0ab4',
        '#b3d4ff',
        '#00bfa0',
    ],
    avatarsMore: [
        '#023fa5',
        '#8e063b',
        '#d33f6a',
        '#11c638',
        '#ef9708',
        '#0fcfc0',
        '#f79cd4',
        '#7d87b9',
        '#bb7784',
        '#4a6fe3',
        '#8595e1',
        '#b5bbe3',
        '#e6afb9',
        '#e07b91',
        '#8dd593',
        '#c6dec7',
        '#ead3c6',
        '#f0b98d',
        '#9cded6',
        '#d5eae7',
        '#f3e1eb',
        '#f6c4e1',
        '#bec1d4',
        '#d6bcc0',
    ],
    topBarButton: {
        main: '#ffffff',
    },
    producao: {
        main: '#e60049',
        contrastText: '#fff',
    },
    empreendimento: {
        main: '#0bb4ff',
        contrastText: '#fff',
    },
    comercializacao: {
        main: '#ffa300',
        contrastText: '#fff',
    },
    credito: {
        main: '#dc0ab4',
        contrastText: '#fff',
    },
    certificacao: {
        main: '#9b19f5',
        contrastText: '#fff',
    },
    background: {
        default: '#D9D9D9',
    },
    primary: {
        main: '#3949AB',
    },
    secondary: {
        main: '#9ca4d5',
        contrastText: '#fff',
    },
    successButton: {
        main: '#CDDC39',
        contrastText: '#fff',
    },
    success: {
        main: '#0e0',
        contrastText: '#fff',
        semaphoric: '#0e0',
    },
    error: {
        main: '#d32f2f',
        semaphoric: '#f66',
    },
    warning: {
        main: '#ee0',
        semaphoric: '#ee0',
    },
    selectedItem: {
        main: '#2962FF',
    },
    tableColumnHeader: {
        main: '#ECEFF1',
    },
    formCard: {
        main: '#FFF',
    },
};

const defaultTheme = createTheme(
    {
        palette,
        components: {
            MuiButton: {
                styleOverrides: {
                    contained: {
                        // borderRadius: 50
                    },
                },
            },
            MuiTextField: {
                styleOverrides: {
                    root: {
                        '&.Mui-required .MuiFormLabel-asterisk': {
                            // color: '#F00'
                        },
                    },
                },
            },
            MuiListItemButton: {
                styleOverrides: {
                    root: {
                        '&.Mui-selected, :hover': {
                            color: palette.selectedItem.main,
                            '& .MuiListItemIcon-root': {
                                color: palette.selectedItem.main,
                            },
                        },
                        '&.Mui-selected:hover': {
                            color: palette.selectedItem.main,
                            '& .MuiListItemIcon-root': {
                                color: palette.selectedItem.main,
                            },
                        },
                        '&.disabled': {
                            opacity: 0.5,
                        },
                    },
                },
            },
            MuiDataGrid: {
                styleOverrides: {
                    root: {
                        backgroundColor: palette.formCard.main,
                        '& .MuiDataGrid-columnHeaderTitle': {
                            overflow: 'visible',
                            lineHeight: '1.43rem',
                            whiteSpace: 'normal',
                        },
                    },
                    cell: {
                        // backgroundColor: 'yellow'
                    },
                    columnHeader: {
                        backgroundColor: palette.tableColumnHeader.main,
                    },
                },
            },
            MuiTabs: {
                styleOverrides: {
                    root: {
                        backgroundColor: palette.formCard.main,
                        marginTop: 20,
                    },
                },
            },
        },
    },
    corePtBR,
    ptBR,
    pickersPtBR
);

export default defaultTheme;
