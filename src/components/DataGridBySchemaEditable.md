```js
/*
import DRFReactBySchemaProvider from '../context/DRFReactBySchemaProvider';
const data = {
    data: [],
    columns: [],
    schema: {}
};
const model = 'example';
const indexField = null;
const indexFieldBasePath = null;
const isAutoHeight = true;
const hideFooterPagination = false;
const finalCustomColumnOperations = null;
const customLinkDestination = null;
const setVisibleRows = null;
const setData = (newData) => {
    return '';
};
const Link = null;
<DRFReactBySchemaProvider
    serverEndPoint={{
        url: 'https://icv.eita.org.br/api',
        apiTokenUrl: 'https://icv.eita.org.br/api-auth/token'
    }}
>
    <DataGridBySchemaEditable
        data={data.data}
        columns={data.columns}
        schema={data.schema}
        model={model}
        indexField={indexField}
        indexFieldBasePath={indexFieldBasePath}
        isEditable={false}
        isAutoHeight={isAutoHeight}
        hideFooterPagination={hideFooterPagination}
        customColumnOperations={finalCustomColumnOperations}
        customLinkDestination={customLinkDestination}
        setVisibleRows={setVisibleRows}
        onDataChange={newData => {
            setData({
                ...data,
                data: newData
            });
        }}
        LinkComponent={Link}
    />
</DRFReactBySchemaProvider>
*/
```