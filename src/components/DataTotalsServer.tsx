import React from 'react';
import Alert from '@mui/material/Alert';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import { NumericFormat } from 'react-number-format';

import { SumRowsType } from '../utils';

interface DataTotalsServerProps {
    sumRows?: SumRowsType;
    totals?: null | Record<string, number>;
}

const DataTotalsServer = ({ sumRows, totals }: DataTotalsServerProps) => {
    return (
        <>
            {sumRows && sumRows.rows.length > 0 && (
                <Alert severity={sumRows.severity || 'info'}>
                    <List dense={true}>
                        {sumRows.rows.map((row) => (
                            <ListItem key={`sumRows_${row.field}`}>
                                <NumericFormat
                                    value={totals ? totals[`${row.field}_total`] : 0}
                                    thousandSeparator="."
                                    decimalSeparator=","
                                    displayType={'text'}
                                    decimalScale={row.isCount ? 0 : 2}
                                    fixedDecimalScale={true}
                                    prefix={row.prefix}
                                    suffix={row.suffix}
                                />
                            </ListItem>
                        ))}
                    </List>
                </Alert>
            )}
        </>
    );
};

export default DataTotalsServer;
