import React, { useState, useEffect } from 'react';
import Box from '@mui/material/Box';
import Card from '@mui/material/Card';
import CardHeader from '@mui/material/CardHeader';
import CardContent from '@mui/material/CardContent';
import Backdrop from '@mui/material/Backdrop';
import CircularProgress from '@mui/material/CircularProgress';
import { GridFilterModel, GridRowId } from '@mui/x-data-grid';

import DataGridBySchemaEditable from './DataGridBySchemaEditable';
import DataTotals from './DataTotals';
import DataTotalsServer from './DataTotalsServer';
import { useAPIWrapper } from '../context/APIWrapperContext';
import { OnEditModelType, OnDeleteRelatedModelType } from '../context/APIWrapperContext';
import {
    ActionType,
    CustomAction,
    DataSchemaColumnsType,
    GetGenericModelListProps,
    GridEnrichedBySchemaColDef,
    Id,
    Item,
    OnSelectActions,
    OptionsAC,
    PaginationModel,
    SumRowsType,
    mergeFilterItems,
} from '../utils';
import { getGenericModelList } from '../api';
import { Layout } from '../styles';

interface ContentTableProps {
    data: DataSchemaColumnsType;
    relatedModel: string;
    model: string;
    loading?: boolean;
    id: Id;
    indexField: string;
    indexFieldBasePath?: string;
    indexFieldViewBasePath?: string;
    addExistingModel?: string;
    isEditable?: boolean;
    onEditModel: (p: OnEditModelType) => void;
    onDeleteRelatedModel: (p: OnDeleteRelatedModelType) => Promise<boolean>;
    finalCustomColumnOperations: (
        p: GridEnrichedBySchemaColDef
    ) => GridEnrichedBySchemaColDef | Promise<GridEnrichedBySchemaColDef>;
    setVisibleRows?: (p: GridRowId[]) => void;
    visibleRows: GridRowId[];
    isAutoHeight?: boolean;
    hideFooterPagination: boolean;
    hideFooterComponent?: boolean;
    hideToolbarComponent?: boolean;
    onProcessRow?: (p: Item) => void;
    onDataChange?: (p: Item[]) => void;
    sumRows?: SumRowsType;
    paginationMode: 'server' | 'client';
    paginationModel?: PaginationModel;
    setPaginationModel?: (x: PaginationModel) => void;
    hasBulkSelect?: boolean;
    onSelectActions?: OnSelectActions[];
    tableAutoHeight?: boolean;
    actions?: Partial<ActionType>[];
    customActions?: CustomAction[];
    optionsAC?: OptionsAC;
    defaultValues?: Item;
    LinkComponent?: any | null;
}

const ContentTable = ({
    data,
    relatedModel,
    model,
    loading,
    id,
    indexField,
    indexFieldBasePath,
    indexFieldViewBasePath,
    addExistingModel,
    isEditable,
    onEditModel,
    finalCustomColumnOperations,
    setVisibleRows,
    isAutoHeight,
    hideFooterPagination,
    hideFooterComponent,
    hideToolbarComponent,
    onProcessRow,
    onDataChange,
    sumRows,
    visibleRows,
    paginationMode = 'client',
    paginationModel,
    setPaginationModel,
    hasBulkSelect,
    onSelectActions,
    tableAutoHeight,
    actions,
    customActions,
    optionsAC,
    defaultValues,
    LinkComponent,
}: ContentTableProps) => (
    <>
        {data.columns ? (
            <Box sx={tableAutoHeight ? {} : Layout.dataGridFixedHeight}>
                <DataGridBySchemaEditable
                    data={data.data}
                    columns={data.columns}
                    schema={data.schema}
                    model={relatedModel}
                    modelParent={model}
                    modelParentId={id}
                    loading={loading}
                    indexField={indexField}
                    indexFieldBasePath={indexFieldBasePath}
                    indexFieldViewBasePath={indexFieldViewBasePath}
                    addExistingModel={addExistingModel}
                    isEditable={isEditable}
                    hasBulkSelect={hasBulkSelect}
                    onSelectActions={onSelectActions}
                    onEditModel={onEditModel}
                    isAutoHeight={isAutoHeight}
                    tableAutoHeight={tableAutoHeight}
                    customColumnOperations={finalCustomColumnOperations}
                    setVisibleRows={setVisibleRows}
                    hideFooterPagination={hideFooterPagination}
                    hideFooterComponent={hideFooterComponent}
                    hideToolbarComponent={hideToolbarComponent}
                    actions={actions}
                    customActions={customActions}
                    optionsAC={optionsAC}
                    defaultValues={defaultValues}
                    onProcessRow={onProcessRow}
                    onDataChange={onDataChange}
                    paginationModel={paginationMode === 'server' ? paginationModel : undefined}
                    setPaginationModel={
                        paginationMode === 'server' ? setPaginationModel : undefined
                    }
                    rowCount={
                        paginationMode === 'client'
                            ? undefined
                            : typeof data.rowCount !== undefined
                            ? data.rowCount
                            : 0
                    }
                    LinkComponent={LinkComponent}
                />
            </Box>
        ) : (
            <Backdrop
                invisible={true}
                sx={{ color: '#fff', zIndex: (theme) => theme.zIndex.drawer + 1 }}
                open={loading || false}>
                <CircularProgress />
            </Backdrop>
        )}
        {paginationMode === 'client' ? (
            <DataTotals data={data.data} sumRows={sumRows} visibleRows={visibleRows} />
        ) : (
            <DataTotalsServer sumRows={sumRows} totals={data.sumRowsTotals} />
        )}
    </>
);

interface GenericRelatedModelListProps {
    model: string;
    id: Id;
    relatedModel: string;
    columnFields: string[];
    hiddenFields: string[];
    creatableFields: string[];
    disabledFields: string[];
    usuaria?: Item | null;
    minWidthFields?: Record<string, number>;
    indexField?: string;
    indexFieldBasePath?: string;
    indexFieldViewBasePath?: string;
    addExistingModel?: string;
    label: string;
    onProcessRow?: (p: Item) => void;
    reloadAfterRowUpdate?: boolean;
    customColumnOperations?: (
        column: GridEnrichedBySchemaColDef
    ) => GridEnrichedBySchemaColDef | Promise<GridEnrichedBySchemaColDef>;
    isEditable?: boolean;
    hasBulkSelect?: boolean;
    onSelectActions?: OnSelectActions[];
    sumRows?: SumRowsType;
    isAutoHeight?: boolean;
    isInBatches?: boolean;
    noCardWrapper?: boolean;
    paginationMode: 'server' | 'client';
    defaultFilter?: GridFilterModel;
    queryParams?: string[];
    hideFooterComponent?: boolean;
    hideToolbarComponent?: boolean;
    tableAutoHeight?: boolean;
    actions?: Partial<ActionType>[];
    optionsAC?: OptionsAC;
    defaultValues?: Item;
    LinkComponent?: any | null;
}

export default function GenericRelatedModelList({
    model,
    id,
    relatedModel,
    columnFields,
    hiddenFields,
    creatableFields,
    disabledFields,
    usuaria = null,
    minWidthFields,
    indexField,
    indexFieldBasePath,
    indexFieldViewBasePath,
    addExistingModel,
    label,
    onProcessRow,
    reloadAfterRowUpdate,
    customColumnOperations,
    isEditable = true,
    hasBulkSelect = false,
    onSelectActions,
    sumRows,
    isAutoHeight = false,
    isInBatches = true,
    noCardWrapper = false,
    paginationMode = 'client',
    defaultFilter,
    queryParams,
    hideFooterComponent,
    hideToolbarComponent,
    tableAutoHeight,
    actions,
    optionsAC,
    defaultValues,
    LinkComponent,
}: GenericRelatedModelListProps) {
    const [data, setData] = useState<DataSchemaColumnsType | false>(false);
    const [visibleRows, setVisibleRows] = useState<GridRowId[]>([]);
    const [loading, setLoading] = useState(false);
    const [hideFooterPagination, setHideFooterPagination] = useState(false);
    const [paginationModel, setPaginationModel] = useState<PaginationModel | undefined>(
        paginationMode === 'server' ? { page: 0, pageSize: 100 } : undefined
    );

    const { onEditModel, onDeleteRelatedModel, serverEndPoint } = useAPIWrapper();

    const finalCustomColumnOperations = async (column: GridEnrichedBySchemaColDef) => {
        if (minWidthFields) {
            if (Object.prototype.hasOwnProperty.call(minWidthFields, column.field)) {
                column.minWidth = minWidthFields[column.field];
            }
        }

        if (customColumnOperations) {
            return await customColumnOperations(column);
        }

        return column;
    };

    const loadObjectList = async () => {
        setLoading(true);
        switch (paginationMode) {
            case 'client':
                const loadParams = {
                    model,
                    id,
                    relatedModel,
                    indexFieldBasePath,
                    columnFields,
                    creatableFields,
                    disabledFields,
                    hiddenFields,
                    usuaria,
                    isInBatches,
                    queryParams,
                    serverEndPoint,
                };
                const loadedData = await getGenericModelList(loadParams);
                if (loadedData && typeof loadedData !== 'boolean') {
                    setData(loadedData);
                    setLoading(false);
                    if (isInBatches && loadedData.data.length === 100) {
                        setHideFooterPagination(true);
                        getGenericModelList({
                            ...loadParams,
                            loadedSchema: loadedData.schema,
                        }).then((lastBatchData) => {
                            if (lastBatchData) {
                                setData({
                                    ...loadedData,
                                    data: [...loadedData.data, ...lastBatchData.data],
                                });
                                setHideFooterPagination(false);
                            }
                        });
                    }
                    return;
                }
                console.log('error retrieving data!');
                break;
            case 'server':
                const page = paginationModel ? paginationModel.page : 0;
                const filter = mergeFilterItems(
                    defaultFilter,
                    paginationModel ? paginationModel.filter : undefined
                );
                const sort = paginationModel ? paginationModel.sort : undefined;
                const loadPaginatedParams: GetGenericModelListProps = {
                    model,
                    id,
                    relatedModel,
                    columnFields,
                    hiddenFields,
                    creatableFields,
                    disabledFields,
                    page,
                    filter,
                    sort,
                    sumRows,
                    queryParams,
                    serverEndPoint,
                };
                const paginatedData = await getGenericModelList(loadPaginatedParams);
                if (paginatedData && typeof paginatedData !== 'boolean') {
                    setData(paginatedData);
                    setLoading(false);
                    return;
                }
                console.log('error retrieving data!');
                break;
        }
    };

    const onDataChange = (newData: Item[]) => {
        if (!data) {
            return;
        }
        if (reloadAfterRowUpdate) {
            loadObjectList();
            return;
        }

        setData({
            ...data,
            data: newData,
        });
    };

    useEffect(() => {
        loadObjectList();
    }, [id, model, defaultFilter, queryParams]);

    useEffect(() => {
        if (paginationMode === 'server') {
            loadObjectList();
        }
    }, [paginationModel]);

    return (
        <>
            {typeof data !== 'boolean' && data.columns && (
                <>
                    {noCardWrapper ? (
                        <ContentTable
                            data={data}
                            relatedModel={relatedModel}
                            model={model}
                            loading={loading}
                            id={id}
                            indexField={indexField || ''}
                            indexFieldBasePath={indexFieldBasePath}
                            indexFieldViewBasePath={indexFieldViewBasePath}
                            addExistingModel={addExistingModel}
                            isEditable={isEditable}
                            hasBulkSelect={hasBulkSelect}
                            onSelectActions={onSelectActions}
                            onEditModel={onEditModel}
                            onDeleteRelatedModel={onDeleteRelatedModel}
                            finalCustomColumnOperations={finalCustomColumnOperations}
                            setVisibleRows={setVisibleRows}
                            isAutoHeight={isAutoHeight}
                            tableAutoHeight={tableAutoHeight}
                            hideFooterPagination={hideFooterPagination}
                            hideFooterComponent={hideFooterComponent}
                            hideToolbarComponent={hideToolbarComponent}
                            onProcessRow={onProcessRow}
                            onDataChange={onDataChange}
                            sumRows={sumRows}
                            visibleRows={visibleRows}
                            paginationMode={paginationMode}
                            paginationModel={paginationModel}
                            setPaginationModel={setPaginationModel}
                            actions={actions}
                            optionsAC={optionsAC}
                            defaultValues={defaultValues}
                            LinkComponent={LinkComponent}
                        />
                    ) : (
                        <Card sx={Layout.formCard}>
                            <CardHeader title={label} />
                            <CardContent>
                                <ContentTable
                                    data={data}
                                    relatedModel={relatedModel}
                                    model={model}
                                    loading={loading}
                                    id={id}
                                    indexField={indexField || ''}
                                    indexFieldBasePath={indexFieldBasePath}
                                    indexFieldViewBasePath={indexFieldViewBasePath}
                                    addExistingModel={addExistingModel}
                                    isEditable={isEditable}
                                    hasBulkSelect={hasBulkSelect}
                                    onSelectActions={onSelectActions}
                                    onEditModel={onEditModel}
                                    onDeleteRelatedModel={onDeleteRelatedModel}
                                    finalCustomColumnOperations={finalCustomColumnOperations}
                                    setVisibleRows={setVisibleRows}
                                    isAutoHeight={isAutoHeight}
                                    tableAutoHeight={tableAutoHeight}
                                    hideFooterPagination={hideFooterPagination}
                                    hideFooterComponent={hideFooterComponent}
                                    hideToolbarComponent={hideToolbarComponent}
                                    onProcessRow={onProcessRow}
                                    onDataChange={onDataChange}
                                    sumRows={sumRows}
                                    visibleRows={visibleRows}
                                    paginationMode={paginationMode}
                                    paginationModel={paginationModel}
                                    setPaginationModel={setPaginationModel}
                                    actions={actions}
                                    optionsAC={optionsAC}
                                    defaultValues={defaultValues}
                                    LinkComponent={LinkComponent}
                                />
                            </CardContent>
                        </Card>
                    )}
                </>
            )}
        </>
    );
}
