import React, { useState, useEffect } from 'react';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import AddCircleOutlineIcon from '@mui/icons-material/AddCircleOutline';
import Backdrop from '@mui/material/Backdrop';
import CircularProgress from '@mui/material/CircularProgress';
import { GridRowId, GridFilterModel, GridRenderCellParams } from '@mui/x-data-grid';

import DataGridBySchemaEditable from './DataGridBySchemaEditable';
import DataTotals from './DataTotals';
import DataTotalsServer from './DataTotalsServer';
import { Layout } from '../styles';
import {
    GridEnrichedBySchemaColDef,
    DataSchemaColumnsType,
    PaginationModel,
    mergeFilterItems,
    ActionType,
    Item,
    OnSelectActions,
    OptionsAC,
    FormFieldLayout,
    CustomAction,
    GetGenericModelListProps,
    SumRowsType,
} from '../utils';
import { getGenericModelList } from '../api';
import { useDRFReactBySchema } from '../context/DRFReactBySchemaContext';
import { useAPIWrapper } from '../context/APIWrapperContext';

interface GenericModelListProps {
    model: string;
    columnFields: string[];
    hiddenFields?: string[];
    creatableFields?: string[];
    disabledFields?: string[];
    minWidthFields?: Record<string, number>;
    indexField: string;
    indexFieldBasePath: string;
    indexFieldViewBasePath?: string;
    addExistingModel?: string;
    onProcessRow?: (p: Item) => void;
    reloadAfterRowUpdate?: boolean;
    customColumnOperations?: (
        column: GridEnrichedBySchemaColDef
    ) => GridEnrichedBySchemaColDef | Promise<GridEnrichedBySchemaColDef>;
    customFieldFormLayouts?: Record<string, FormFieldLayout[]>;
    customLinkDestination?: (p: GridRenderCellParams) => string;
    isEditable?: boolean;
    hasBulkSelect?: boolean;
    onSelectActions?: OnSelectActions[];
    sumRows?: SumRowsType;
    isAutoHeight?: boolean;
    forceReload?: boolean;
    LinkComponent?: any | null;
    hasHeader: boolean;
    paginationMode: 'server' | 'client';
    defaultFilter?: GridFilterModel;
    queryParams?: string[];
    hideFooterComponent?: boolean;
    hideToolbarComponent?: boolean;
    tableAutoHeight?: boolean;
    actions?: Partial<ActionType>[];
    customActions?: CustomAction[];
    optionsAC?: OptionsAC;
    defaultValues?: Item;
    disableScreenLoading?: boolean;
}

const GenericModelList = ({
    model,
    columnFields,
    hiddenFields = [],
    creatableFields,
    disabledFields,
    minWidthFields,
    indexField,
    indexFieldBasePath,
    indexFieldViewBasePath,
    addExistingModel,
    onProcessRow,
    reloadAfterRowUpdate,
    customColumnOperations,
    customFieldFormLayouts,
    customLinkDestination,
    isEditable,
    hasBulkSelect = false,
    onSelectActions,
    sumRows,
    isAutoHeight = true,
    forceReload = false,
    LinkComponent = null,
    hasHeader = false,
    paginationMode = 'client',
    defaultFilter,
    queryParams,
    hideFooterComponent,
    hideToolbarComponent,
    tableAutoHeight,
    actions,
    customActions,
    optionsAC,
    defaultValues,
    disableScreenLoading,
}: GenericModelListProps) => {
    const { serverEndPoint, isInBatches, firstBatchLength } = useDRFReactBySchema();
    const { onEditModel } = useAPIWrapper();
    const [data, setData] = useState<DataSchemaColumnsType | boolean>(false);
    const [loading, setLoading] = useState(false);
    const [visibleRows, setVisibleRows] = useState<GridRowId[]>([]);
    const [hideFooterPagination, setHideFooterPagination] = useState(false);
    const [paginationModel, setPaginationModel] = useState<PaginationModel | undefined>(
        paginationMode === 'server' ? { page: 0, pageSize: 100 } : undefined
    );

    const finalCustomColumnOperations = async (column: GridEnrichedBySchemaColDef) => {
        if (minWidthFields) {
            if (Object.prototype.hasOwnProperty.call(minWidthFields, column.field)) {
                column.minWidth = minWidthFields[column.field];
            }
        }

        if (customColumnOperations) {
            return await customColumnOperations(column);
        }

        return column;
    };

    const loadObjectList = async () => {
        if (!disableScreenLoading) {
            setLoading(true);
        }
        switch (paginationMode) {
            case 'client':
                const loadParams: GetGenericModelListProps = {
                    model,
                    serverEndPoint,
                    columnFields,
                    hiddenFields,
                    creatableFields,
                    disabledFields,
                    isInBatches,
                    queryParams,
                };
                const loadedData = await getGenericModelList(loadParams);
                if (loadedData && typeof loadedData !== 'boolean') {
                    setData(loadedData);
                    setLoading(false);
                    if (isInBatches && loadedData.data.length === firstBatchLength) {
                        setHideFooterPagination(true);
                        getGenericModelList({
                            ...loadParams,
                            loadedSchema: loadedData.schema,
                            loadedModelOptions: loadedData.modelOptions,
                        }).then((lastBatchData) => {
                            if (lastBatchData && typeof lastBatchData !== 'boolean') {
                                setData({
                                    ...loadedData,
                                    data: [...loadedData.data, ...lastBatchData.data],
                                });
                            }
                            setHideFooterPagination(false);
                        });
                    }
                    return;
                }
                console.log('error retrieving data!');
                return;
            case 'server':
                const page = paginationModel ? paginationModel.page : 0;
                const filter = mergeFilterItems(
                    defaultFilter,
                    paginationModel ? paginationModel.filter : undefined
                );
                const sort = paginationModel ? paginationModel.sort : undefined;
                const loadPaginatedParams: GetGenericModelListProps = {
                    model,
                    serverEndPoint,
                    columnFields,
                    hiddenFields,
                    creatableFields,
                    disabledFields,
                    page,
                    filter,
                    queryParams,
                    sort,
                    sumRows,
                };
                const paginatedData = await getGenericModelList(loadPaginatedParams);
                if (paginatedData && typeof paginatedData !== 'boolean') {
                    setData(paginatedData);
                    setLoading(false);
                    return;
                }
                console.log('error retrieving data!');
                return;
        }
    };

    useEffect(() => {
        if (forceReload) {
            loadObjectList();
        }
    }, [forceReload]);

    useEffect(() => {
        loadObjectList();
    }, [model, defaultFilter, queryParams, hiddenFields]);

    useEffect(() => {
        if (paginationMode === 'server') {
            loadObjectList();
        }
    }, [paginationModel]);

    if (!serverEndPoint) {
        console.error('Error: There is no endpoint defined in DRFReactBySchemaProvider!');
        return <></>;
    }

    return (
        <>
            {typeof data !== 'boolean' && data.columns ? (
                <>
                    {hasHeader && (
                        <Box sx={{ ...Layout.flexRowGrow, mb: 2 }}>
                            <Typography variant="h5">
                                {data.modelOptions.verbose_name_plural || data.modelOptions.name}
                            </Typography>
                            {LinkComponent && (
                                <Box sx={Layout.flexRow}>
                                    <LinkComponent to={`novo`}>
                                        <Button
                                            variant="contained"
                                            size="medium"
                                            sx={{ alignSelf: 'stretch' }}
                                            startIcon={<AddCircleOutlineIcon />}>
                                            Adicionar
                                        </Button>
                                    </LinkComponent>
                                </Box>
                            )}
                        </Box>
                    )}
                    <Box sx={tableAutoHeight ? {} : Layout.dataGridWithTabs}>
                        <DataGridBySchemaEditable
                            data={data.data}
                            columns={data.columns}
                            schema={data.schema || {}}
                            model={model}
                            loading={loading}
                            indexField={indexField}
                            indexFieldBasePath={indexFieldBasePath}
                            indexFieldViewBasePath={indexFieldViewBasePath}
                            addExistingModel={addExistingModel}
                            isEditable={isEditable}
                            hasBulkSelect={hasBulkSelect}
                            onSelectActions={onSelectActions}
                            onEditModel={onEditModel}
                            isAutoHeight={isAutoHeight}
                            tableAutoHeight={tableAutoHeight}
                            customColumnOperations={finalCustomColumnOperations}
                            customFieldFormLayouts={customFieldFormLayouts}
                            setVisibleRows={setVisibleRows}
                            hideFooterPagination={hideFooterPagination}
                            hideFooterComponent={hideFooterComponent}
                            hideToolbarComponent={hideToolbarComponent}
                            customLinkDestination={customLinkDestination}
                            actions={actions}
                            customActions={customActions}
                            optionsAC={optionsAC}
                            defaultValues={defaultValues}
                            onProcessRow={onProcessRow}
                            onDataChange={(newData) => {
                                if (reloadAfterRowUpdate) {
                                    loadObjectList();
                                    return;
                                }

                                setData({
                                    ...data,
                                    data: newData,
                                });
                            }}
                            LinkComponent={LinkComponent}
                            paginationModel={
                                paginationMode === 'server' ? paginationModel : undefined
                            }
                            setPaginationModel={
                                paginationMode === 'server' ? setPaginationModel : undefined
                            }
                            rowCount={
                                paginationMode === 'client'
                                    ? undefined
                                    : typeof data.rowCount !== undefined
                                    ? data.rowCount
                                    : 0
                            }
                        />
                    </Box>
                    {paginationMode === 'client' ? (
                        <DataTotals data={data.data} sumRows={sumRows} visibleRows={visibleRows} />
                    ) : (
                        <DataTotalsServer sumRows={sumRows} totals={data.sumRowsTotals} />
                    )}
                </>
            ) : (
                <Backdrop
                    invisible={true}
                    sx={{ color: '#fff', zIndex: (theme) => theme.zIndex.drawer + 1 }}
                    open={loading}>
                    <CircularProgress />
                </Backdrop>
            )}
        </>
    );
};

export default GenericModelList;
