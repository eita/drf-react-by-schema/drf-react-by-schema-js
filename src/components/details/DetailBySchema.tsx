import React, { useEffect, useState } from 'react';
import Box from '@mui/material/Box';
import Card from '@mui/material/Card';
import CardHeader from '@mui/material/CardHeader';
import CardContent from '@mui/material/CardContent';
import CircularProgress from '@mui/material/CircularProgress';
import { Button, SxProps } from '@mui/material';
import Stack from '@mui/material/Stack';

import DetailFieldBySchema from './DetailFieldBySchema';
import { Layout } from '../../styles';
import { DetailBySchemaProps, DetailCommonFieldProps, DetailFieldLayout } from '../../utils';

export default function DetailBySchema({
    values,
    schema,
    editLink,
    editLabel,
    labelKey = 'label',
    decimalScale = 2,
    fieldsLayout: fieldsLayoutInitial,
    fieldsProps,
    sxRow,
    sxRowMultiple,
    sxField,
    sxLabel,
    sxValue,
    sxValueList,
    sxValueListItem,
    sxValueListItemText,
}: DetailBySchemaProps) {
    const [fieldsLayout, setFieldsLayout] = useState<DetailFieldLayout[]>([]);

    const getColumns = () => {
        if (fieldsLayoutInitial && fieldsLayoutInitial.length > 0) {
            return fieldsLayoutInitial;
        }

        const rows = [];
        for (const [key, field] of Object.entries(schema)) {
            if (['field', 'nested object'].includes(field.type)) {
                rows.push(key);
                continue;
            }
        }
        return [{ rows }];
    };

    useEffect(() => {
        const newFieldsLayout = getColumns();
        setFieldsLayout(newFieldsLayout);
    }, []);

    if (fieldsLayout.length === 0) {
        return (
            <Card sx={Layout.formCard}>
                <CardContent>
                    <Box sx={Layout.loadingBoxWhite}>
                        <CircularProgress />
                    </Box>
                </CardContent>
            </Card>
        );
    }

    return (
        <>
            {fieldsLayout.map((section, sectionIndex) => {
                return (
                    <Card sx={Layout.formCard} key={`section_${sectionIndex}`}>
                        {section.title && (
                            <CardHeader
                                title={section.title}
                                action={
                                    sectionIndex === 0 && editLink ? (
                                        <Stack direction="row" justifyContent="flex-end">
                                            <Button href={editLink} variant="contained">
                                                {editLabel}
                                            </Button>
                                        </Stack>
                                    ) : (
                                        <></>
                                    )
                                }
                            />
                        )}
                        <CardContent>
                            {section.CustomElement && <>{section.CustomElement}</>}
                            {section.rows && (
                                <>
                                    {section.rows.map((row, rowIndex) => {
                                        if (typeof row === 'string') {
                                            const field = row;
                                            const fieldProps: DetailCommonFieldProps =
                                                fieldsProps && fieldsProps.hasOwnProperty(field)
                                                    ? fieldsProps[field]
                                                    : {};
                                            return (
                                                <Box key={field} sx={sxRow}>
                                                    <DetailFieldBySchema
                                                        name={field}
                                                        value={values[field]}
                                                        schema={schema}
                                                        labelKey={labelKey}
                                                        decimalScale={decimalScale}
                                                        sxField={sxField}
                                                        sxLabel={sxLabel}
                                                        sxValue={sxValue}
                                                        sxValueList={sxValueList}
                                                        sxValueListItem={sxValueListItem}
                                                        sxValueListItemText={sxValueListItemText}
                                                        {...fieldProps}
                                                    />
                                                </Box>
                                            );
                                        }

                                        if (Array.isArray(row)) {
                                            return (
                                                <Box key={`row_${rowIndex}`} sx={sxRowMultiple}>
                                                    {row.map((field, i) => {
                                                        const key =
                                                            typeof field === 'string'
                                                                ? field
                                                                : field.key;
                                                        const fieldProps: DetailCommonFieldProps =
                                                            fieldsProps &&
                                                            fieldsProps.hasOwnProperty(key)
                                                                ? fieldsProps[key]
                                                                : {};
                                                        return (
                                                            <React.Fragment
                                                                key={`field_${rowIndex}_${i}`}>
                                                                {typeof field === 'string' ? (
                                                                    <DetailFieldBySchema
                                                                        name={field}
                                                                        value={values[field]}
                                                                        schema={schema}
                                                                        labelKey={labelKey}
                                                                        decimalScale={decimalScale}
                                                                        sxField={sxField}
                                                                        sxLabel={sxLabel}
                                                                        sxValue={sxValue}
                                                                        sxValueList={sxValueList}
                                                                        sxValueListItem={
                                                                            sxValueListItem
                                                                        }
                                                                        sxValueListItemText={
                                                                            sxValueListItemText
                                                                        }
                                                                        {...fieldProps}
                                                                    />
                                                                ) : (
                                                                    <field.CustomElement
                                                                        sxField={sxField}
                                                                        sxLabel={sxLabel}
                                                                        sxValue={sxValue}
                                                                        sxValueList={sxValueList}
                                                                        sxValueListItem={
                                                                            sxValueListItem
                                                                        }
                                                                        sxValueListItemText={
                                                                            sxValueListItemText
                                                                        }
                                                                        {...fieldProps}
                                                                    />
                                                                )}
                                                            </React.Fragment>
                                                        );
                                                    })}
                                                </Box>
                                            );
                                        }

                                        const fieldProps: DetailCommonFieldProps =
                                            fieldsProps && fieldsProps.hasOwnProperty(row.key)
                                                ? fieldsProps[row.key]
                                                : {};
                                        return (
                                            <row.CustomElement
                                                sxField={sxField}
                                                sxLabel={sxLabel}
                                                sxValue={sxValue}
                                                sxValueList={sxValueList}
                                                sxValueListItem={sxValueListItem}
                                                sxValueListItemText={sxValueListItemText}
                                                {...fieldProps}
                                            />
                                        );
                                    })}
                                </>
                            )}
                        </CardContent>
                    </Card>
                );
            })}
        </>
    );
}
