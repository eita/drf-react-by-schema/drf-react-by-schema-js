import React from 'react';
import moment from 'moment';
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemText from '@mui/material/ListItemText';

import { DetailFieldBySchemaProps } from '../../utils';

export default function DetailFieldBySchema({
    name,
    value,
    schema,
    labelKey = 'label',
    optionIdKey = 'value',
    optionLabelKey = 'display_name',
    sxField,
    sxLabel,
    sxValue,
    sxValueList,
    sxValueListItem,
    sxValueListItemText,
}: DetailFieldBySchemaProps) {
    if (!value) {
        return (
            <Box sx={sxField}>
                <Typography variant="h5" sx={sxLabel}>
                    {schema[name].label}
                </Typography>
                <Typography variant="body2" sx={sxValue}>
                    -
                </Typography>
            </Box>
        );
    }
    switch (schema[name].type) {
        case 'date':
            return (
                <Box sx={sxField}>
                    <Typography variant="h5" sx={sxLabel}>
                        {schema[name].label}
                    </Typography>
                    <Typography variant="body2" sx={sxValue}>
                        {moment(value).format('DD/MM/YYYY')}
                    </Typography>
                </Box>
            );
        case 'datetime':
            return (
                <Box sx={sxField}>
                    <Typography variant="h5" sx={sxLabel}>
                        {schema[name].label}
                    </Typography>
                    <Typography variant="body2" sx={sxValue}>
                        {moment(value).format('DD/MM/YYYY HH:mm')}
                    </Typography>
                </Box>
            );
        case 'nested object':
        case 'field':
            const multiple = schema[name].many || false;
            if (multiple) {
                return (
                    <Box sx={sxField}>
                        <Typography variant="h5" sx={sxLabel}>
                            {schema[name].label}
                        </Typography>
                        <List sx={sxValueList}>
                            {value.map((singleValue: any) => (
                                <ListItem key={singleValue.id} sx={sxValueListItem}>
                                    <ListItemText sx={sxValueListItemText}>
                                        {singleValue[labelKey]}
                                    </ListItemText>
                                </ListItem>
                            ))}
                        </List>
                    </Box>
                );
            }
            return (
                <Box sx={sxField}>
                    <Typography variant="h5" sx={sxLabel}>
                        {schema[name].label}
                    </Typography>
                    <Typography variant="body2" sx={sxValue}>
                        {value[labelKey]}
                    </Typography>
                </Box>
            );
        case 'choice':
            const multipleChoice = schema[name].many || false;
            if (multipleChoice) {
                return (
                    <Box sx={sxField}>
                        <Typography variant="h5" sx={sxLabel}>
                            {schema[name].label}
                        </Typography>
                        <List sx={sxValueList}>
                            {value.map((singleValue: any) => (
                                <ListItem key={singleValue[optionIdKey]} sx={sxValueListItem}>
                                    <ListItemText sx={sxValueListItemText}>
                                        {singleValue[optionLabelKey]}
                                    </ListItemText>
                                </ListItem>
                            ))}
                        </List>
                    </Box>
                );
            }
            return (
                <Box sx={sxField}>
                    <Typography variant="h5" sx={sxLabel}>
                        {schema[name].label}
                    </Typography>
                    <Typography variant="body2" sx={sxValue}>
                        {value.display_name}
                    </Typography>
                </Box>
            );
        case 'boolean':
            return (
                <Box sx={sxField}>
                    <Typography variant="h5" sx={sxLabel}>
                        {schema[name].label}
                    </Typography>
                    <Typography variant="body2" sx={sxValue}>
                        {value ? 'Sim' : 'Não'}
                    </Typography>
                </Box>
            );
        case 'decimal':
        case 'float':
            return (
                <Box sx={sxField}>
                    <Typography variant="h5" sx={sxLabel}>
                        {schema[name].label}
                    </Typography>
                    <Typography variant="body2" sx={sxValue}>
                        {value.toLocaleString()}
                    </Typography>
                </Box>
            );
        case 'email':
            return (
                <Box sx={sxField}>
                    <Typography variant="h5" sx={sxLabel}>
                        {schema[name].label}
                    </Typography>
                    <Typography variant="body2" sx={sxValue}>
                        <a href={`mailto: ${value}`}>{value}</a>
                    </Typography>
                </Box>
            );
        case 'number':
        case 'integer':
        case 'password':
        default:
            return (
                <Box sx={sxField}>
                    <Typography variant="h5" sx={sxLabel}>
                        {schema[name].label}
                    </Typography>
                    <Typography variant="body2" sx={sxValue}>
                        {value}
                    </Typography>
                </Box>
            );
    }
}
