import React from 'react';
import Button from '@mui/material/Button';
import { DialogType } from '../context/APIWrapperContext';

interface DialogActionsProps {
    setDialog: (x: Partial<DialogType>) => void;
    handleSave?: (e: React.SyntheticEvent) => Promise<boolean>;
    btnCancel?: string;
    btnConfirm?: string;
}
export default function DialogActions({
    setDialog,
    handleSave,
    btnCancel = 'Cancelar',
    btnConfirm = 'Salvar',
}: DialogActionsProps) {
    return (
        <>
            <Button
                onClick={() => {
                    setDialog({ open: false });
                }}>
                {btnCancel}
            </Button>
            {handleSave && <Button onClick={handleSave}>{btnConfirm}</Button>}
        </>
    );
}
