import React from 'react';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
// import { SxProps } from '@mui/material';
import Typography from '@mui/material/Typography';

import { Layout } from '../styles';
import { Id, isTmpId } from '../utils';
import { APIWrapperContext } from '../context/APIWrapperContext';

interface FormButtonsProps {
    model: string;
    objId: Id;
    title: string;
    formDisabled: boolean;
    cancelBtn?: (e: React.BaseSyntheticEvent) => any;
    cancelBtnLabel?: string;
    deleteBtnLabel?: string;
    saveAndCreateNewBtnLabel?: string;
    saveAndContinueBtnLabel?: string;
    saveBtnLabel?: string;
    onSuccess?: (e: React.BaseSyntheticEvent) => any;
    bottom?: boolean;
    borderBottom?: boolean;
    saveAndContinue?: boolean;
    sx?: any;
}

export default function FormButtons ({
    model,
    objId,
    title,
    formDisabled,
    cancelBtn,
    cancelBtnLabel = 'Cancel',
    deleteBtnLabel = 'Delete',
    saveAndCreateNewBtnLabel = 'Save and Create New',
    saveAndContinueBtnLabel = 'Save and Continue',
    saveBtnLabel = 'Save',
    onSuccess,
    bottom = false,
    borderBottom = true,
    saveAndContinue = true,
    sx = {}
}:FormButtonsProps) {
    const apiContext = React.useContext(APIWrapperContext);
    if (!apiContext) {
        console.error('Error on drf-react-by-schema: there is no serverEndPoint configuration!');
        return(<></>);
    }
    const onDeleteModel = apiContext.onDeleteModel;
    if (bottom && borderBottom && objId && objId !== 'novo' && sx !== null) {
        sx.mb = 5;
        sx.pb = 2;
        sx.borderBottom = 'solid 1px #aaa';
    }
    return (
        <Box sx={{ ...Layout.flexRowGrow, ...sx }}>
            {bottom
                ? <Button
                    variant="contained"
                    type="button"
                    color="error"
                    size="small"
                    disabled={isTmpId(objId) || !onDeleteModel}
                    onClick={() => {
                        onDeleteModel(
                            model,
                            objId,
                            onSuccess
                        );
                    }}
                >
                    {deleteBtnLabel}
                </Button>
                : title
                    ? <Typography variant="h5">
                        {title}
                    </Typography>
                    : <Box />

            }
            <Box sx={Layout.flexRow}>
                {cancelBtn &&
                    <Button
                        variant="contained"
                        color="primary"
                        size="small"
                        sx={{ ml: 1 }}
                        onClick={cancelBtn}
                    >
                        {cancelBtnLabel}
                    </Button>
                }
                {saveAndContinue &&
                    <>
                        <Button
                            variant="contained"
                            name="createNewOnSave"
                            type="submit"
                            color="secondary"
                            size="small"
                            sx={{ ml: 1 }}
                            disabled={formDisabled}
                        >
                            {saveAndCreateNewBtnLabel}
                        </Button>
                        <Button
                            variant="contained"
                            name="stayOnSave"
                            type="submit"
                            color="secondary"
                            size="small"
                            sx={{ ml: 1 }}
                            disabled={formDisabled}
                        >
                            {saveAndContinueBtnLabel}
                        </Button>
                    </>
                }
                <Button
                    variant="contained"
                    name="exitOnSave"
                    type="submit"
                    color="primary"
                    size="small"
                    sx={{ ml: 1 }}
                    disabled={formDisabled}
                >
                    {saveBtnLabel}
                </Button>
            </Box>
        </Box>
    );
}
