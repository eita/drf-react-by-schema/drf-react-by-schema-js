import React from 'react';
import Alert from '@mui/material/Alert';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import { NumericFormat } from 'react-number-format';
import { GridRowId } from '@mui/x-data-grid';

import { Item, SumRowsType } from '../utils';

interface DataTotalsProps {
    data?: Item[];
    sumRows?: SumRowsType;
    visibleRows: GridRowId[];
}

const DataTotals = ({ data, sumRows, visibleRows }: DataTotalsProps) => {
    return (
        <>
            {data && sumRows && sumRows.rows.length > 0 && (
                <Alert severity={sumRows.severity || 'info'}>
                    <List dense={true}>
                        {sumRows.rows.map((row) => (
                            <ListItem key={`sumRows_${row.field}`}>
                                <NumericFormat
                                    value={data.reduce((total, item) => {
                                        if (
                                            parseFloat(item[row.field]) &&
                                            visibleRows.includes(`${item.id}`)
                                        ) {
                                            return total + parseFloat(item[row.field]);
                                        }
                                        return total;
                                    }, 0)}
                                    thousandSeparator="."
                                    decimalSeparator=","
                                    displayType={'text'}
                                    decimalScale={row.isCount ? 0 : 2}
                                    fixedDecimalScale={true}
                                    prefix={row.prefix}
                                    suffix={row.suffix}
                                />
                            </ListItem>
                        ))}
                    </List>
                </Alert>
            )}
        </>
    );
};

export default DataTotals;
