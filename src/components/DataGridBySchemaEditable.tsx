import React, { useEffect, useState, useRef, forwardRef } from 'react';
import moment from 'moment';
import {
    DataGrid,
    GridEditInputCell,
    GridRowModes,
    GridActionsCellItem,
    gridStringOrNumberComparator,
    GridRowId,
    GridSelectionModel,
    GridRenderCellParams,
    GridRowModesModel,
} from '@mui/x-data-grid';
import * as Yup from 'yup';
import Box from '@mui/material/Box';
import CircularProgress from '@mui/material/CircularProgress';
import EditIcon from '@mui/icons-material/Edit';
import BoltIcon from '@mui/icons-material/Bolt';
import VisibilityIcon from '@mui/icons-material/Visibility';
import ClearIcon from '@mui/icons-material/Clear';
import CheckIcon from '@mui/icons-material/Check';
import UndoIcon from '@mui/icons-material/Undo';
import Snackbar from '@mui/material/Snackbar';
import Alert from '@mui/material/Alert';
import { SxProps } from '@mui/material';
import stringMask from 'string-mask';
import { AxiosError } from 'axios';

import { Layout } from '../styles';
import { deleteData, getAutoComplete, updateDataBySchema } from '../api';
import {
    Item,
    SchemaType,
    Choice,
    Id,
    GridEnrichedBySchemaColDef,
    isTmpId,
    emptyByType,
    buildGenericYupValidationSchema,
    PaginationModel,
    buildDateFormatBySchema,
    ActionType,
    OnSelectActions,
    BulkUpdateData,
    BulkDeleteData,
    getTmpId,
    OptionsAC,
    FormFieldLayout,
    CustomAction,
} from '../utils';
import { useDRFReactBySchema } from '../context/DRFReactBySchemaContext';
import { quantityOnlyOperators } from './DataGridBySchemaEditable/utils';
import { CustomToolbar } from './DataGridBySchemaEditable/CustomToolbar';
import { SelectEditInputCell } from './DataGridBySchemaEditable/SelectEditInputCell';
import { GridDateInput } from './DataGridBySchemaEditable/GridDateInput';
import { GridDecimalInput } from './DataGridBySchemaEditable/GridDecimalInput';
import { GridPatternInput } from './DataGridBySchemaEditable/GridPatternInput';
import { BooleanInputCell } from './DataGridBySchemaEditable/BooleanInputCell';
import { FooterToolbar } from './DataGridBySchemaEditable/FooterToolbar';
import { ConfirmDialog } from './DataGridBySchemaEditable/ConfirmDialog';
import { OnEditModelType, useAPIWrapper } from '../context/APIWrapperContext';

interface DataGridBySchemaEditableProps {
    schema: SchemaType;
    data: Item[];
    rowCount?: number;
    columns: GridEnrichedBySchemaColDef[];
    model: string;
    fieldKey?: string;
    labelKey?: string;
    index?: number;
    name?: string;
    indexField?: string;
    addExistingModel?: string;
    indexFieldMinWidth?: number;
    indexFieldBasePath?: string;
    indexFieldViewBasePath?: string;
    stateToLink?: object;
    minWidth?: number;
    loading?: boolean;

    modelParentId?: Id;
    modelParent?: string;

    customColumnOperations?: (
        p: GridEnrichedBySchemaColDef
    ) => GridEnrichedBySchemaColDef | Promise<GridEnrichedBySchemaColDef>;
    customFieldFormLayouts?: Record<string, FormFieldLayout[]>;
    customLinkDestination?: (p: GridRenderCellParams) => string;
    LinkComponent?: any;
    onProcessRow?: (p: Item) => void;
    onDataChange?: (p: Item[]) => void;
    onEditModel?: (p: OnEditModelType) => void;
    isEditable?: boolean;
    hasBulkSelect?: boolean;
    onSelectActions?: OnSelectActions[];
    sx?: SxProps;
    isAutoHeight?: boolean;
    defaultValues?: Item;
    hideFooterPagination?: boolean;
    setVisibleRows?: (p: GridRowId[]) => void;
    paginationModel?: PaginationModel;
    setPaginationModel?: (x: PaginationModel) => void;
    hideFooterComponent?: boolean;
    hideToolbarComponent?: boolean;
    tableAutoHeight?: boolean;
    actions?: Partial<ActionType>[];
    customActions?: CustomAction[];
    hideColumnsButton?: boolean;
    hideFilterButton?: boolean;
    hideDensityButton?: boolean;
    hideExportButton?: boolean;
    hideQuickFilterBar?: boolean;
    optionsAC?: OptionsAC;
}

const DataGridBySchemaEditable = forwardRef(
    (
        {
            schema,
            data,
            rowCount = 0,
            columns,
            model,
            name = Math.floor(Math.random() * 1000000).toString(),
            indexField = 'nome',
            addExistingModel,
            indexFieldMinWidth = 350,
            indexFieldBasePath = '',
            indexFieldViewBasePath,
            stateToLink = {},
            minWidth = 80,
            loading,
            modelParent,
            modelParentId,
            customColumnOperations,
            customFieldFormLayouts,
            customLinkDestination,
            LinkComponent,
            onProcessRow,
            onDataChange,
            onEditModel,
            isEditable = false,
            hasBulkSelect = false,
            onSelectActions,
            isAutoHeight = false,
            defaultValues = {},
            hideFooterPagination = false,
            setVisibleRows,
            paginationModel = undefined,
            setPaginationModel = undefined,
            hideFooterComponent,
            hideToolbarComponent,
            tableAutoHeight,
            actions = ['editInline', 'remove'],
            customActions,
            hideColumnsButton,
            hideFilterButton,
            hideDensityButton,
            hideExportButton,
            hideQuickFilterBar,
            optionsAC: optionsACExternal,
        }: DataGridBySchemaEditableProps,
        ref
    ) => {
        const { serverEndPoint } = useDRFReactBySchema();

        const apiContext = useAPIWrapper();

        const initialSnackBar: Item = {
            open: false,
            msg: '',
            severity: 'info',
        };
        const [snackBar, setSnackBar] = useState(initialSnackBar);
        const [dataGrid, setDataGrid] = useState<{ data: Item[] }>({
            data: [],
        });
        const [preparedColumns, setPreparedColumns] = useState<null | GridEnrichedBySchemaColDef[]>(
            null
        );
        const [dataGridLoading, setDataGridLoading] = useState(false);
        const [rowModesModel, setRowModesModel] = useState<GridRowModesModel>();
        const [dialogOpen, setDialogOpen] = useState(false);
        const [selectionModel, setSelectionModel] = useState<Item[]>([]);
        const [selectionModelIds, setSelectionModelIds] = useState<GridSelectionModel>([]);
        const optionsAC = useRef<OptionsAC | null>(null);
        const emptyItem = useRef<Item>({});
        const yupValidationSchema = useRef<Yup.AnySchema | null>(null);
        const processingRow = useRef<number | string | null>(null);
        const idToRemove = useRef<Id | null>(null);
        const visibleRows = useRef<string>('');
        if (!onEditModel) {
            onEditModel = apiContext.onEditModel;
        }

        const updateOptionsAC = async () => {
            optionsAC.current = {};
            for (const col of columns) {
                if (optionsACExternal && optionsACExternal[col.field]) {
                    optionsAC.current[col.field] = optionsACExternal[col.field];
                    continue;
                }
                if (!schema[col.field]) {
                    continue;
                }
                if (
                    ['field', 'nested object'].includes(schema[col.field].type) &&
                    !(col.field in optionsAC.current)
                ) {
                    const options = await getAutoComplete({
                        model: col.field,
                        serverEndPoint,
                    });
                    if (options) {
                        optionsAC.current[col.field] = options;
                        continue;
                    }
                    console.log(`Couldn't load autocomplete options from '${col.field}'`);
                    continue;
                }
                if (schema[col.field].type === 'choice' && !(col.field in optionsAC.current)) {
                    optionsAC.current[col.field] = schema[col.field].choices as Choice[];
                    continue;
                }
                if (col.field === indexField && addExistingModel && !optionsAC.current[col.field]) {
                    const options = await getAutoComplete({
                        model: addExistingModel,
                        serverEndPoint,
                    });
                    if (options) {
                        optionsAC.current[col.field] = options;
                        continue;
                    }
                    if (!(col.field in optionsAC.current)) {
                        delete optionsAC.current[col.field];
                    }
                }
            }
        };

        const initColumns = async () => {
            const actionCols: GridEnrichedBySchemaColDef[] = [];
            if (isEditable) {
                const handleSaveClick = (id: Id) => () => {
                    if (id) {
                        setRowModesModel({
                            ...rowModesModel,
                            [id]: { mode: GridRowModes.View },
                        });
                    }
                };
                const handleCancelClick = (id: Id) => () => {
                    if (id) {
                        setRowModesModel({
                            ...rowModesModel,
                            [id]: { mode: GridRowModes.View, ignoreModifications: true },
                        });
                    }

                    setDataGrid((currentDataGrid) => {
                        const editedRow = currentDataGrid.data.find((row) => row.id === id);
                        if (editedRow && editedRow.isNew) {
                            return {
                                ...currentDataGrid,
                                data: currentDataGrid.data.filter((row) => row.id !== id),
                            };
                        }
                        return currentDataGrid;
                    });
                };
                const handleEditInlineClick = (id: Id) => () => {
                    if (id) {
                        setRowModesModel({
                            ...rowModesModel,
                            [id]: { mode: GridRowModes.Edit },
                        });
                    }
                };
                actionCols.push({
                    field: 'actions',
                    headerName: '',
                    type: 'actions',
                    getActions: ({ id }: { id: Id }) => {
                        if (!id) {
                            return [];
                        }
                        const isInEditMode =
                            rowModesModel &&
                            rowModesModel[id] &&
                            rowModesModel[id].mode === GridRowModes.Edit;
                        if (isInEditMode) {
                            return [
                                <GridActionsCellItem
                                    key={`save_${id}`}
                                    icon={<CheckIcon />}
                                    label="Salvar"
                                    onClick={handleSaveClick(id)}
                                    color="success"
                                    onResize={() => null}
                                    onResizeCapture={() => null}
                                    showInMenu={false}
                                    placeholder={''}
                                />,
                                <GridActionsCellItem
                                    key={`cancel_${id}`}
                                    icon={<UndoIcon />}
                                    label="Cancelar"
                                    onClick={handleCancelClick(id)}
                                    color="inherit"
                                    onResize={() => null}
                                    onResizeCapture={() => null}
                                    showInMenu={false}
                                    placeholder={''}
                                />,
                            ];
                        }

                        const actionItems: JSX.Element[] = [];

                        actions.map((action) => {
                            switch (action) {
                                case 'editInline':
                                    actionItems.push(
                                        <GridActionsCellItem
                                            key={`editInline_${id}`}
                                            icon={
                                                actions.includes('edit') ? (
                                                    <BoltIcon />
                                                ) : (
                                                    <EditIcon />
                                                )
                                            }
                                            label="Edit inline"
                                            onClick={handleEditInlineClick(id)}
                                            color="inherit"
                                            onResize={() => null}
                                            onResizeCapture={() => null}
                                            showInMenu={false}
                                            placeholder={''}
                                        />
                                    );
                                    break;
                                case 'remove':
                                    actionItems.push(
                                        <GridActionsCellItem
                                            key={`remove_${id}`}
                                            icon={<ClearIcon />}
                                            label="Delete"
                                            onClick={handleDeleteClick(id)}
                                            color="error"
                                            onResize={() => null}
                                            onResizeCapture={() => null}
                                            showInMenu={false}
                                            placeholder={''}
                                        />
                                    );
                                    break;
                                case 'edit':
                                    if (!LinkComponent) {
                                        console.log(
                                            'you must pass LinkComponent prop to navigate externally'
                                        );
                                        break;
                                    }
                                    actionItems.push(
                                        <LinkComponent
                                            key={`edit_${id}`}
                                            to={`${indexFieldBasePath}${id}`}
                                            state={stateToLink}>
                                            <GridActionsCellItem
                                                icon={<EditIcon />}
                                                label="Edit"
                                                color="inherit"
                                                onResize={() => null}
                                                onResizeCapture={() => null}
                                                showInMenu={false}
                                                placeholder={''}
                                            />
                                        </LinkComponent>
                                    );
                                    break;
                                case 'view':
                                    if (!LinkComponent) {
                                        console.log(
                                            'you must pass LinkComponent prop to navigate externally'
                                        );
                                        break;
                                    }
                                    if (!indexFieldViewBasePath) {
                                        console.log(
                                            'For view action to work, you must supply indexFieldViewBasePath prop!'
                                        );
                                        break;
                                    }
                                    actionItems.push(
                                        <LinkComponent
                                            key={`view_${id}`}
                                            to={`${indexFieldViewBasePath}${id}`}
                                            state={stateToLink}>
                                            <GridActionsCellItem
                                                icon={<VisibilityIcon />}
                                                label="View"
                                                color="inherit"
                                                onResize={() => null}
                                                onResizeCapture={() => null}
                                                showInMenu={false}
                                                placeholder={''}
                                            />
                                        </LinkComponent>
                                    );
                                    break;
                            }
                        });
                        // React.ReactElement<any, string | React.JSXElementConstructor<any>>
                        // React.ReactElement<any, string | React.JSXElementConstructor<any>>

                        if (customActions) {
                            customActions.map((customAction) => {
                                actionItems.push(
                                    <GridActionsCellItem
                                        key={`${customAction.key}_${id}`}
                                        icon={customAction.icon}
                                        label={customAction.label}
                                        onClick={() =>
                                            customAction.handleClick(
                                                dataGrid.data.find((row) => row.id === id)
                                            )
                                        }
                                        onResize={() => null}
                                        onResizeCapture={() => null}
                                        showInMenu={false}
                                        placeholder={''}
                                    />
                                );
                            });
                        }

                        return actionItems;
                    },
                });
            }
            const cols = [...actionCols, ...columns];
            const colsPromises = cols.map(async (col) => {
                if (!schema[col.field]) {
                    return col;
                }
                const isIndexField = indexField !== '' && col.field === indexField;
                let column = col;
                if (isIndexField) {
                    col.isIndexField = true;
                }
                column.editable =
                    isEditable &&
                    !col.disabled &&
                    (!schema[col.field].read_only ||
                        ['field', 'nested object'].includes(schema[col.field].type));
                if (['field', 'nested object'].includes(schema[col.field].type)) {
                    column.creatable =
                        'related_editable' in schema[col.field]
                            ? schema[col.field].related_editable
                            : column.creatable;
                }
                switch (schema[col.field].type) {
                    case 'date':
                        column.type = 'date';
                        const dateFormat = buildDateFormatBySchema(schema[col.field].date_views);
                        column.valueFormatter = (params) =>
                            params.value ? moment(params.value).format(dateFormat) : '';
                        column.filterOperators = quantityOnlyOperators({
                            type: 'date',
                        });
                        if (isEditable && !col.disabled) {
                            column.renderEditCell = (params) => (
                                <GridDateInput
                                    {...params}
                                    column={column}
                                    dateViews={schema[col.field].date_views}
                                />
                            );
                            break;
                        }
                        break;
                    case 'datetime':
                        column.type = 'dateTime';
                        column.minWidth = 180;
                        column.valueFormatter = (params) =>
                            params.value ? moment(params.value).format('DD/MM/YYYY HH:mm') : '';
                        column.filterOperators = quantityOnlyOperators({
                            type: 'date',
                        });
                        break;
                    case 'nested object':
                        column.minWidth = 150;
                        if (isEditable && !col.disabled) {
                            column.valueFormatter = (params) => {
                                return !params.value ? '' : params.value.label;
                            };
                            // column.filterable = false;
                            column.sortComparator = (v1, v2, param1, param2) => {
                                return gridStringOrNumberComparator(
                                    v1.label,
                                    v2.label,
                                    param1,
                                    param2
                                );
                            };
                            column.renderEditCell = (params) => (
                                <SelectEditInputCell
                                    {...params}
                                    column={column}
                                    type={schema[col.field].type}
                                    optionsAC={optionsAC.current}
                                    isIndexField={isIndexField}
                                    onEditModel={onEditModel}
                                    fieldsLayout={
                                        customFieldFormLayouts &&
                                        col.field in customFieldFormLayouts
                                            ? customFieldFormLayouts[col.field]
                                            : undefined
                                    }
                                    setDialog={apiContext.setDialog}
                                />
                            );
                            break;
                        }

                        column.valueGetter = (params) => {
                            return !params.value ? '' : params.value.label;
                        };
                        break;
                    case 'field':
                        column.orderable = false;

                        if (isEditable && !col.disabled) {
                            column.minWidth = 300;
                            column.valueFormatter = (params) => {
                                return !params.value || !Array.isArray(params.value)
                                    ? ''
                                    : params.value.map((val) => val.label).join(', ');
                            };
                            column.filterable = false;
                            column.renderEditCell = (params) => (
                                <SelectEditInputCell
                                    {...params}
                                    column={column}
                                    type={schema[col.field].type}
                                    optionsAC={optionsAC.current}
                                    isIndexField={isIndexField}
                                    multiple={schema[col.field].many || false}
                                    setDialog={apiContext.setDialog}
                                />
                            );
                            break;
                        }

                        column.valueGetter = (params) => {
                            return !params.value || !Array.isArray(params.value)
                                ? ''
                                : params.value.map((val) => val.label).join(', ');
                        };
                        break;
                    case 'choice':
                        if (isEditable && !col.disabled) {
                            column.minWidth = 150;
                            column.valueFormatter = (params) => {
                                return !params.value ? '' : params.value.display_name;
                            };
                            column.filterable = false;
                            column.sortComparator = (v1, v2, param1, param2) => {
                                return gridStringOrNumberComparator(
                                    v1.display_name,
                                    v2.display_name,
                                    param1,
                                    param2
                                );
                            };
                            column.renderEditCell = (params) => (
                                <SelectEditInputCell
                                    {...params}
                                    column={column}
                                    type={schema[col.field].type}
                                    optionsAC={optionsAC.current}
                                    isIndexField={isIndexField}
                                    setDialog={apiContext.setDialog}
                                />
                            );
                            break;
                        }

                        column.valueGetter = (params) => {
                            return !params.value ? '' : params.value.display_name;
                        };
                        break;
                    case 'boolean':
                        if (isEditable && !col.disabled) {
                            column.valueFormatter = (params) => {
                                return params.value ? 'Sim' : 'Não';
                            };
                            column.renderEditCell = (params) => (
                                <BooleanInputCell {...params} column={column} />
                            );
                            break;
                        }

                        column.valueGetter = (params) => {
                            return params.value ? 'Sim' : 'Não';
                        };
                        break;
                    case 'decimal':
                    case 'float':
                        const decimalScale = schema[col.field].decimal_places || 2;
                        const prefix = schema[col.field].prefix || '';
                        const suffix = schema[col.field].suffix || '';
                        const isCurrency = schema[col.field].is_currency;
                        column.type = 'number';
                        column.valueFormatter = (params) => {
                            return !params.value
                                ? ''
                                : parseFloat(params.value).toLocaleString('pt-BR', {
                                      minimumFractionDigits: decimalScale,
                                      maximumFractionDigits: decimalScale,
                                  });
                        };
                        if (isEditable && !col.disabled) {
                            column.renderEditCell = (params) => (
                                <GridDecimalInput
                                    {...params}
                                    decimalPlaces={decimalScale}
                                    isCurrency={isCurrency}
                                    prefix={prefix}
                                    suffix={suffix}
                                    column={column}
                                />
                            );
                        }
                        column.filterOperators = quantityOnlyOperators({
                            type: 'float',
                        });
                        break;
                    case 'number':
                    case 'integer':
                        column.type = 'number';
                        column.filterOperators = quantityOnlyOperators({
                            type: 'number',
                        });
                        break;
                }

                if (indexFieldMinWidth && !column.minWidth) {
                    column.minWidth = col.field === indexField ? indexFieldMinWidth : minWidth;
                }
                if (indexField) {
                    if (col.field === indexField) {
                        column.flex = 1;
                        column.renderCell = (params) => {
                            if (
                                LinkComponent &&
                                customLinkDestination &&
                                !actions.includes('edit')
                            ) {
                                return (
                                    <LinkComponent
                                        to={`${customLinkDestination(params)}`}
                                        state={stateToLink}>
                                        {params.formattedValue}
                                    </LinkComponent>
                                );
                            }

                            if (
                                !LinkComponent ||
                                !indexFieldBasePath ||
                                actions.includes('edit') ||
                                (['field', 'nested object'].includes(schema[col.field].type) &&
                                    (!params.row[col.field] || !params.row[col.field].id))
                            ) {
                                return <>{params.formattedValue}</>;
                            }

                            const targetId = ['field', 'nested object'].includes(
                                schema[col.field].type
                            )
                                ? params.row[col.field].id.toString()
                                : params.row.id.toString();

                            return (
                                <LinkComponent
                                    to={`${indexFieldBasePath}${targetId}`}
                                    state={stateToLink}>
                                    {params.formattedValue}
                                </LinkComponent>
                            );
                        };
                        if (
                            isEditable &&
                            !col.disabled &&
                            optionsAC.current &&
                            col.field in optionsAC.current &&
                            addExistingModel
                        ) {
                            column.renderEditCell = (params) => {
                                if (!isTmpId(params.id)) {
                                    return <GridEditInputCell {...params} />;
                                }
                                return (
                                    <SelectEditInputCell
                                        {...params}
                                        column={column}
                                        type={schema[col.field].type}
                                        optionsAC={optionsAC.current}
                                        isIndexField={true}
                                        setDialog={apiContext.setDialog}
                                    />
                                );
                            };
                        }
                    }
                }

                // format numbers:
                if (column.patternFormat) {
                    column.valueFormatter = (params) => {
                        if (!params.value || typeof params.value !== 'string') {
                            return params.value;
                        }
                        const formattedValue = new stringMask(column.patternFormat, {}).apply(
                            params.value
                        );
                        return formattedValue;
                    };
                    if (isEditable && !col.disabled) {
                        column.renderEditCell = (params) => (
                            <GridPatternInput {...params} patternFormat={column.patternFormat} />
                        );
                    }
                }

                // Custom column operations:
                if (customColumnOperations) {
                    column = await customColumnOperations(column);
                }

                // Finally!
                return column;
            });
            const finalCols = await Promise.all(colsPromises);
            setPreparedColumns(finalCols);
        };

        const handleAddItems = (numberOfRows = 1) => {
            const newRows: Item[] = [];
            for (let i = 0; i < numberOfRows; i++) {
                const id = getTmpId();
                newRows.push({ ...emptyItem.current, id });
            }
            const newData = tableAutoHeight
                ? [...dataGrid.data, ...newRows]
                : [...newRows, ...dataGrid.data];
            setDataGrid({
                data: newData,
            });
            setRowModesModel((oldModel) => {
                const newModel = { ...oldModel };
                newRows.map((newRow) => {
                    newModel[newRow.id] = { mode: GridRowModes.Edit, fieldToFocus: indexField };
                });
                return newModel;
            });
            if (!tableAutoHeight) {
                // Ugly hack to scroll to top, since scroll to cell is only available in Pro
                const el = document.querySelector(`.dataGrid_${name} .MuiDataGrid-virtualScroller`);
                // console.log(el, name);
                if (el) {
                    el.scrollTop = 0;
                    setTimeout(() => {
                        el.scrollTop = 0;
                    }, 10);
                }
            }
        };

        const bulkUpdateData: BulkUpdateData = async (newData: Item[]) => {
            const promises: Promise<Item | AxiosError>[] = [];
            const ids: Id[] = [];
            newData.map((item) => {
                promises.push(
                    updateDataBySchema({
                        model,
                        modelObjectId: item.id,
                        serverEndPoint,
                        data: item,
                        schema,
                    })
                );
                ids.push(item.id);
            });
            const results = await Promise.all(promises);
            setSelectionModel([]);
            setSelectionModelIds([]);
            setDataGrid((currentData) => {
                const updatedData = currentData.data.map((item) => {
                    const index = ids.indexOf(item.id);
                    return index === -1 ? item : newData[index];
                });
                return {
                    data: updatedData,
                };
            });
            setSnackBar({
                open: true,
                severity: 'success',
                msg: 'Alterações salvas com sucesso',
            });
            return results.map((result, index) => {
                return {
                    id: ids[index],
                    success: 'isAxiosError' in result,
                };
            });
        };

        const bulkDeleteData: BulkDeleteData = async (ids: Id[]) => {
            const promises: Promise<true | AxiosError>[] = [];
            ids.map((id) => {
                promises.push(deleteData(model, serverEndPoint, id));
            });
            setDataGridLoading(true);
            const results = await Promise.all(promises);
            setSelectionModel([]);
            setSelectionModelIds([]);
            setDataGrid({
                data: data.filter((item) => !ids.includes(item.id)),
            });
            setDataGridLoading(false);
            setSnackBar({
                open: true,
                severity: 'success',
                msg: 'Itens apagados com sucesso',
            });
            return results.map((result, index) => {
                return {
                    id: ids[index],
                    success: result === true,
                };
            });
        };

        const bulkCreateData = async (ids: Id[]) => {
            handleAddItems(ids.length);
            setSelectionModel([]);
            setSelectionModelIds([]);
            setSnackBar({
                open: true,
                severity: 'success',
                msg: 'Linhas adicionadas com sucesso',
            });
        };

        const handleDialogClose = () => {
            setDialogOpen(false);
        };

        const handleDeleteClick = (id: Id) => () => {
            idToRemove.current = id;
            setDialogOpen(true);
        };

        const handleDeleteSave = () => {
            if (idToRemove.current === null) {
                setDialogOpen(false);
                return;
            }
            const newData = dataGrid.data.filter((row) => row.id !== idToRemove.current);
            setDataGrid({
                ...dataGrid,
                data: newData,
            });
            if (modelParent && modelParentId && apiContext) {
                apiContext.onDeleteRelatedModel({
                    model: modelParent,
                    id: modelParentId,
                    relatedModel: model,
                    relatedModelId: idToRemove.current,
                });
            }

            // Reflect changes to the outside, for example for doing global calculations over multiple rows:
            if (onDataChange) {
                onDataChange(newData);
            }
            idToRemove.current = null;
            setDialogOpen(false);
        };

        useEffect(() => {
            if (!columns) {
                setDataGrid({ data: [] });
                return;
            }

            if (isEditable) {
                for (const col of columns) {
                    if (schema[col.field].model_default) {
                        emptyItem.current[col.field] = schema[col.field].model_default;
                        continue;
                    }
                    if (col.field in defaultValues) {
                        emptyItem.current[col.field] = defaultValues[col.field];
                        continue;
                    }
                    emptyItem.current[col.field] = schema[col.field]
                        ? emptyByType(schema[col.field])
                        : null;
                }
            }

            const skipFields = [];
            if (indexField && addExistingModel) {
                skipFields.push(indexField);
            }
            yupValidationSchema.current = buildGenericYupValidationSchema({
                data: emptyItem.current,
                schema,
                skipFields,
            });

            setDataGrid({
                data,
            });
        }, [data]);

        useEffect(() => {
            // I don't undedrstand why I had this. I'm commenting out....
            // if (optionsAC.current) {
            //     initColumns();
            //     return;
            // }

            updateOptionsAC().then(() => {
                initColumns();
            });
        }, [rowModesModel, JSON.stringify(columns)]);

        const processRowUpdate = async (editedRow: Item) => {
            if (!preparedColumns || !yupValidationSchema.current) {
                return false;
            }
            setDataGridLoading(true);
            const indexCol = preparedColumns.find((col) => col.field === indexField);
            processingRow.current = editedRow.id;
            await yupValidationSchema.current.validate(editedRow);
            const onlyAddExisting =
                indexField &&
                isTmpId(editedRow.id) &&
                editedRow[indexField] &&
                !isTmpId(editedRow[indexField].id) &&
                (!indexCol || !indexCol.valueFormatter);
            const createNewItem =
                indexField &&
                isTmpId(editedRow.id) &&
                editedRow[indexField] &&
                isTmpId(editedRow[indexField].id);
            if (onlyAddExisting) {
                const row: Item = {};
                row.id_to_add = editedRow[indexField].id;
                for (const [key, value] of Object.entries(editedRow[indexField])) {
                    if (key === 'id') {
                        row[key] = editedRow.id;
                        continue;
                    }
                    row[key] = value;
                }
                editedRow = { ...row };
            }
            if (createNewItem && editedRow[indexField]) {
                if (editedRow[indexField].inputValue) {
                    editedRow[indexField] = editedRow[indexField].inputValue;
                }
                if (addExistingModel) {
                    editedRow[indexField] = editedRow[indexField].label;
                }
            }

            if (modelParent && modelParentId && apiContext) {
                const response = await apiContext.onEditRelatedModelSave({
                    model: modelParent,
                    id: modelParentId,
                    relatedModel: model,
                    relatedModelId: editedRow.id,
                    newRow: editedRow,
                    schema,
                    onlyAddExisting,
                });

                if (response !== false && (onlyAddExisting || createNewItem || !onlyAddExisting)) {
                    updateOptionsAC();
                    const data = [...dataGrid.data];
                    const newRow =
                        'data' in response && 'schema' in response
                            ? (response.data as Item)
                            : response;
                    for (const i in data) {
                        if (data[i].id === editedRow.id) {
                            data[i] = newRow;

                            // This is for cases where users want to do custom operations after saving the row,
                            // like for example make calculations among columns.
                            if (onProcessRow) {
                                onProcessRow(data[i]);
                            }

                            // Reflect the changes to the outside, for example for global calculations over all data
                            if (onDataChange) {
                                onDataChange(data);
                            }

                            setDataGrid({ data });
                            break;
                        }
                    }
                    setDataGridLoading(false);
                    return newRow;
                }
                setDataGridLoading(false);
                return false;
            }

            const response = await apiContext.onEditModelDataGridSave({
                model,
                id: editedRow.id,
                newRow: editedRow,
                schema,
            });
            setDataGridLoading(false);
            if (response !== false) {
                updateOptionsAC();
                const data = [...dataGrid.data];
                const newRow = response;
                for (const i in data) {
                    if (data[i].id === editedRow.id) {
                        data[i] = newRow;

                        // This is for cases where users want to do custom operations after saving the row,
                        // like for example make calculations among columns.
                        if (onProcessRow) {
                            onProcessRow(data[i]);
                        }

                        // Reflect the changes to the outside, for example for global calculations over all data
                        if (onDataChange) {
                            onDataChange(data);
                        }

                        setDataGrid({ data });
                        break;
                    }
                }
                return newRow;
            }
            return false;
        };

        const customPaddings = isAutoHeight
            ? {
                  '&.MuiDataGrid-root--densityCompact .MuiDataGrid-cell': {
                      py: '8px',
                  },
                  '&.MuiDataGrid-root--densityStandard .MuiDataGrid-cell': {
                      py: '15px',
                  },
                  '&.MuiDataGrid-root--densityComfortable .MuiDataGrid-cell': {
                      py: '22px',
                  },
              }
            : undefined;

        const checkboxSelection = hasBulkSelect ? true : undefined;
        const disableRowSelectionOnClick = hasBulkSelect ? true : undefined;

        return (
            <Box className={`dataGrid_${name}`} sx={{ height: '100%' }}>
                {preparedColumns === null ? (
                    <Box sx={Layout.loadingBox}>
                        <CircularProgress />
                    </Box>
                ) : (
                    <DataGrid
                        rows={dataGrid.data}
                        columns={preparedColumns}
                        autoHeight={tableAutoHeight}
                        onStateChange={(state) => {
                            if (setVisibleRows) {
                                const newVisibleRows = Object.entries(
                                    state.filter.visibleRowsLookup
                                )
                                    .filter((entry) => {
                                        return entry[1] === true;
                                    })
                                    .map((entry: Item) => {
                                        return entry[0];
                                    }) as GridRowId[];
                                const newVisibleRowsJSON = JSON.stringify(newVisibleRows);
                                if (newVisibleRowsJSON !== visibleRows.current) {
                                    setVisibleRows(newVisibleRows);
                                    visibleRows.current = newVisibleRowsJSON;
                                }
                            }
                        }}
                        editMode="row"
                        loading={dataGridLoading || loading}
                        hideFooterPagination={hideFooterPagination}
                        getRowHeight={() => {
                            if (isAutoHeight) {
                                return 'auto';
                            }
                            return null;
                        }}
                        isCellEditable={(params) =>
                            (rowModesModel &&
                                rowModesModel[params.row.id] &&
                                rowModesModel[params.row.id].mode === GridRowModes.Edit &&
                                (!isTmpId(params.row.id) ||
                                    (isTmpId(params.row.id) &&
                                        (params.field === indexField ||
                                            !optionsAC.current ||
                                            !Object.prototype.hasOwnProperty.call(
                                                optionsAC.current,
                                                indexField
                                            ) ||
                                            (preparedColumns.find(
                                                (col) => col.field === indexField
                                            ) &&
                                                Object.prototype.hasOwnProperty.call(
                                                    preparedColumns.find(
                                                        (col) => col.field === indexField
                                                    ),
                                                    'valueFormatter'
                                                )))))) as boolean
                        }
                        checkboxSelection={checkboxSelection}
                        onSelectionModelChange={(newSelectionModel: GridSelectionModel) => {
                            const selectedData = dataGrid.data.filter((item) =>
                                newSelectionModel.includes(item.id)
                            );
                            setSelectionModel(selectedData);
                            setSelectionModelIds(newSelectionModel);
                        }}
                        selectionModel={selectionModelIds}
                        disableSelectionOnClick={disableRowSelectionOnClick}
                        rowModesModel={rowModesModel}
                        onRowEditStart={(params, event) => {
                            event.defaultMuiPrevented = true;
                        }}
                        onRowEditStop={(params, event) => {
                            event.defaultMuiPrevented = true;
                        }}
                        processRowUpdate={processRowUpdate}
                        onProcessRowUpdateError={(e) => {
                            setDataGridLoading(false);
                            if (processingRow.current) {
                                setRowModesModel({
                                    ...rowModesModel,
                                    [processingRow.current]: {
                                        mode: GridRowModes.Edit,
                                    },
                                });
                            }
                            const msg = `Erro em "${e.path}": ${e.errors}`;
                            setSnackBar({
                                open: true,
                                severity: 'error',
                                msg,
                            });
                            console.log(e);
                        }}
                        components={{
                            Toolbar: hideToolbarComponent ? () => <></> : CustomToolbar,
                            Footer: hideFooterComponent ? () => <></> : FooterToolbar,
                        }}
                        componentsProps={{
                            toolbar: {
                                preparedColumns,
                                setPreparedColumns,
                                showQuickFilter: true,
                                quickFilterProps: { debounceMs: 500 },
                                // getRowsToExport: () => {
                                //     return [];
                                // },
                                selectionModel,
                                onSelectActions,
                                bulkUpdateData,
                                bulkDeleteData,
                                bulkCreateData,
                                hideColumnsButton,
                                hideFilterButton,
                                hideDensityButton,
                                hideExportButton,
                                hideQuickFilterBar,
                            },
                            footer: {
                                isEditable,
                                handleAddItem: () => {
                                    handleAddItems();
                                },
                            },
                            filterPanel: {
                                sx: {
                                    '& .MuiDataGrid-filterFormValueInput': {
                                        width: 300,
                                    },
                                },
                            },
                        }}
                        experimentalFeatures={{ newEditingApi: isEditable }}
                        sx={customPaddings}
                        paginationMode={paginationModel ? 'server' : 'client'}
                        onPageChange={(newPage: number) => {
                            if (setPaginationModel && paginationModel) {
                                setPaginationModel({ ...paginationModel, page: newPage });
                            }
                        }}
                        onPageSizeChange={(newPageSize: number) => {
                            if (setPaginationModel && paginationModel) {
                                setPaginationModel({ ...paginationModel, pageSize: newPageSize });
                            }
                        }}
                        rowCount={
                            paginationModel && typeof rowCount !== 'undefined'
                                ? rowCount
                                : undefined
                        }
                        pageSize={paginationModel ? paginationModel.pageSize : 100}
                        rowsPerPageOptions={
                            paginationModel ? [paginationModel.pageSize] : [5, 10, 25, 50, 100]
                        }
                        filterMode={paginationModel ? 'server' : 'client'}
                        onFilterModelChange={
                            paginationModel
                                ? (newFilter) => {
                                      if (setPaginationModel && paginationModel) {
                                          setPaginationModel({
                                              ...paginationModel,
                                              filter: newFilter,
                                          });
                                      }
                                  }
                                : undefined
                        }
                        sortingMode={paginationModel ? 'server' : 'client'}
                        onSortModelChange={
                            paginationModel
                                ? (newSorting) => {
                                      if (setPaginationModel && paginationModel) {
                                          setPaginationModel({
                                              ...paginationModel,
                                              sort: newSorting,
                                          });
                                      }
                                  }
                                : undefined
                        }
                    />
                )}
                <ConfirmDialog
                    open={dialogOpen}
                    onClose={handleDialogClose}
                    onConfirm={handleDeleteSave}
                />
                <Snackbar
                    open={snackBar.open}
                    autoHideDuration={5000}
                    onClose={() => {
                        setSnackBar({ ...initialSnackBar, open: false });
                    }}
                    anchorOrigin={{ vertical: 'top', horizontal: 'center' }}>
                    <Alert severity={snackBar.severity}>{snackBar.msg}</Alert>
                </Snackbar>
            </Box>
        );
    }
);

DataGridBySchemaEditable.displayName = 'DataGridBySchemaEditable';

export default DataGridBySchemaEditable;
