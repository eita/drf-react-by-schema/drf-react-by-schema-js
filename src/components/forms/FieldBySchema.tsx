import React from 'react';

import { FieldBySchemaProps } from '../../utils';

import DesktopDatePickerBySchema from './inputs/DesktopDatePickerBySchema';
import DesktopDateTimePickerBySchema from './inputs/DesktopDateTimePickerBySchema';
import EditableAutocompleteFieldBySchema from './inputs/EditableAutocompleteFieldBySchema';
import AutocompleteFieldBySchema from './inputs/AutocompleteFieldBySchema';
import BooleanFieldBySchema from './inputs/BooleanFieldBySchema';
import FloatFieldBySchema from './inputs/FloatFieldBySchema';
import TextFieldBySchema from './inputs/TextFieldBySchema';

export default function FieldBySchema({
    name,
    schema,
    control,
    errors,
    multiline = false,
    setValue,
    getValues,
    fieldKey,
    labelKey = 'nome',
    index,
    optionsAC,
    optionsModel,
    getOptionLabel,
    renderOption,
    onEditModel,
    fieldsLayout,
    sx,
    options,
    isSemaphoric = false,
    label,
    onValueChange,
    decimalScale = 2,
    ...other
}: FieldBySchemaProps) {
    switch (schema[name].type) {
        case 'date':
            return (
                <DesktopDatePickerBySchema
                    name={name}
                    schema={schema}
                    control={control}
                    errors={errors}
                    multiline={multiline}
                    fieldKey={fieldKey}
                    index={index}
                    sx={sx}
                    onValueChange={onValueChange}
                    {...other}
                />
            );
        case 'datetime':
            return (
                <DesktopDateTimePickerBySchema
                    name={name}
                    schema={schema}
                    control={control}
                    errors={errors}
                    multiline={multiline}
                    fieldKey={fieldKey}
                    index={index}
                    sx={sx}
                    onValueChange={onValueChange}
                    {...other}
                />
            );
        // case 'field':
        //     return (
        //         <AutocompleteFieldBySchema
        //             name={name}
        //             control={control}
        //             schema={schema}
        //             errors={errors}
        //             options={optionsAC[name] || []}
        //             multiple
        //             sx={sx}
        //         />
        //     );
        case 'nested object':
        case 'field':
            const relatedEditable = schema[name].related_editable;
            const multiple = schema[name].many || false;
            return (
                <EditableAutocompleteFieldBySchema
                    name={name}
                    schema={schema}
                    control={control}
                    errors={errors}
                    setValue={setValue}
                    getValues={getValues}
                    fieldKey={fieldKey}
                    labelKey={labelKey}
                    index={index}
                    optionsAC={optionsAC}
                    optionsModel={optionsModel}
                    getOptionLabel={getOptionLabel}
                    renderOption={renderOption}
                    onEditModel={relatedEditable ? onEditModel : undefined}
                    fieldsLayout={fieldsLayout}
                    multiple={multiple}
                    sx={sx}
                    onValueChange={onValueChange}
                    {...other}
                />
            );
        case 'choice':
            return (
                <AutocompleteFieldBySchema
                    index={index}
                    name={name}
                    schema={schema}
                    control={control}
                    errors={errors}
                    fieldKey={fieldKey}
                    optionIdKey="value"
                    optionLabelKey="display_name"
                    options={options}
                    isSemaphoric={isSemaphoric}
                    label={label}
                    sx={sx}
                    onValueChange={onValueChange}
                />
            );
        case 'boolean':
            return (
                <BooleanFieldBySchema
                    name={name}
                    schema={schema}
                    control={control}
                    errors={errors}
                    fieldKey={fieldKey}
                    index={index}
                    sx={sx}
                    onValueChange={onValueChange}
                    {...other}
                />
            );
        case 'decimal':
        case 'float':
            return (
                <FloatFieldBySchema
                    name={name}
                    schema={schema}
                    control={control}
                    errors={errors}
                    multiline={multiline}
                    fieldKey={fieldKey}
                    index={index}
                    onValueChange={onValueChange}
                    decimalScale={decimalScale}
                    label={label}
                    sx={sx}
                    {...other}
                />
            );
        case 'number':
        case 'integer':
        case 'password':
        default:
            const localMultiline = schema[name].model_multiline === true || multiline;
            const minRows = localMultiline ? 3 : 0;
            return (
                <TextFieldBySchema
                    name={name}
                    schema={schema}
                    control={control}
                    errors={errors}
                    multiline={localMultiline}
                    minRows={minRows}
                    fieldKey={fieldKey}
                    index={index}
                    sx={sx}
                    isPassword={schema[name].type === 'password'}
                    onValueChange={onValueChange}
                    {...other}
                />
            );
    }
}
