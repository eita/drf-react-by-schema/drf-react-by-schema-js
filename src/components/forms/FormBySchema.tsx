import React, { useEffect, useState } from 'react';
import { UseFormRegister } from 'react-hook-form';
import Box from '@mui/material/Box';
import Card from '@mui/material/Card';
import CardHeader from '@mui/material/CardHeader';
import CardContent from '@mui/material/CardContent';
import CircularProgress from '@mui/material/CircularProgress';

import FieldBySchema from './FieldBySchema';
import { Layout } from '../../styles';
import {
    CommonFieldProps,
    FieldBySchemaProps,
    FormFieldLayout,
    Item,
    OptionsAC,
} from '../../utils';
import { useAPIWrapper } from '../../context/APIWrapperContext';

interface FormBySchemaProps extends Omit<FieldBySchemaProps, 'name'> {
    fieldsLayout?: FormFieldLayout[];
    customFieldFormLayouts?: Record<string, FormFieldLayout[]>;
    hiddenFields?: string[];
    register?: UseFormRegister<Item>;
    setOptionsAC?: (x: OptionsAC) => void;
    forceReload?: boolean;
    isolatedGetAutoComplete?: (model: string) => Promise<false | Item[]>;
    fieldsProps?: Record<string, CommonFieldProps>;
    relatedEditable?: boolean;
}

export default function FormBySchema({
    schema,
    control,
    errors,
    register,
    multiline = false,
    setValue,
    getValues,
    fieldKey,
    labelKey = 'nome',
    index,
    optionsAC,
    setOptionsAC,
    forceReload,
    optionsModel,
    getOptionLabel,
    renderOption,
    onEditModel,
    options,
    isSemaphoric = false,
    label,
    decimalScale = 2,

    fieldsLayout: fieldsLayoutInitial,
    customFieldFormLayouts,

    fieldsProps,
    hiddenFields = [],
    isolatedGetAutoComplete,
    relatedEditable,
}: FormBySchemaProps) {
    const { getAutoComplete, onEditModel: defaultOnEditModel } = useAPIWrapper();
    if (!onEditModel) {
        onEditModel = defaultOnEditModel;
    }
    const [fieldsLayout, setFieldsLayout] = useState<FormFieldLayout[]>([]);
    const [localOptionsAC, setLocalOptionsAC] = useState<OptionsAC | undefined>(undefined);
    const defaultSx = { mr: 0 };
    const getColumns = () => {
        const optionsACModels: string[] = [];
        if (fieldsLayoutInitial && fieldsLayoutInitial.length > 0) {
            for (const section of fieldsLayoutInitial) {
                if (!section.rows) {
                    continue;
                }
                for (const row of section.rows) {
                    if (typeof row === 'string') {
                        const field = row;
                        if (
                            ['field', 'nested object'].includes(schema[field].type) &&
                            !optionsACModels.includes(field)
                        ) {
                            optionsACModels.push(field);
                        }
                        continue;
                    }

                    if (Array.isArray(row)) {
                        for (const field of row) {
                            if (
                                typeof field === 'string' &&
                                ['field', 'nested object'].includes(schema[field].type) &&
                                !optionsACModels.includes(field)
                            ) {
                                optionsACModels.push(field);
                            }
                        }
                        continue;
                    }

                    if (optionsACModels.includes(row.key)) {
                        optionsACModels.push(row.key);
                    }
                }
            }
            return { newFieldsLayout: fieldsLayoutInitial, optionsACModels };
        }

        const rows = [];
        for (const [key, field] of Object.entries(schema)) {
            if (['field', 'nested object'].includes(field.type)) {
                rows.push(key);
                if (!optionsACModels.includes(key)) {
                    optionsACModels.push(key);
                }
                continue;
            }
            if (!field.read_only) {
                rows.push(key);
            }
        }
        return { newFieldsLayout: [{ rows }], optionsACModels };
    };

    const populateOptionsAC = async (optionsACModels: string[]) => {
        const promises: Promise<false | Item[]>[] = [];
        const fieldsToLoad: string[] = [];
        optionsACModels.map((field) => {
            if (!optionsAC || !(field in optionsAC)) {
                fieldsToLoad.push(field);
                promises.push(
                    isolatedGetAutoComplete
                        ? isolatedGetAutoComplete(field)
                        : getAutoComplete(field)
                );
            }
        });
        const newOptionsAC = {} as OptionsAC;
        const results = await Promise.all(promises);
        results.map((result, index) => {
            if (result !== false) {
                newOptionsAC[fieldsToLoad[index]] = result;
            }
        });
        if (setOptionsAC) {
            setOptionsAC(newOptionsAC);
            return;
        }
        setLocalOptionsAC(newOptionsAC);
    };

    useEffect(() => {
        const { newFieldsLayout, optionsACModels } = getColumns();
        setFieldsLayout(newFieldsLayout);
        populateOptionsAC(optionsACModels);
    }, []);

    useEffect(() => {
        if (forceReload) {
            const { newFieldsLayout, optionsACModels } = getColumns();
            setFieldsLayout(newFieldsLayout);
            populateOptionsAC(optionsACModels);
        }
    }, [forceReload]);

    if (
        fieldsLayout.length === 0 ||
        (setOptionsAC && !optionsAC) ||
        (!setOptionsAC && !localOptionsAC)
    ) {
        return (
            <Card sx={Layout.formCard}>
                <CardContent>
                    <Box sx={Layout.loadingBoxWhite}>
                        <CircularProgress />
                    </Box>
                </CardContent>
            </Card>
        );
    }

    return (
        <>
            {register &&
                hiddenFields.map((hiddenField) => (
                    <input
                        hidden
                        {...register(hiddenField)}
                        value={getValues ? getValues(hiddenField) : null}
                        key={hiddenField}
                    />
                ))}
            {fieldsLayout.map((section, sectionIndex) => {
                return (
                    <Card sx={Layout.formCard} key={`section_${sectionIndex}`}>
                        {section.title && <CardHeader title={section.title} />}
                        <CardContent>
                            {section.CustomElement && <>{section.CustomElement}</>}
                            {section.rows && (
                                <>
                                    {section.rows.map((row, rowIndex) => {
                                        if (typeof row === 'string') {
                                            const field = row;
                                            const fieldProps: CommonFieldProps =
                                                fieldsProps && field in fieldsProps
                                                    ? fieldsProps[field]
                                                    : {};
                                            fieldProps.sx = fieldProps.hasOwnProperty('sx')
                                                ? fieldProps.sx
                                                : defaultSx;
                                            return (
                                                <React.Fragment key={field}>
                                                    <FieldBySchema
                                                        name={field}
                                                        schema={schema}
                                                        control={control}
                                                        errors={errors}
                                                        multiline={multiline}
                                                        setValue={setValue}
                                                        getValues={getValues}
                                                        fieldKey={fieldKey}
                                                        labelKey={labelKey}
                                                        index={index}
                                                        optionsAC={
                                                            setOptionsAC
                                                                ? optionsAC
                                                                : localOptionsAC
                                                        }
                                                        optionsModel={optionsModel}
                                                        getOptionLabel={getOptionLabel}
                                                        renderOption={renderOption}
                                                        onEditModel={
                                                            typeof relatedEditable ===
                                                                'undefined' ||
                                                            relatedEditable === true
                                                                ? onEditModel
                                                                : undefined
                                                        }
                                                        fieldsLayout={
                                                            customFieldFormLayouts &&
                                                            field in customFieldFormLayouts
                                                                ? customFieldFormLayouts[field]
                                                                : undefined
                                                        }
                                                        options={options}
                                                        isSemaphoric={isSemaphoric}
                                                        label={label}
                                                        decimalScale={decimalScale}
                                                        {...fieldProps}
                                                    />
                                                </React.Fragment>
                                            );
                                        }
                                        if (Array.isArray(row)) {
                                            return (
                                                <Box sx={Layout.inLineForm} key={`row_${rowIndex}`}>
                                                    <>
                                                        {row.map((field, i, j) => {
                                                            const mr = i + 1 === j.length ? 0 : 2;
                                                            const key =
                                                                typeof field === 'string'
                                                                    ? field
                                                                    : field.key;
                                                            const fieldProps: CommonFieldProps =
                                                                fieldsProps &&
                                                                fieldsProps.hasOwnProperty(key)
                                                                    ? fieldsProps[key]
                                                                    : {};
                                                            fieldProps.sx =
                                                                fieldProps.hasOwnProperty('sx')
                                                                    ? {
                                                                          ...fieldProps.sx,
                                                                          mr,
                                                                      }
                                                                    : {
                                                                          mr,
                                                                      };
                                                            return (
                                                                <React.Fragment
                                                                    key={`field_${rowIndex}_${i}`}>
                                                                    {typeof field === 'string' ? (
                                                                        <FieldBySchema
                                                                            name={field}
                                                                            schema={schema}
                                                                            control={control}
                                                                            errors={errors}
                                                                            multiline={multiline}
                                                                            setValue={setValue}
                                                                            getValues={getValues}
                                                                            fieldKey={fieldKey}
                                                                            labelKey={labelKey}
                                                                            index={index}
                                                                            optionsAC={optionsAC}
                                                                            optionsModel={
                                                                                optionsModel
                                                                            }
                                                                            getOptionLabel={
                                                                                getOptionLabel
                                                                            }
                                                                            renderOption={
                                                                                renderOption
                                                                            }
                                                                            onEditModel={
                                                                                onEditModel
                                                                            }
                                                                            options={options}
                                                                            isSemaphoric={
                                                                                isSemaphoric
                                                                            }
                                                                            label={label}
                                                                            decimalScale={
                                                                                decimalScale
                                                                            }
                                                                            {...fieldProps}
                                                                        />
                                                                    ) : (
                                                                        <field.CustomElement
                                                                            {...fieldProps}
                                                                        />
                                                                    )}
                                                                </React.Fragment>
                                                            );
                                                        })}
                                                    </>
                                                </Box>
                                            );
                                        }

                                        const fieldProps: CommonFieldProps =
                                            fieldsProps && fieldsProps.hasOwnProperty(row.key)
                                                ? fieldsProps[row.key]
                                                : {};
                                        fieldProps.sx = fieldProps.hasOwnProperty('sx')
                                            ? fieldProps.sx
                                            : defaultSx;
                                        return <row.CustomElement {...fieldProps} />;
                                    })}
                                </>
                            )}
                        </CardContent>
                    </Card>
                );
            })}
        </>
    );
}
