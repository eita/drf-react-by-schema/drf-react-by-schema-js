import React from 'react';
import { Controller } from 'react-hook-form';
import Checkbox from '@mui/material/Checkbox';
import FormControlLabel from '@mui/material/FormControlLabel';

import { FieldBySchemaProps } from '../../../utils';

export default function BooleanFieldBySchema({
    name,
    schema,
    control,
    fieldKey,
    index,
    sx = { mr: 2 },
    onValueChange,
    ...other
}: FieldBySchemaProps) {
    const model = name;
    if (fieldKey && index && index >= 0) {
        name = `${fieldKey}.${index}.${name}`;
    }
    return (
        <Controller
            name={name}
            control={control}
            render={({ field }) => (
                <FormControlLabel
                    control={
                        <Checkbox
                            {...field}
                            {...other}
                            value={field.value || false}
                            checked={field.value || false}
                            inputProps={{ 'aria-label': 'controlled' }}
                            onChange={(e) => {
                                if (onValueChange) {
                                    onValueChange(e);
                                }
                                field.onChange(e);
                            }}
                        />
                    }
                    label={schema[model].label}
                    required={schema[model].required}
                    sx={{ width: '100%', ...sx }}
                />
            )}
        />
    );
}
