import React from 'react';
import { Controller } from 'react-hook-form';
import TextField from '@mui/material/TextField';

import { errorProps, FieldBySchemaProps } from '../../../utils';

export default function TextFieldBySchema({
    name,
    schema,
    control,
    errors,
    multiline = false,
    minRows,
    fieldKey,
    index,
    sx = { mr: 2 },
    isPassword = false,
    type,
    autocomplete,
    disabled,
    onValueChange,
    ...other
}: FieldBySchemaProps) {
    const model = name;
    if (fieldKey && index && index >= 0) {
        name = `${fieldKey}.${index}.${name}`;
    }
    if (isPassword) {
        type = 'password';
        autocomplete = 'off';
    }
    const { error, helperText } =
        fieldKey && index && index >= 0
            ? errorProps({
                  fieldKey,
                  index,
                  fieldKeyProp: name,
                  errors,
              })
            : {
                  error: errors && Boolean(errors[name]),
                  helperText:
                      errors && errors[name] ? errors[name].message : schema[name].help_text || '',
              };
    return (
        <Controller
            name={name}
            control={control}
            render={({ field }) => (
                <TextField
                    {...field}
                    {...other}
                    id={name}
                    key={name}
                    label={schema[model].label}
                    required={schema[model].required}
                    disabled={schema[model].disabled === true || disabled}
                    onChange={(e) => {
                        field.onChange(e.target.value);
                        if (onValueChange) {
                            onValueChange(e);
                        }
                    }}
                    margin="normal"
                    fullWidth
                    multiline={multiline}
                    error={error}
                    helperText={helperText}
                    sx={sx}
                    type={type}
                    autoComplete={autocomplete}
                    minRows={minRows}
                />
            )}
        />
    );
}
