import React from 'react';
import { Controller } from 'react-hook-form';
import { DesktopDateTimePicker } from '@mui/x-date-pickers/DesktopDateTimePicker';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import dayjs from 'dayjs';
import 'dayjs/locale/pt-br';

import { errorProps, FieldBySchemaProps } from '../../../utils';

export default function DesktopDateTimePickerBySchema({
    name,
    schema,
    control,
    errors,
    fieldKey,
    index,
    sx = { mr: 2, mt: 2 },
}: FieldBySchemaProps) {
    const model = name;
    if (fieldKey && index && index >= 0) {
        name = `${fieldKey}.${index}.${name}`;
    }
    const { error, helperText } =
        fieldKey && index && index >= 0
            ? errorProps({
                  fieldKey,
                  index,
                  fieldKeyProp: name,
                  errors,
              })
            : {
                  error: errors && Boolean(errors[name]),
                  helperText:
                      errors && errors[name] ? errors[name].message : schema[name].help_text || '',
              };

    return (
        <Controller
            name={name}
            control={control}
            render={({ field }) => (
                <LocalizationProvider dateAdapter={AdapterDayjs} adapterLocale="pt-br">
                    <DesktopDateTimePicker
                        {...field}
                        value={dayjs(field.value)}
                        label={schema[model].label}
                        format="DD/MM/YYYY HH:mm"
                        sx={{ mt: 2, mb: 1, ...sx }}
                        slotProps={{
                            textField: {
                                error,
                                helperText,
                                sx,
                                margin: 'normal',
                            },
                        }}
                    />
                </LocalizationProvider>
            )}
        />
    );
}
