import React from 'react';
import { Controller } from 'react-hook-form';
import { DesktopDatePicker } from '@mui/x-date-pickers/DesktopDatePicker';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import dayjs from 'dayjs';
import 'dayjs/locale/pt-br';

import { buildDateFormatBySchema, errorProps, FieldBySchemaProps } from '../../../utils';

export default function DesktopDatePickerBySchema({
    name,
    schema,
    control,
    errors,
    fieldKey,
    index,
    sx = { mr: 2, mt: 2 },
}: FieldBySchemaProps) {
    if (!sx) {
        sx = { minWidth: 200 };
    }
    if (!sx.hasOwnProperty('minWidth')) {
        sx = { ...sx, minWidth: 200 };
    }
    const model = name;
    if (fieldKey && index && index >= 0) {
        name = `${fieldKey}.${index}.${name}`;
    }
    const inputFormat = buildDateFormatBySchema(schema[model].date_views);
    const { error, helperText } =
        fieldKey && index && index >= 0
            ? errorProps({
                  fieldKey,
                  index,
                  fieldKeyProp: name,
                  errors,
              })
            : {
                  error: errors && Boolean(errors[name]),
                  helperText:
                      errors && errors[name] ? errors[name].message : schema[name].help_text || '',
              };

    return (
        <Controller
            name={name}
            control={control}
            render={({ field }) => (
                <LocalizationProvider dateAdapter={AdapterDayjs} adapterLocale="pt-br">
                    <DesktopDatePicker
                        {...field}
                        value={dayjs(field.value)}
                        label={schema[model].label}
                        views={schema[model].date_views}
                        format={inputFormat}
                        sx={{ mt: 2, mb: 1, ...sx }}
                        slotProps={{
                            textField: {
                                error,
                                helperText,
                                sx,
                                margin: 'normal',
                            },
                        }}
                    />
                </LocalizationProvider>
            )}
        />
    );
}
