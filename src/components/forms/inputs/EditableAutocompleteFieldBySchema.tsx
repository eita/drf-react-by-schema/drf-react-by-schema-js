import React, { useEffect, useState } from 'react';
import { Controller } from 'react-hook-form';
import TextField from '@mui/material/TextField';
import Autocomplete, { createFilterOptions } from '@mui/material/Autocomplete';
import EditIcon from '@mui/icons-material/Edit';
import IconButton from '@mui/material/IconButton';
import Chip from '@mui/material/Chip';

import { isTmpId, errorProps, getTmpId, Item, FieldBySchemaProps, slugify } from '../../../utils';
import DialogActions from '../../DialogActions';
import { useAPIWrapper } from '../../../context/APIWrapperContext';

const filter = createFilterOptions<Item>();

const EditableAutocompleteFieldBySchema = React.forwardRef<typeof Autocomplete, FieldBySchemaProps>(
    (
        {
            control,
            schema,
            errors,
            setValue,
            getValues,
            fieldKey,
            labelKey = 'nome',
            index,
            name = 'name',
            optionsAC,
            optionsModel,
            getOptionLabel,
            renderOption,
            onEditModel,
            sx = { mr: 2 },
            onValueChange,
            multiple = false,
            disabled = false,
            fieldsLayout,
            ...other
        },
        ref
    ) => {
        const { setDialog } = useAPIWrapper();
        const [options, setOptions] = useState<Item[] | null>(null);
        const model = name;
        const label = schema[model].label;
        if (!optionsModel) {
            optionsModel = model;
        }
        if (fieldKey && index && index >= 0) {
            name = `${fieldKey}.${index}.${name}`;
        }

        useEffect(() => {
            if (optionsAC && optionsModel && optionsAC[optionsModel]) {
                setOptions(optionsAC[optionsModel]);
            }
        }, [optionsAC, optionsModel]);

        useEffect(() => {
            if (!getValues || !setValue || !options) {
                return;
            }
            const valuesInitial = getValues(model);
            if (!valuesInitial) {
                return;
            }
            if (multiple) {
                const harmonizedValues: Item[] = [];
                for (const valueInitial of valuesInitial) {
                    const harmonizedValue = options.find((option) => option.id === valueInitial.id);
                    if (harmonizedValue) {
                        harmonizedValues.push(harmonizedValue);
                    }
                }
                setValue(model, harmonizedValues);
                return;
            }
            const harmonizedValues = options.find((option) => option.id === valuesInitial.id);
            setValue(model, harmonizedValues);
        }, [options, setValue, getValues, model, multiple]);

        const { error, helperText } =
            fieldKey && index && index >= 0
                ? errorProps({
                      fieldKey,
                      index,
                      fieldKeyProp: name,
                      errors,
                  })
                : {
                      error: errors && Boolean(errors[name]),
                      helperText:
                          errors && errors[name]
                              ? errors[name].message
                              : schema[name].help_text || '',
                  };

        if (options === null) {
            return <></>;
        }

        if (!disabled && !schema[model].disabled && onEditModel && optionsModel) {
            return (
                <Controller
                    control={control}
                    name={name}
                    render={({ field }) => (
                        <Autocomplete
                            key={name}
                            {...field}
                            id={name}
                            options={options}
                            disabled={schema[model].disabled === true}
                            autoHighlight={true}
                            isOptionEqualToValue={(option, value) => option.id === value.id}
                            fullWidth={true}
                            multiple={multiple}
                            sx={sx}
                            onChange={(e, value) => {
                                let valueAr = value;
                                if (!multiple) {
                                    valueAr = [value];
                                }
                                const newValueAr: Item[] = [];
                                if (valueAr) {
                                    for (let newValue of valueAr as Item[]) {
                                        if (typeof newValue === 'string') {
                                            const tmpId = getTmpId();
                                            newValue = {
                                                id: tmpId,
                                                label: newValue,
                                                [labelKey]: newValue,
                                            };
                                        }
                                        if (newValue && newValue.inputValue) {
                                            const tmpId = getTmpId();
                                            if (onEditModel) {
                                                // Open modal to save new item:
                                                onEditModel({
                                                    fieldKey,
                                                    index,
                                                    model: optionsModel,
                                                    id: tmpId,
                                                    labelKey,
                                                    setValue,
                                                    getValues,
                                                    fieldsLayout,
                                                });
                                            }
                                            // Define new Item as an "optimistic response"
                                            newValue = {
                                                id: tmpId,
                                                label: newValue.inputValue,
                                                [labelKey]: newValue.inputValue,
                                            };
                                        }
                                        newValueAr.push(newValue);
                                    }
                                }
                                field.onChange(multiple ? newValueAr : newValueAr[0]);
                                if (onValueChange) {
                                    onValueChange(e);
                                }
                            }}
                            renderInput={(params) => (
                                <TextField
                                    {...params}
                                    label={label}
                                    required={schema[model].required}
                                    margin="normal"
                                    error={error}
                                    helperText={helperText}
                                    InputProps={{
                                        ...params.InputProps,
                                        endAdornment: (
                                            <>
                                                {!multiple && field.value && (
                                                    <IconButton
                                                        size="small"
                                                        onClick={() => {
                                                            if (isTmpId(field.value.id)) {
                                                                setDialog({
                                                                    open: true,
                                                                    loading: false,
                                                                    title: 'Item sendo criado',
                                                                    Body: 'Este item está sendo criado agora por você. Para editar suas propriedades, salve antes, e depois você poderá editar!',
                                                                    Actions: (
                                                                        <DialogActions
                                                                            setDialog={setDialog}
                                                                            btnCancel="Entendi"
                                                                        />
                                                                    ),
                                                                });
                                                                return;
                                                            }
                                                            if (optionsModel) {
                                                                onEditModel({
                                                                    fieldKey,
                                                                    index,
                                                                    model: optionsModel,
                                                                    id: field.value.id,
                                                                    labelKey,
                                                                    setValue,
                                                                    getValues,
                                                                    fieldsLayout,
                                                                });
                                                            }
                                                        }}>
                                                        <EditIcon />
                                                    </IconButton>
                                                )}
                                                {params.InputProps.endAdornment}
                                            </>
                                        ),
                                    }}
                                    {...other}
                                />
                            )}
                            freeSolo={true}
                            filterOptions={(filteredOptions, params) => {
                                if (filteredOptions.length === 0) {
                                    return [];
                                }
                                let filtered = filter(filteredOptions, params);
                                const { inputValue } = params;
                                const inputValueSlug = slugify(inputValue);
                                const inputValueLength = inputValueSlug.length;
                                // Suggest the creation of a new value
                                const isExisting = filteredOptions.find(
                                    (option) => inputValueSlug === slugify(option.label)
                                );
                                if (inputValue !== '' && !isExisting) {
                                    filtered.push({
                                        inputValue,
                                        label: `Criar "${inputValue}"`,
                                    });
                                }
                                // Show first the exact match:
                                if (isExisting) {
                                    filtered = [
                                        isExisting,
                                        ...filtered.filter((option) => isExisting.id !== option.id),
                                    ];
                                }

                                // Show first the options that start with inputValue:
                                const startsWith = filtered.filter(
                                    (option) =>
                                        inputValueSlug ===
                                        slugify(option.label).substring(0, inputValueLength)
                                );
                                if (startsWith.length > 0) {
                                    const startsWithIds = startsWith.map((option) => option.id);
                                    filtered = [
                                        ...startsWith,
                                        ...filtered.filter(
                                            (option) => !startsWithIds.includes(option.id)
                                        ),
                                    ];
                                }

                                return filtered;
                            }}
                            handleHomeEndKeys={true}
                            getOptionLabel={
                                getOptionLabel
                                    ? getOptionLabel
                                    : (option) => {
                                          // Value selected with enter, right from the input
                                          if (typeof option === 'string') {
                                              return option;
                                          }
                                          // Criar "xxx" option created dynamically
                                          if (option.inputValue) {
                                              return option.inputValue;
                                          }
                                          // Regular option
                                          return option.label;
                                      }
                            }
                            renderOption={
                                renderOption
                                    ? renderOption
                                    : (props, option) => <li {...props}>{option.label}</li>
                            }
                            renderTags={
                                multiple
                                    ? (tagValue, getTagProps) => {
                                          return tagValue.map((option, index) => (
                                              <Chip
                                                  {...getTagProps({ index })}
                                                  label={option.label}
                                                  icon={
                                                      isTmpId(option.id) ? (
                                                          <></>
                                                      ) : (
                                                          <IconButton
                                                              size="small"
                                                              onClick={() => {
                                                                  if (optionsModel) {
                                                                      onEditModel({
                                                                          fieldKey,
                                                                          index,
                                                                          model: optionsModel,
                                                                          id: option.id,
                                                                          labelKey,
                                                                          setValue,
                                                                          getValues,
                                                                          fieldsLayout,
                                                                      });
                                                                  }
                                                              }}>
                                                              <EditIcon />
                                                          </IconButton>
                                                      )
                                                  }
                                              />
                                          ));
                                      }
                                    : undefined
                            }
                        />
                    )}
                />
            );
        }

        return (
            <Controller
                control={control}
                name={name}
                render={({ field }) => (
                    <Autocomplete
                        key={name}
                        {...field}
                        id={name}
                        options={options}
                        disabled={schema[model].disabled === true || disabled === true}
                        autoHighlight={true}
                        isOptionEqualToValue={(option, value) => option.id === value.id}
                        fullWidth={true}
                        multiple={multiple}
                        sx={sx}
                        onChange={(e, value) => {
                            field.onChange(value);
                            if (onValueChange) {
                                onValueChange(e);
                            }
                        }}
                        renderInput={(params) => (
                            <TextField
                                {...params}
                                label={label}
                                required={schema[model].required}
                                margin="normal"
                                error={error}
                                helperText={helperText}
                                {...other}
                            />
                        )}
                        getOptionLabel={getOptionLabel}
                        renderOption={renderOption}
                    />
                )}
            />
        );
    }
);

EditableAutocompleteFieldBySchema.displayName = 'EditableAutocompleteFieldBySchema';
export default EditableAutocompleteFieldBySchema;
