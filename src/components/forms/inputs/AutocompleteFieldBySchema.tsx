import React from 'react';
import { Controller } from 'react-hook-form';
import { SxProps, Theme } from '@mui/material';
import Autocomplete, { AutocompleteRenderInputParams } from '@mui/material/Autocomplete';
import TextField from '@mui/material/TextField';

import { errorProps, FieldBySchemaProps } from '../../../utils';
import { useDRFReactBySchema } from '../../../context/DRFReactBySchemaContext';

const renderInput = ({
    control,
    options,
    optionLabelKey,
    schema,
    name,
    errors,
    fieldKey,
    index,
    isSemaphoric,
    label,
    theme,
    ...params
}: FieldBySchemaProps & AutocompleteRenderInputParams & { theme: Theme }) => {
    const model = name;
    const { error, helperText } =
        fieldKey && index && index >= 0
            ? errorProps({
                  fieldKey,
                  index,
                  fieldKeyProp: name,
                  errors,
              })
            : {
                  error: errors && Boolean(errors[name]),
                  helperText:
                      errors && errors[name] ? errors[name].message : schema[name].help_text || '',
              };
    let sx: SxProps | undefined = undefined;
    if (isSemaphoric && options && optionLabelKey) {
        let tipo: 'success' | 'warning' | 'error' | undefined;
        switch (params.inputProps.value) {
            case options[0][optionLabelKey]:
                tipo = 'success';
                break;
            case options[1][optionLabelKey]:
                tipo = 'warning';
                break;
            case options[2][optionLabelKey]:
                tipo = 'error';
                break;
        }
        if (tipo) {
            sx = { backgroundColor: theme.palette[tipo].semaphoric } as SxProps;
        }
    }
    return (
        <TextField
            {...params}
            label={label || schema[model].label}
            required={schema[model].required}
            margin="normal"
            error={error}
            helperText={helperText}
            sx={sx}
        />
    );
};

export default function AutocompleteFieldBySchema({
    fieldKey,
    index,
    name,
    schema,
    control,
    errors,
    optionIdKey = 'id',
    optionLabelKey = 'label',
    options,
    isSemaphoric = false,
    sx = { mr: 2 },
    onValueChange,
    ...other
}: FieldBySchemaProps) {
    const model = name;
    const { theme } = useDRFReactBySchema();
    if (!options) {
        options = schema[model].choices;
    }
    if (fieldKey && index && index >= 0) {
        name = `${fieldKey}.${index}.${name}`;
    }
    return (
        <Controller
            name={name}
            control={control}
            render={({ field }) => (
                <Autocomplete
                    {...field}
                    key={name}
                    id={name}
                    options={options || []}
                    selectOnFocus
                    autoHighlight
                    isOptionEqualToValue={(option, value) => {
                        return option[optionIdKey] === value[optionIdKey];
                    }}
                    getOptionLabel={(option) => {
                        return option[optionLabelKey];
                    }}
                    onChange={(e, value) => {
                        field.onChange(value);
                        if (onValueChange) {
                            onValueChange(e);
                        }
                    }}
                    fullWidth
                    renderInput={(params) => {
                        return renderInput({
                            ...params,
                            control,
                            isSemaphoric,
                            options,
                            optionLabelKey,
                            schema,
                            name,
                            errors,
                            fieldKey,
                            index,
                            theme,
                        });
                    }}
                    sx={sx}
                    {...other}
                />
            )}
        />
    );
}
