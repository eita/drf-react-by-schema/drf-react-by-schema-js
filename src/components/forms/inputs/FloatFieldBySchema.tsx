import React from 'react';
import { NumericFormat } from 'react-number-format';
import TextField from '@mui/material/TextField';
import { Controller } from 'react-hook-form';

import { errorProps, FieldBySchemaProps } from '../../../utils';

export default function FloatFieldBySchema({
    fieldKey,
    index,
    name,
    control,
    schema,
    errors,
    onValueChange,
    decimalScale,
    label,
    ...other
}: Omit<FieldBySchemaProps, 'type'>) {
    const model = name;
    if (!label) {
        label = schema[model].label;
    }
    if (fieldKey && index && index >= 0) {
        name = `${fieldKey}.${index}.${name}`;
    }
    const { error, helperText } =
        fieldKey && index && index >= 0
            ? errorProps({
                  fieldKey,
                  index,
                  fieldKeyProp: name,
                  errors,
              })
            : {
                  error: errors && Boolean(errors[name]),
                  helperText:
                      errors && errors[name] ? errors[name].message : schema[name].help_text || '',
              };

    const prefix = schema[model].is_currency
        ? 'R$ '
        : schema[model].prefix !== ''
        ? `${schema[model].prefix} `
        : '';
    const suffix = schema[model].suffix !== '' ? ` ${schema[model].suffix}` : '';
    const decimals = decimalScale || schema[model].decimal_places || 2;
    return (
        <Controller
            name={name}
            control={control}
            render={({ field: { ref, ...field } }) => {
                const moedaInputProps = {
                    ...field,
                    onChange: undefined,
                };
                return (
                    <NumericFormat
                        {...moedaInputProps}
                        {...other}
                        label={label}
                        required={schema[model].required}
                        onValueChange={(values) => {
                            field.onChange(values.floatValue);
                            if (onValueChange) {
                                onValueChange(values.floatValue);
                            }
                        }}
                        thousandSeparator="."
                        decimalSeparator=","
                        decimalScale={decimals}
                        fixedDecimalScale={true}
                        valueIsNumericString
                        prefix={prefix}
                        suffix={suffix}
                        error={error}
                        helperText={helperText}
                        margin="normal"
                        customInput={TextField}
                    />
                );
            }}
        />
    );
}
