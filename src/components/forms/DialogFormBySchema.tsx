import React, { useEffect } from 'react';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import { AnyObjectSchema } from 'yup';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';

import { Layout } from '../../styles';
import { FormFieldLayout, Item, SchemaType } from '../../utils';
import FormBySchema from './FormBySchema';
import { DialogType } from '../../context/APIWrapperContext';

interface DialogFormBySchemaProps {
    schema: SchemaType;
    validationSchema: AnyObjectSchema;
    initialValues: Item;
    onEditModelSave: (p: Item) => Promise<boolean>;
    setDialog: (x: Partial<DialogType>) => void;
    getAutoComplete: (model: string) => Promise<false | Item[]>;
    fieldsLayout?: FormFieldLayout[];
}

export default function DialogFormBySchema({
    schema,
    validationSchema,
    initialValues,
    onEditModelSave,
    setDialog,
    getAutoComplete,
    fieldsLayout,
}: DialogFormBySchemaProps) {
    const {
        control,
        handleSubmit,
        reset,
        getValues,
        setValue,
        formState: { errors, isDirty },
    } = useForm({
        mode: 'onBlur',
        resolver: yupResolver(validationSchema),
    });

    useEffect(() => {
        reset(initialValues);
    }, []);

    if (!schema) {
        return <>Houve um erro ao carregar este formulário!</>;
    }

    return (
        <>
            {initialValues && control && (
                <form onSubmit={handleSubmit(onEditModelSave)}>
                    <Box>
                        <FormBySchema
                            control={control}
                            errors={errors}
                            schema={schema}
                            getValues={getValues}
                            setValue={setValue}
                            isolatedGetAutoComplete={getAutoComplete}
                            fieldsLayout={fieldsLayout}
                            relatedEditable={false}
                        />
                    </Box>
                    <Box sx={Layout.flexRow}>
                        <Button
                            variant="contained"
                            color="primary"
                            size="small"
                            sx={{ ml: 1 }}
                            onClick={() => {
                                setDialog({
                                    open: false,
                                });
                            }}>
                            Cancelar
                        </Button>
                        <Button
                            variant="contained"
                            name="exitOnSave"
                            disabled={!isDirty}
                            type="submit"
                            color="primary"
                            size="small">
                            Salvar
                        </Button>
                    </Box>
                </form>
            )}
        </>
    );
}
