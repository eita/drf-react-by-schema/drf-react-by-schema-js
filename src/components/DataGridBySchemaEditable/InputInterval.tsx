import React, { useRef } from 'react';
import Box from '@mui/system/Box';
import TextField from '@mui/material/TextField';
import { NumericFormat } from 'react-number-format';
import SyncIcon from '@mui/icons-material/Sync';

const SUBMIT_FILTER_STROKE_TIME = 500;
type InputIntervalProps = {
    applyValue: (p: any) => void;
    focusElementRef?:
        | null
        | ((p: any) => void)
        | {
              current: any;
          };
    item: {
        columnField: string;
        id: number | string;
        operatorValue: string;
        value: any;
    };
    type: string;
};
const InputInterval = ({ item, applyValue, focusElementRef = null, type }: InputIntervalProps) => {
    const filterTimeout = useRef<any>();
    const [filterValueState, setFilterValueState] = React.useState(item.value ?? '');

    const [applying, setIsApplying] = React.useState(false);

    React.useEffect(() => {
        return () => {
            clearTimeout(filterTimeout.current);
        };
    }, []);

    React.useEffect(() => {
        const itemValue = item.value ?? [undefined, undefined];
        setFilterValueState(itemValue);
    }, [item.value]);

    const updateFilterValue = (lowerBound: any, upperBound: any) => {
        clearTimeout(filterTimeout.current);
        setFilterValueState([lowerBound, upperBound]);

        setIsApplying(true);
        filterTimeout.current = setTimeout(() => {
            setIsApplying(false);
            applyValue({ ...item, value: [lowerBound, upperBound] });
        }, SUBMIT_FILTER_STROKE_TIME);
    };

    const handleUpperFilterChange = (
        event: React.ChangeEvent<HTMLInputElement> | { target: { value: string } }
    ) => {
        const newUpperBound = event.target?.value;
        updateFilterValue(filterValueState[0], newUpperBound);
    };
    const handleLowerFilterChange = (
        event: React.ChangeEvent<HTMLInputElement> | { target: { value: string } }
    ) => {
        const newLowerBound = event.target?.value;
        updateFilterValue(newLowerBound, filterValueState[1]);
    };

    return (
        <Box
            sx={{
                display: 'inline-flex',
                flexDirection: 'row',
                alignItems: 'end',
                height: 48,
                pl: '20px',
            }}>
            {type === 'number' && (
                <>
                    <TextField
                        name="lower-bound-input"
                        placeholder="De"
                        label="De"
                        variant="standard"
                        value={Number(filterValueState[0])}
                        onChange={handleLowerFilterChange}
                        type="number"
                        inputRef={focusElementRef}
                        sx={{ mr: 2, minWidth: 130 }}
                    />
                    <TextField
                        name="upper-bound-input"
                        placeholder="Até"
                        label="Até"
                        variant="standard"
                        value={Number(filterValueState[1])}
                        onChange={handleUpperFilterChange}
                        type="number"
                        sx={{ minWidth: 130 }}
                        InputProps={applying ? { endAdornment: <SyncIcon /> } : {}}
                    />
                </>
            )}
            {type === 'float' && (
                <>
                    <NumericFormat
                        name="lower-bound-input"
                        placeholder="De"
                        label="De"
                        variant="standard"
                        value={Number(filterValueState[0])}
                        onValueChange={(values, sourceInfo) => {
                            handleLowerFilterChange({ target: { value: values.value } });
                        }}
                        thousandSeparator="."
                        decimalSeparator=","
                        decimalScale={2}
                        fixedDecimalScale={true}
                        valueIsNumericString
                        inputRef={focusElementRef}
                        sx={{ mr: 2, minWidth: 130 }}
                        customInput={TextField}
                    />
                    <NumericFormat
                        name="upper-bound-input"
                        placeholder="Até"
                        label="Até"
                        variant="standard"
                        value={Number(filterValueState[1])}
                        onValueChange={(values, sourceInfo) => {
                            handleUpperFilterChange({ target: { value: values.value } });
                        }}
                        thousandSeparator="."
                        decimalSeparator=","
                        decimalScale={2}
                        fixedDecimalScale={true}
                        valueIsNumericString
                        InputProps={applying ? { endAdornment: <SyncIcon /> } : {}}
                        sx={{ minWidth: 130 }}
                        customInput={TextField}
                    />
                </>
            )}
            {type === 'date' && (
                <>
                    <TextField
                        name="lower-bound-input"
                        label="De"
                        variant="standard"
                        value={filterValueState[0] || ''}
                        onChange={handleLowerFilterChange}
                        type="date"
                        inputRef={focusElementRef}
                        InputLabelProps={{ shrink: true }}
                        sx={{ mr: 2, minWidth: 130 }}
                    />
                    <TextField
                        name="upper-bound-input"
                        label="Até"
                        variant="standard"
                        value={filterValueState[1] || ''}
                        onChange={handleUpperFilterChange}
                        type="date"
                        InputProps={applying ? { endAdornment: <SyncIcon /> } : {}}
                        InputLabelProps={{ shrink: true }}
                        sx={{ minWidth: 130 }}
                    />
                </>
            )}
        </Box>
    );
};

export const InputNumberInterval = (props: any) => {
    return <InputInterval {...props} type="number" />;
};

export const InputDateInterval = (props: any) => {
    return <InputInterval {...props} type="date" />;
};

export const InputFloatInterval = (props: any) => {
    return <InputInterval {...props} type="float" />;
};
