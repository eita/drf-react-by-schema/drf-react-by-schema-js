import React from 'react';
import { GridFooterContainer, GridFooter } from '@mui/x-data-grid';
import Button from '@mui/material/Button';
import AddIcon from '@mui/icons-material/Add';

type FooterToolbarProps = {
    isEditable: boolean;
    handleAddItem: () => void;
};

export const FooterToolbar = ({ isEditable, handleAddItem }: FooterToolbarProps) => {
    return (
        <GridFooterContainer>
            {isEditable && (
                <Button
                    color="primary"
                    startIcon={<AddIcon />}
                    onClick={handleAddItem}
                    sx={{ ml: 2 }}>
                    Adicionar
                </Button>
            )}
            <GridFooter sx={isEditable ? { border: 'none' } : { width: '100%' }} />
        </GridFooterContainer>
    );
};
