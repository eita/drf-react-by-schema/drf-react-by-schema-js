import React from 'react';
import { useGridApiContext } from '@mui/x-data-grid';
import { DesktopDatePicker } from '@mui/x-date-pickers/DesktopDatePicker';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { DateView } from '@mui/x-date-pickers';
import dayjs from 'dayjs';

import { GenericValue, buildDateFormatBySchema } from '../../utils';

function buildOpenTo(dateViews?: DateView[]) {
    if (!dateViews || dateViews.includes('day')) {
        return 'day';
    }
    if (dateViews.includes('month')) {
        return 'month';
    }
    return 'year';
}

type GridDateInputProps = {
    field: string;
    id: number | string;
    value?: GenericValue;
    column: object;
    dateViews?: DateView[];
};
export const GridDateInput = ({ id, value, field, dateViews }: GridDateInputProps) => {
    const apiRef = useGridApiContext();
    const inputFormat = buildDateFormatBySchema(dateViews);
    const handleChange = async (newValue: GenericValue) => {
        await apiRef.current.setEditCellValue({ id, field, value: newValue });
        apiRef.current.stopCellEditMode({ id, field });
    };
    const openTo = buildOpenTo(dateViews);
    return (
        <LocalizationProvider dateAdapter={AdapterDayjs} adapterLocale="pt-br">
            <DesktopDatePicker
                key={field}
                value={dayjs(value)}
                onChange={handleChange}
                views={dateViews}
                openTo={openTo}
                format={inputFormat}
                slotProps={{
                    textField: {
                        margin: 'normal',
                    },
                }}
            />
        </LocalizationProvider>
    );
};
