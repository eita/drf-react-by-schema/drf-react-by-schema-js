import React from 'react';
import { useGridApiContext } from '@mui/x-data-grid';
import { PatternFormat } from 'react-number-format';
import TextField from '@mui/material/TextField';

type GridPatternInputProps = {
    field: string;
    id: number | string;
    value?: string | number | null;
    patternFormat?: string;
};
export const GridPatternInput = ({
    id,
    value,
    field,
    patternFormat = 'cpf',
}: GridPatternInputProps) => {
    const apiRef = useGridApiContext();
    const handleChange = async (newValue: string | number | null) => {
        await apiRef.current.setEditCellValue({ id, field, value: newValue });
        apiRef.current.stopCellEditMode({ id, field });
    };
    return (
        <PatternFormat
            key={field}
            id={field}
            onValueChange={(values) => {
                handleChange(values.value);
            }}
            value={value}
            valueIsNumericString
            format={patternFormat}
            mask="_"
            customInput={TextField}
        />
    );
};
