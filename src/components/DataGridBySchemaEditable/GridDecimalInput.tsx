import React from 'react';
import { useGridApiContext } from '@mui/x-data-grid';
import { NumericFormat } from 'react-number-format';
import TextField from '@mui/material/TextField';
import { Item } from '../../utils';

type GridDecimalInputProps = {
    field: string,
    id: number | string,
    value?: any,
    column: object,
    decimalPlaces?: number,
    prefix?: string,
    suffix?: string,
    isCurrency?: boolean,
};
export const GridDecimalInput = ({
    id,
    value,
    field,
    decimalPlaces,
    prefix,
    suffix,
    isCurrency,
}:GridDecimalInputProps) => {
    const apiRef = useGridApiContext();
    const prefixFinal = isCurrency
            ? 'R$ '
            : prefix !== ''
            ? `${prefix} `
            : '';
    const suffixFinal = suffix !== '' ? ` ${suffix}` : '';
    const decimalScale = decimalPlaces || 2;
    const handleChange = async (newValue:any) => {
        await apiRef.current.setEditCellValue({ id, field, value: newValue });
        apiRef.current.stopCellEditMode({ id, field });
    };
    return (
        <NumericFormat
            key={field}
            id={field}
            onValueChange={(values:Item) => {
                handleChange(values.value);
            }}
            value={value}
            thousandSeparator='.'
            decimalSeparator=','
            decimalScale={decimalScale}
            fixedDecimalScale={true}
            valueIsNumericString
            prefix={prefixFinal}
            suffix={suffixFinal}
            customInput={TextField}
        />
    );
};