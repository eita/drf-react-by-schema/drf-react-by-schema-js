import React from 'react';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';

type FConfirmDialogProps = {
    open: boolean,
    onClose: (p: any) => void,
    onConfirm: (p: any) => void
};
const FConfirmDialog = ({
    open,
    onClose,
    onConfirm
}: FConfirmDialogProps) => {
    return (
        <Dialog open={open} onClose={onClose}>
            <DialogTitle>
                Confirmar
            </DialogTitle>
            <DialogContent>
                Tem certeza de que você quer remover este item?
            </DialogContent>
            <DialogActions>
                <Button
                    onClick={onClose}
                >
                        Cancelar
                </Button>
                <Button
                    onClick={onConfirm}
                >
                    Remover
                </Button>
            </DialogActions>
        </Dialog>
    );
};
export const ConfirmDialog = React.memo(FConfirmDialog);