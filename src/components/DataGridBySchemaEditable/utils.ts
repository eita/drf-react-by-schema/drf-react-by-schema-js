import {
    GridApi,
    getGridNumericOperators,
    getGridDateOperators,
    GridFilterOperator,
} from '@mui/x-data-grid';
import { GridEnrichedBySchemaColDef } from '../../utils';
import { InputNumberInterval, InputDateInterval, InputFloatInterval } from './InputInterval';

/**
 * Get the largest width in a column, looking at all rows
 * @param colIndex the column index
 * @returns width of the biggest row inside a column
 */
function maxOfCol(colIndex: number): number {
    const invisibleContainer = document.createElement('div');

    invisibleContainer.style.visibility = 'hidden';
    invisibleContainer.style.zIndex = '-9999999999';
    invisibleContainer.style.position = 'absolute';
    invisibleContainer.style.fontSize = '14px';
    invisibleContainer.style.top = '0';
    invisibleContainer.style.left = '0';
    document.body.append(invisibleContainer);
    const widths: any[] = [];
    document.querySelectorAll(`[aria-colindex='${colIndex}']`).forEach((cell) => {
        const invisibleCell = document.createElement('div');
        invisibleCell.innerHTML = cell.innerHTML;
        invisibleCell.style.width = 'max-content';
        invisibleCell.style.maxWidth = 'none';
        invisibleCell.style.minWidth = 'none';
        invisibleContainer.append(invisibleCell);
        widths.push(Math.ceil(invisibleCell.clientWidth));
    });
    let max = Math.max(...widths);
    if (max !== 0 && max < 50) {
        max = 50;
    }
    invisibleContainer.remove();
    return max;
}

export type ResizeType = 'condense' | 'maxContent' | 'fitScreen';

/**
 *
 * @param columns
 * @param resizeType
 * @param apiRef
 * @returns columns resized to the chosen resizeType
 */
export function resizeColumns(
    columns: GridEnrichedBySchemaColDef[],
    resizeType: ResizeType,
    apiRef: React.MutableRefObject<GridApi>
): GridEnrichedBySchemaColDef[] {
    const cols = [...columns];
    cols.forEach((col, index: number) => {
        if (resizeType === 'fitScreen') {
            delete col.width;
            col.minWidth = 80;
            if (col.isIndexField) {
                col.flex = 1;
            }
        } else if (resizeType === 'maxContent') {
            const maxColWidth = maxOfCol(index);
            delete col.flex;
            delete col.minWidth;
            col.width = maxColWidth + 22;
        } else {
            col.width = 0;
        }
    });
    return cols;
}

export const quantityOnlyOperators = ({ type }: { type: string }) => {
    const builtInFilters = type === 'date' ? getGridDateOperators() : getGridNumericOperators();
    let InputComponent = InputNumberInterval;
    if (type === 'date') {
        InputComponent = InputDateInterval;
    }
    if (type === 'float') {
        InputComponent = InputFloatInterval;
    }
    return [
        ...builtInFilters,
        {
            label: 'entre',
            value: 'entre',
            getApplyFilterFn: (filterItem: any) => {
                if (!Array.isArray(filterItem.value) || filterItem.value.length !== 2) {
                    return null;
                }
                if (filterItem.value[0] === null || filterItem.value[1] === null) {
                    return null;
                }

                return ({ value }: { value: any }) => {
                    return (
                        value !== null &&
                        filterItem.value[0] <= value &&
                        value <= filterItem.value[1]
                    );
                };
            },
            InputComponent,
        },
    ] as GridFilterOperator[];
};
