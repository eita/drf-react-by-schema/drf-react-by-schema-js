import React from 'react';
import { useGridApiContext } from '@mui/x-data-grid';
import { SxProps } from '@mui/material';
import Checkbox from '@mui/material/Checkbox';

import { GridEnrichedBySchemaColDef } from '../../utils';

interface BooleanInputCellProps {
    field: string;
    id: number | string;
    value?: any;
    column: GridEnrichedBySchemaColDef;
    sx?: SxProps;
}

/**
 *
 *
 * @param {BooleanInputCellProps} {
 *     field,
 *     id,
 *     value,
 *     column,
 *     sx = {}
 * }
 * @returns {*}  {JSX.Element}
 */
export function BooleanInputCell ({
    field,
    id,
    value,
    sx = {}
}: BooleanInputCellProps): JSX.Element {
    // TODO: allow edit option label, as in formautocomplete!
    const apiRef = useGridApiContext();

    const handleChange = async (newValue:any) => {
        await apiRef.current.setEditCellValue({ id, field, value: newValue });
        apiRef.current.stopCellEditMode({ id, field });
    };


    return (
        <Checkbox
            key={field}
            id={field}
            checked={value}
            inputProps={{ 'aria-label': 'controlled' }}
            onChange={(event) => {
                handleChange(event.target.checked);
            }}
            sx={sx}
        />
    );
}