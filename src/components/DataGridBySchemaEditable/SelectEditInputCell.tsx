import React from 'react';
import { useGridApiContext } from '@mui/x-data-grid';
import { FilterOptionsState, SxProps } from '@mui/material';
import TextField from '@mui/material/TextField';
import IconButton from '@mui/material/IconButton';
import EditIcon from '@mui/icons-material/Edit';
import Autocomplete, { createFilterOptions } from '@mui/material/Autocomplete';

import {
    FormFieldLayout,
    GenericValue,
    Item,
    OptionsAC,
    getTmpId,
    isTmpId,
    slugify,
} from '../../utils';
import { GridEnrichedBySchemaColDef } from '../../utils';
import { DialogType, OnEditModelType } from '../../context/APIWrapperContext';
import DialogActions from '../DialogActions';

const filter = createFilterOptions<Item>();

interface SelectEditInputCellProps {
    field: string;
    id: number | string;
    value?: GenericValue;
    column: GridEnrichedBySchemaColDef;
    type: string;
    optionsAC: OptionsAC | null;
    isIndexField: boolean;
    multiple?: boolean;
    onEditModel?: (x: OnEditModelType) => void;
    fieldsLayout?: FormFieldLayout[];
    sx?: SxProps;
    setDialog: (x: Partial<DialogType>) => void;
}

export function SelectEditInputCell({
    field,
    id,
    value,
    column,
    type,
    optionsAC,
    isIndexField,
    multiple = false,
    onEditModel,
    fieldsLayout,
    sx = {},
    setDialog,
}: SelectEditInputCellProps): JSX.Element {
    // TODO: allow edit option label, as in formautocomplete!
    const apiRef = useGridApiContext();

    const handleChange = async (newValue: GenericValue) => {
        await apiRef.current.setEditCellValue({ id, field, value: newValue });
        apiRef.current.stopCellEditMode({ id, field });
    };

    const labelKey =
        ['field', 'nested object'].includes(type) || isIndexField ? 'label' : 'display_name';
    const valueKey = ['field', 'nested object'].includes(type) || isIndexField ? 'id' : 'value';

    let creatableProps = {};
    // if (column.creatable || isIndexField) {
    if (column.creatable) {
        creatableProps = {
            freesolo: 'true',
            filterOptions: (
                options: Record<string, GenericValue>[],
                params: FilterOptionsState<GenericValue>
            ) => {
                let filtered = filter(options, params);
                const inputValue = params.inputValue ? params.inputValue : '';
                const inputValueSlug = slugify(inputValue);
                const inputValueLength = inputValueSlug.length;
                // Suggest the creation of a new value
                const isExisting = options.find(
                    (option) => inputValueSlug === slugify(option[labelKey])
                );
                if (inputValue !== '' && !isExisting) {
                    filtered.push({
                        inputValue,
                        [labelKey]: `Criar "${inputValue}"`,
                    });
                }

                // Show first the exact match:
                if (isExisting) {
                    filtered = [
                        isExisting,
                        ...filtered.filter((option) => isExisting.id !== option.id),
                    ];
                }

                // Show first the options that start with inputValue:
                const startsWith = filtered.filter(
                    (option) =>
                        inputValueSlug === slugify(option.label).substring(0, inputValueLength)
                );
                if (startsWith.length > 0) {
                    const startsWithIds = startsWith.map((option) => option.id);
                    filtered = [
                        ...startsWith,
                        ...filtered.filter((option) => !startsWithIds.includes(option.id)),
                    ];
                }
                return filtered;
            },
            handleHomeEndKeys: true,
            getOptionLabel: (option: string | Record<string, GenericValue>) => {
                // Value selected with enter, right from the input
                if (typeof option === 'string') {
                    return option;
                }
                // Criar "xxx" option created dynamically
                if (option.inputValue) {
                    return option.inputValue;
                }
                // Regular option
                return option[labelKey];
            },
            // eslint-disable-next-line @typescript-eslint/no-explicit-any
            renderOption: (props: any, option: Record<string, any>) => {
                return (
                    <li key={option[valueKey]} {...props}>
                        {option[labelKey]}
                    </li>
                );
            },
        };
    }

    return (
        <Autocomplete
            key={field}
            id={field}
            value={value}
            options={optionsAC && optionsAC[field] ? optionsAC[field] : []}
            selectOnFocus
            autoHighlight
            multiple={multiple}
            isOptionEqualToValue={(option, value) => {
                return option[labelKey] === value[labelKey];
            }}
            getOptionLabel={(option) => {
                return option ? option[labelKey] : '';
            }}
            onChange={(e, value) => {
                // if (!column.creatable && !isIndexField) {
                if (!column.creatable) {
                    handleChange(value);
                    return;
                }
                let newValue = value;
                if (typeof newValue === 'string') {
                    const tmpId = getTmpId();
                    newValue = {
                        [valueKey]: tmpId,
                        [labelKey]: newValue,
                    };
                }
                if (newValue && newValue.inputValue) {
                    const tmpId = getTmpId();
                    if (onEditModel) {
                        // Open modal to save new item:
                        onEditModel({
                            model: field,
                            id: tmpId,
                            labelKey,
                            fieldsLayout,
                        });
                    }
                    // Define new Item as an "optimistic response"
                    newValue = {
                        [valueKey]: tmpId,
                        [labelKey]: newValue.inputValue,
                    };
                }
                handleChange(newValue);
            }}
            fullWidth
            renderInput={(params) => (
                <>
                    {column.creatable ? (
                        <TextField
                            {...params}
                            margin="normal"
                            InputProps={{
                                ...params.InputProps,
                                endAdornment: (
                                    <>
                                        {!multiple && onEditModel && value && (
                                            <IconButton
                                                size="small"
                                                onClick={() => {
                                                    if (isTmpId(value.id)) {
                                                        setDialog({
                                                            open: true,
                                                            loading: false,
                                                            title: 'Linha sendo criada',
                                                            Body: 'Esta linha está sendo criada agora por você. Para editar suas propriedades, salve antes, e depois você poderá editar!',
                                                            Actions: (
                                                                <DialogActions
                                                                    setDialog={setDialog}
                                                                    btnCancel="Entendi"
                                                                />
                                                            ),
                                                        });
                                                        return;
                                                    }
                                                    onEditModel({
                                                        model: field,
                                                        id: value[valueKey],
                                                        labelKey,
                                                        fieldsLayout,
                                                    });
                                                }}>
                                                <EditIcon />
                                            </IconButton>
                                        )}
                                        {params.InputProps.endAdornment}
                                    </>
                                ),
                            }}
                        />
                    ) : (
                        <TextField {...params} sx={sx} />
                    )}
                </>
            )}
            {...creatableProps}
        />
    );
}
