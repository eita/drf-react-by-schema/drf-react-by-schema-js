import React, { useState } from 'react';
import {
    useGridApiContext,
    GridToolbarContainer,
    GridToolbarColumnsButton,
    GridToolbarFilterButton,
    GridToolbarDensitySelector,
    GridToolbarExport,
    GridToolbarQuickFilter,
} from '@mui/x-data-grid';
import Button from '@mui/material/Button';
import ExpandIcon from '@mui/icons-material/Expand';
import ArrowDropDownIcon from '@mui/icons-material/ArrowDropDown';
import Menu from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';

import {
    BulkDeleteData,
    BulkUpdateData,
    GridEnrichedBySchemaColDef,
    Id,
    Item,
    OnSelectActions,
} from '../../utils';
import { resizeColumns } from './utils';

type CustomToolbarProps = {
    preparedColumns: GridEnrichedBySchemaColDef[];
    setPreparedColumns: (p: null | GridEnrichedBySchemaColDef[]) => void;
    onSelectActions?: OnSelectActions[];
    selectionModel: Item[];
    bulkUpdateData: BulkUpdateData;
    bulkDeleteData: BulkDeleteData;
    bulkCreateData: (p: Id[]) => void;
    hideColumnsButton?: boolean;
    hideFilterButton?: boolean;
    hideDensityButton?: boolean;
    hideExportButton?: boolean;
    hideQuickFilterBar?: boolean;
};

/**
 *
 *
 * @returns Custom Toolbar for the grid
 */
export const CustomToolbar = ({
    preparedColumns,
    setPreparedColumns,
    onSelectActions,
    selectionModel,
    bulkUpdateData,
    bulkDeleteData,
    bulkCreateData,
    hideColumnsButton,
    hideFilterButton,
    hideDensityButton,
    hideExportButton,
    hideQuickFilterBar,
}: CustomToolbarProps) => {
    const apiRef = useGridApiContext();
    const [resizeMenuAnchorEl, setResizeMenuAnchorEl] = useState<(EventTarget & Element) | null>(
        null
    );
    const isResizeMenuOpen = Boolean(resizeMenuAnchorEl);
    const openResizeMenu = (event: React.MouseEvent) => {
        setResizeMenuAnchorEl(event.currentTarget);
    };
    const closeResizeMenu = () => {
        setResizeMenuAnchorEl(null);
    };
    const [actionsMenuAnchorEl, setActionsMenuAnchorEl] = useState<(EventTarget & Element) | null>(
        null
    );
    const isActionsMenuOpen = Boolean(actionsMenuAnchorEl);
    const openActionsMenu = (event: React.MouseEvent) => {
        setActionsMenuAnchorEl(event.currentTarget);
    };
    const closeActionsMenu = () => {
        setActionsMenuAnchorEl(null);
    };

    return (
        <GridToolbarContainer sx={{ justifyContent: 'space-between' }}>
            <div style={{ display: 'flex', flexWrap: 'wrap' }}>
                {onSelectActions && onSelectActions.length > 0 && selectionModel.length > 0 && (
                    <>
                        <Button
                            onClick={openActionsMenu}
                            sx={{ ml: '0px', fontSize: '13px', color: '#000' }}>
                            <ArrowDropDownIcon sx={{ mr: '6px' }} />
                            Ações ({selectionModel.length})
                        </Button>
                        <Menu
                            anchorEl={actionsMenuAnchorEl}
                            open={isActionsMenuOpen}
                            onClose={closeActionsMenu}>
                            {onSelectActions.map((selectAction, index) => (
                                <MenuItem
                                    key={`onSelectAction_${index}`}
                                    onClick={() => {
                                        closeActionsMenu();
                                        if (typeof selectAction.action === 'string') {
                                            const ids = selectionModel.map((item) => item.id);
                                            if (selectAction.action === 'bulkDelete') {
                                                bulkDeleteData(ids);
                                                return;
                                            }
                                            if (selectAction.action === 'bulkCreate') {
                                                bulkCreateData(ids);
                                                return;
                                            }
                                        }
                                        selectAction.action(selectionModel, bulkUpdateData);
                                    }}>
                                    {selectAction.title}
                                </MenuItem>
                            ))}
                        </Menu>
                    </>
                )}
                {!hideColumnsButton && (
                    <GridToolbarColumnsButton
                        sx={{ ml: '10px', fontSize: '13px' }}
                        placeholder={''}
                    />
                )}
                {!hideFilterButton && (
                    <GridToolbarFilterButton
                        sx={{ ml: '10px', fontSize: '13px' }}
                        placeholder={''}
                    />
                )}
                {!hideDensityButton && (
                    <>
                        <GridToolbarDensitySelector
                            sx={{ ml: '10px', fontSize: '13px' }}
                            placeholder={''}
                        />
                        <Button onClick={openResizeMenu} sx={{ ml: '0px', fontSize: '13px' }}>
                            <ExpandIcon sx={{ transform: 'rotate(90deg)', mr: '6px' }} />
                            Ajustar
                        </Button>
                        <Menu
                            anchorEl={resizeMenuAnchorEl}
                            open={isResizeMenuOpen}
                            onClose={closeResizeMenu}>
                            <MenuItem
                                onClick={() => {
                                    closeResizeMenu();
                                    setPreparedColumns(
                                        resizeColumns(preparedColumns, 'fitScreen', apiRef)
                                    );
                                }}>
                                Ajustar à tela
                            </MenuItem>
                            <MenuItem
                                onClick={() => {
                                    closeResizeMenu();
                                    setPreparedColumns(
                                        resizeColumns(preparedColumns, 'maxContent', apiRef)
                                    );
                                }}>
                                Ajustar ao conteúdo
                            </MenuItem>
                        </Menu>
                    </>
                )}
                {!hideExportButton && <GridToolbarExport sx={{ ml: '0px', fontSize: '13px' }} />}
            </div>
            {!hideQuickFilterBar && (
                <div>
                    <GridToolbarQuickFilter />
                </div>
            )}
        </GridToolbarContainer>
    );
};
