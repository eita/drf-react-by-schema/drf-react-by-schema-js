const path = require('path');
const glob = require('glob');

module.exports = {
  title: 'Styleguides for DRF React By Schema Package',
  styleguideDir: 'public',
  components: [
    'src/components/*.tsx',
    'src/context/*.tsx'
  ],
  resolver: require('react-docgen').resolver.findAllComponentDefinitions,
  propsParser: require('react-docgen-typescript').withDefaultConfig({ propFilter: { skipPropsWithoutDoc: false } }).parse
};